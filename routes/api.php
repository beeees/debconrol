<?php

use App\Http\Controllers\ApiAuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/debug', function() {
    return response()->json([
        'host' => request()->getHost(),
        'httphost' => request()->getHttpHost(),
        'schmehost' => request()->getSchemeAndHttpHost(),
        'path' => request()->getPathInfo(),
        'params' => request()->all(),
    ]);
});
Route::post('time', function () {
    $time = \Carbon\Carbon::now()->formatLocalized('H:i D ,d M Y') . 'г.';
    return response()->json(compact('time'));
});
Route::group(['prefix' => 'auth' ], function (){

    Route::post('login', [ApiAuthController::class, 'login']);
    Route::post('logout', [ApiAuthController::class, 'logout']);
    Route::post('refresh', [ApiAuthController::class, 'refresh']);
    Route::post('reset', [ApiAuthController::class, 'resetLink']);
    Route::post('reset/password', [ApiAuthController::class, 'resetPassword']);
    Route::post('profile', [ApiAuthController::class, 'profile']);

});
Route::group(['prefix' => 'histories' ], function(){
    Route::get('/{model}/{id}', [\App\Http\Controllers\Api\HistoryController::class, 'index']);
});
Route::get('user/service/meta',[\App\Http\Controllers\Api\ServiceUserController::class, 'getMeta']);
Route::resource('user/service',\App\Http\Controllers\Api\ServiceUserController::class);
Route::get('user/company/meta',[\App\Http\Controllers\Api\CompanyUserController::class, 'getMeta']);
Route::get('user/company/export',[\App\Http\Controllers\Api\CompanyUserController::class, 'export']);
Route::get('user/company/check',[\App\Http\Controllers\Api\CompanyUserController::class, 'checkByEmail']);
Route::resource('user/company',\App\Http\Controllers\Api\CompanyUserController::class);
Route::get('plan/meta',[\App\Http\Controllers\Api\CompanyPlanController::class, 'getMeta']);
Route::resource('plan',\App\Http\Controllers\Api\CompanyPlanController::class);
Route::get('company/meta',[\App\Http\Controllers\Api\CompanyController::class, 'getMeta']);
Route::resource('company',\App\Http\Controllers\Api\CompanyController::class);
Route::post('court/ifns',[\App\Http\Controllers\Api\CourtController::class, 'ifnsImport']);
Route::get('court/meta',[\App\Http\Controllers\Api\CourtController::class, 'getMeta']);
Route::get('court/{id}/area',[\App\Http\Controllers\Api\CourtController::class, 'area']);
Route::post('region/{code}/court/import',[\App\Http\Controllers\Api\CourtController::class,'importCourts']);
Route::get('region/{code}',[\App\Http\Controllers\Api\CourtController::class, 'regionCourts']);
Route::resource('court',\App\Http\Controllers\Api\CourtController::class);
Route::get('bailiff',[\App\Http\Controllers\Api\BailiffController::class, 'index']);
Route::resource('company/{company}/case', \App\Http\Controllers\Api\CompanyCaseController::class);
Route::group(['prefix' => 'company/{company}/case/{case}'], function() {
    $controller = \App\Http\Controllers\Api\CompanyCaseUploadedFileController::class;
    Route::get('file/example/{format}', [$controller, 'example']);
    Route::get('file/formats', [$controller, 'formats']);
    Route::get('file/progress', [$controller, 'checkProgress']);
    Route::apiResource('file', $controller, ['except' => ['destroy', 'update']]);
    Route::post('deal-fssp', [\App\Http\Controllers\Api\CaseCheckTypeChangeController::class, 'dealToFssp']);
    Route::post('fssp-deal', [\App\Http\Controllers\Api\CaseCheckTypeChangeController::class, 'fsspToDeal']);
    Route::get('debtor/meta', [\App\Http\Controllers\Api\CompanyCaseDebtorController::class, 'getMeta']);
    Route::get('debtor/results', [\App\Http\Controllers\Api\CompanyCaseDebtorController::class, 'results']);
    Route::put('debtor/refresh', [\App\Http\Controllers\Api\CompanyCaseDebtorController::class, 'refresh']);
    Route::get('debtor/{debtor}/area', [\App\Http\Controllers\Api\CompanyCaseDebtorController::class, 'area']);
    Route::apiResource('debtor', \App\Http\Controllers\Api\CompanyCaseDebtorController::class, ['except' => ['show', 'store']]);
});
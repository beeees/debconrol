<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'task'], function () {
    Route::any('create/{format}', function ($format) {
        return response()->json([
            'taskCode' => \Illuminate\Support\Str::random(10),
            'taskType' => $format,
            'records' => count(request()->json()),
        ]);
    });
    Route::any('{taskCode}/add', function ($taskCode) {
        return response()->json([
            'taskCode' => $taskCode,
            'taskType' => 'add',
            'records' => 100,
        ]);
    });
    Route::any('{taskCode}/status', function ($taskCode) {
        return response()->json([
            'code' => $taskCode,
            'type' => 'status',
            'created' => 0,
            'finished' => true,
            'entries' => [
                'total' => ($f = \App\Models\Company\CompanyUploadedFile::query()
                    ->where('task_code', $taskCode)->first()) ? $f->items : $all = random_int(0, 10),
                'completed' => $f ? $f->debtors()->whereNotNull('finished_at')->count() : random_int(0, $all),
            ]
        ]);
    });
    Route::any('{taskCode}/data', function ($taskCode) {
        return response()->json([
            "task" => [
                "taskType" => "data",
                "taskCode" => $taskCode,
                "finished" => true
            ],
            "items" => [
                [
                    "entity" => [
                        "completed" => true,
                        "lastname" => "ds",
                        "name" => "ds",
                        "secondname" => "ds",
                        "birthday" => "12.05.2020",
                        "regionCode" => 21
                    ],
                    "result" => [
                        [
                            "region" => "string",
                            "address" => "string",
                            "enforcementProceeding" => [
                                "date" => "string",
                                "number" => "string",
                                "summaryCase" => "string"
                            ],
                            "executiveDocument" => [
                                "date" => "string",
                                "number" => "string",
                                "type" => 0,
                                "typeName" => "string",
                                "issuer" => "string"
                            ],
                            "terminationEnforcementProceeding" => [
                                "date" => "string",
                                "reason" => "string"
                            ],
                            "subjectExecution" => [
                                "name" => "ГИБДД",
                                "amount" => 33.22,
                                "enforcementFees" => 0
                            ],
                            "bailiffDepartament" => [
                                "postIndex" => "string",
                                "name" => "string",
                                "address" => "string"
                            ],
                            "bailiff" => [
                                "fullname" => "string",
                                "phones" => [
                                    "string"
                                ]
                            ],
                            "updatedAt" => "string",
                            "deleted" => true,
                            "lastname" => "string",
                            "name" => "string",
                            "secondname" => "string",
                            "birthday" => "string",
                            "regionCode" => 0
                        ],
                        [
                            "region" => "string",
                            "address" => "string",
                            "enforcementProceeding" => [
                                "date" => "string",
                                "number" => "string",
                                "summaryCase" => "string"
                            ],
                            "executiveDocument" => [
                                "date" => "string",
                                "number" => "string",
                                "type" => 0,
                                "typeName" => "string",
                                "issuer" => "string"
                            ],
                            "terminationEnforcementProceeding" => [
                                "date" => "string",
                                "reason" => "string"
                            ],
                            "subjectExecution" => [
                                "name" => "ЖКХ,ипотека",
                                "amount" => 33.22,
                                "enforcementFees" => 0
                            ],
                            "bailiffDepartament" => [
                                "postIndex" => "string",
                                "name" => "string",
                                "address" => "string"
                            ],
                            "bailiff" => [
                                "fullname" => "string",
                                "phones" => [
                                    "string"
                                ]
                            ],
                            "updatedAt" => "string",
                            "deleted" => true,
                            "lastname" => "string",
                            "name" => "string",
                            "secondname" => "string",
                            "birthday" => "string",
                            "regionCode" => 0
                        ]
                    ]
                ],
                [
                    "entity" => [
                        "completed" => true,
                        "lastname" => "ds",
                        "name" => "ds",
                        "secondname" => "sd",
                        "birthday" => "12.05.2020",
                        "regionCode" => 21
                    ],
                    "result" => [
                        [
                            "region" => "string",
                            "address" => "string",
                            "enforcementProceeding" => [
                                "date" => "string",
                                "number" => "string",
                                "summaryCase" => "string"
                            ],
                            "executiveDocument" => [
                                "date" => "string",
                                "number" => "string",
                                "type" => 0,
                                "typeName" => "string",
                                "issuer" => "string"
                            ],
                            "terminationEnforcementProceeding" => [
                                "date" => "string",
                                "reason" => "string"
                            ],
                            "subjectExecution" => [
                                "name" => "string",
                                "amount" => 0,
                                "enforcementFees" => 0
                            ],
                            "bailiffDepartament" => [
                                "postIndex" => "string",
                                "name" => "string",
                                "address" => "string"
                            ],
                            "bailiff" => [
                                "fullname" => "string",
                                "phones" => [
                                    "string"
                                ]
                            ],
                            "updatedAt" => "string",
                            "deleted" => true,
                            "lastname" => "string",
                            "name" => "string",
                            "secondname" => "string",
                            "birthday" => "string",
                            "regionCode" => 0
                        ]
                    ]
                ],
                [
                    "entity" => [
                        "completed" => true,
                        "number" => "fs_number",
                    ],
                    "result" => [],
                ],
                [
                    "entity" => [
                        "completed" => true,
                        "number" => "deal_number",
                    ],
                    "result" => [
                        [
                            "region" => "string",
                            "address" => "string",
                            "enforcementProceeding" => [
                                "date" => "string",
                                "number" => "deal_number",
                                "summaryCase" => "string"
                            ],
                            "executiveDocument" => [
                                "date" => "string",
                                "number" => "string",
                                "type" => 0,
                                "typeName" => "string",
                                "issuer" => "string"
                            ],
                            "terminationEnforcementProceeding" => [
                                "date" => "string",
                                "reason" => "string"
                            ],
                            "subjectExecution" => [
                                "name" => "string",
                                "amount" => 0,
                                "enforcementFees" => 0
                            ],
                            "bailiffDepartament" => [
                                "postIndex" => "string",
                                "name" => "string",
                                "address" => "string"
                            ],
                            "bailiff" => [
                                "fullname" => "string",
                                "phones" => [
                                    "string"
                                ]
                            ],
                            "updatedAt" => "string",
                            "deleted" => true,
                            "lastname" => "string",
                            "name" => "string",
                            "secondname" => "string",
                            "birthday" => "string",
                            "regionCode" => 0
                        ]
                    ]
                ],
                [
                    "entity" => [
                        "completed" => true,
                        "number" => "fs_number",
                        "type" => 0,
                        "regionCode" => 0,
                    ],
                    "result" => [
                        [
                            "region" => "string",
                            "address" => "string",
                            "enforcementProceeding" => [
                                "date" => "string",
                                "number" => "string",
                                "summaryCase" => "string"
                            ],
                            "executiveDocument" => [
                                "date" => "string",
                                "number" => "fs_number",
                                "type" => 0,
                                "typeName" => "string",
                                "issuer" => "string"
                            ],
                            "terminationEnforcementProceeding" => [
                                "date" => "string",
                                "reason" => "string"
                            ],
                            "subjectExecution" => [
                                "name" => "string",
                                "amount" => 0,
                                "enforcementFees" => 0
                            ],
                            "bailiffDepartament" => [
                                "postIndex" => "string",
                                "name" => "string",
                                "address" => "string"
                            ],
                            "bailiff" => [
                                "fullname" => "string",
                                "phones" => [
                                    "string"
                                ]
                            ],
                            "updatedAt" => "string",
                            "deleted" => true,
                            "lastname" => "string",
                            "name" => "string",
                            "secondname" => "string",
                            "birthday" => "string",
                            "regionCode" => 0
                        ]
                    ]
                ]
            ]
        ]);
    });
});
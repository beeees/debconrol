<?php

namespace App\Imports;

use App\Models\Catalog\Court;
use Maatwebsite\Excel\Concerns\SkipsUnknownSheets;
use Maatwebsite\Excel\Concerns\ToArray;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class RequisitesImport implements ToArray, WithHeadingRow, WithMultipleSheets, SkipsUnknownSheets
{

    /**
     * @param array $array
     */
    public function array(array $array)
    {
        foreach ($array as $courtData) {
            $c = Court::query()->where('name', $courtData['sud'])->first() ??
                Court::query()->where('url', $courtData['sait_suda'])->first();
            if ($c) {
                $r = $c->requisite()->firstOrNew();
                $courtData['kazna_account'] = $courtData['scet_eks'];
                $courtData['account'] = $courtData['scet'];
                $courtData['receiver_name'] = $courtData['polucatel'];
                $courtData['bank_name'] = $courtData['naimenovanie_banka_polucatelya'];
                $c->address = $courtData['adres_suda'];
                $c->save();
                $r->file = null;
                $r->court()->associate($c);
                $r->fill($courtData);
                $r->save();
            }
        }
    }

    public function sheets(): array
    {
        return [
            'Верные реквизиты' => new RequisitesImport(),
        ];
    }

    /**
     * @param string|int $sheetName
     */
    public function onUnknownSheet($sheetName)
    {
    }
}

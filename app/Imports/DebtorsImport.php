<?php

namespace App\Imports;

use App\Exceptions\BadDebtorsFileFormatException;
use App\Exceptions\UnknownCaseCheckTypeException;
use App\Jobs\GetDadataCleanedAddressesJob;
use App\Jobs\CreateUploadedFileParserTaskJob;
use App\Jobs\ParseDebtorDealsJob;
use App\Models\CanAuthUser;
use App\Models\Company\CompanyCase;
use App\Models\Company\CompanyUploadedFile;
use App\Models\DebtorFsspDeal;
use App\Models\Debtor;
use App\Models\DebtorDeal;
use App\Utils\CompanyCaseDataUploadFormat;
use Facades\App\Utils\DadataApi;
use Carbon\Carbon;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Events\AfterImport;
use Maatwebsite\Excel\Events\BeforeImport;
use Maatwebsite\Excel\HeadingRowImport;
use Maatwebsite\Excel\Row;

class DebtorsImport implements OnEachRow, WithEvents, WithChunkReading, WithHeadingRow, WithBatchInserts, SkipsEmptyRows
{
    use Importable;
    private $fileModel;
    public $checkType;
    private $errors = [];
    public function __construct(CompanyUploadedFile $file)
    {
        $this->fileModel = $file;
        $this->checkType = (int) $file->companyCase->check_type;
        $this->fileModel->items = 0;
    }

    /**
     * @param Row $row
     * @return Debtor|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     * @throws BadDebtorsFileFormatException
     * @throws UnknownCaseCheckTypeException
     */
    public function onRow(Row $row)
    {
        $row = $row->toArray();
        switch($this->checkType) {
            case CompanyCase::FSSP_CHECK_TYPE:
                return $this->caseFsspCheckModel($row);
            case CompanyCase::FULL_CHECK_TYPE :
                return $this->caseFullCheckModel($row);
            case CompanyCase::DEAL_CHECK_TYPE :
                return $this->caseDealsCheckModel($row);
            case CompanyCase::ASSESSED_CHECK_TYPE :
                try {
                    $row = array_values($row);
                    $d = Debtor::query()->newModelInstance([
                        'mark' => $row[0] ?? '',
                        'secondname' => $row[1] ?? '',
                        'name' => $row[2] ?? '',
                        'lastname' => $row[3] ?? '',
                    ]);
                    if ($this->fileModel->format != CompanyCaseDataUploadFormat::FORMAT_REGION) {
                        $d->birthday = $this->excelDate($row[4]);
                        if ($this->fileModel->format != CompanyCaseDataUploadFormat::FORMAT_BIRTHDAY) {
                            $d->regionCode = (int)$row[5];
                        }
                    } else {
                        $d->regionCode = (int)$row[4];
                    }
                    $d->case_id = $this->fileModel->case_id;
                    $d->file()->associate($this->fileModel);
                    $this->fileModel->items++;
                    $d->save();
                }catch (\Throwable $e) {
                    throw new BadDebtorsFileFormatException();
                }
                return $d;
            default: throw new UnknownCaseCheckTypeException();
        }
    }

    /**
     * @param array $row
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     * @throws BadDebtorsFileFormatException
     */
    public function caseFsspCheckModel(array $row)
    {
        try {
            $row = array_values($row);
            $d = Debtor::query()->newModelInstance([
                'mark' => $row[0],
                'secondname' => '',
                'name' => '',
                'lastname' => '',
            ]);
            $d->case_id = $this->fileModel->case_id;
            $d->file()->associate($this->fileModel);
            $this->fileModel->items++;
            $d->save();
            $d->fsspDeals()->save(new DebtorFsspDeal([
                'deal_number' => $row[1]
            ]));
        }catch (\Throwable $e) {
            throw new BadDebtorsFileFormatException();
        }
        return $d;
    }

    /**
     * @param array $row
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     * @throws BadDebtorsFileFormatException
     */
    public function caseDealsCheckModel(array $row)
    {
        $row = array_values($row);
        $d = Debtor::query()->newModelInstance([
            'mark' =>  Arr::get($row, 0),
            'fio' =>  Arr::get($row, 2),
            'birthday' =>  $this->excelDate(Arr::get($row, 3)),
            'name' =>  '',
            'secondname' =>  '',
            'lastname' => '',
            'error' => null,
        ]);
        $d->case_id = $this->fileModel->case_id;
        $d->file()->associate($this->fileModel);
        $d->save();
        $this->fileModel->items++;
        $d->deals()->save(DebtorDeal::query()->newModelInstance([
            'loaded_initiator' => Arr::get($row, 4),
            'act_number' => Arr::get($row, 1),
            'court_urls' => Arr::get($row, 5),
            'deal_init_date' => $this->excelDate(Arr::get($row, 6)),
            'deal_number' => Arr::get($row, 7)
        ]));
        return $d;
    }

    public function excelDate($value) : ?Carbon
    {
        if (is_string($value)) return Carbon::parse($value);
        if ($value)  return Carbon::parse((\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value)));
        return null;
    }
    /**
     * @param array $row
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     * @throws BadDebtorsFileFormatException
     */
    public function caseFullCheckModel(array $row)
    {
        $row = array_filter($row);
        $row = array_values($row);
        $d = Debtor::query()->newModelInstance([
            'mark' =>  Arr::get($row, 0),
            'fio' =>  Arr::get($row, 1),
            'name' =>  '',
            'secondname' =>  '',
            'lastname' => '',
            'loaded_address' => Arr::get($row, 2),
            'error' => null,
        ]);
        $d->case_id = $this->fileModel->case_id;
        $d->file()->associate($this->fileModel);
        $this->fileModel->items++;
        if (!$d->fio) $d->setError(Debtor::ERROR_NO_NAME);
        if (!$d->loaded_address) $d->setError(Debtor::ERROR_NO_ADDRESS);
        if ($d->error) $this->errors[] = $d;
        if (count($this->errors) > 5) {
            foreach ($this->errors as $debtor) $debtor->forceDelete();
            throw new BadDebtorsFileFormatException('В файле обнаружено более 5 плохих записей. Исправьте ошибки и загрузите файл снова.');
        }
        $d->save();
        return $d;
    }


    public function chunkSize(): int
    {
        return 1000;
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            BeforeImport::class => function(BeforeImport $event) {
                $headings = (new HeadingRowImport)->toArray(($this->fileModel->file), 'public', class_basename($event->getReader()->getPhpSpreadsheetReader()));
                if (($headings[0][0]) !== (Arr::get($arr=CompanyCaseDataUploadFormat::EXCEL_HEADERS[$this->checkType], $this->fileModel->format, Arr::first($arr))))
                    throw new BadDebtorsFileFormatException();
            },
            AfterImport::class => function(AfterImport $event) {
                $checkFields = ['name', 'secondname', 'lastname', 'birthday', 'regionCode', 'loaded_address', 'mark', 'fio', 'id'];
                $dealCheckFields = [];
                $raw = function($check_fields, $deal_check_fields = [], $join_table = '') {
                    $v = [];
                    foreach ($check_fields as $f) $v[] = "min(debtors.\"$f\") as \"$f\"";
                    if ($join_table)
                        foreach ($deal_check_fields as $f) $v[] = "min($join_table.\"$f\") as \"$f\"";
                    return implode(', ', $v);
                };
                $groupRaw = function($check_fields, $deal_check_fields = []) {
                    $v = [];
                    foreach (array_merge($check_fields, $deal_check_fields) as $i => $f) {
                        if ($f == 'id') $f = 'name';
                        $v[] = "$i, \"$f\"";
                    }
                    return implode(', ', $v);
                };
                switch ($this->checkType) {
                    case CompanyCase::ASSESSED_CHECK_TYPE:
                    case CompanyCase::FULL_CHECK_TYPE:
                        $clones = $this->fileModel->debtors()
                            ->selectRaw(DB::raw('count(*) clones_count, ' . $raw($checkFields)))
                            ->groupByRaw('json_build_object('.$groupRaw($checkFields).')::TEXT');
                        break;
                    case CompanyCase::DEAL_CHECK_TYPE:
                        $dealCheckFields = (new DebtorDeal())->getFillable();
                        $joinTable = 'debtor_deals';
                        $clones = DB::table('debtors')
                            ->where('company_uploaded_file_id', $this->fileModel->id)
                            ->join($joinTable, 'debtors.id', '=', 'debtor_id')
                            ->selectRaw('count(*) clones_count, ' .$raw($checkFields, $dealCheckFields, $joinTable))
                            ->groupByRaw('json_build_object('. ($groupRaw($checkFields, $dealCheckFields)) .')::TEXT');
                        break;
                    case CompanyCase::FSSP_CHECK_TYPE:
                        $dealCheckFields = (new DebtorFsspDeal())->getFillable();
                        $checkFields = array_filter($checkFields, function($v) { return $v !== 'mark';});
                        $joinTable = 'debtor_fssp_deals';
                        $clones = DB::table('debtors')
                            ->where('company_uploaded_file_id', $this->fileModel->id)
                            ->join($joinTable, 'debtors.id', '=', 'debtor_id')
                            ->selectRaw('count(*) clones_count, ' .$raw($checkFields, $dealCheckFields,$joinTable))
                            ->groupByRaw('json_build_object('. ($groupRaw($checkFields, $dealCheckFields)) .')::TEXT');
                        break;
                    default: throw new UnknownCaseCheckTypeException();
                }
                $clones = $clones->orderBy('clones_count', 'desc')
                    ->get();
                foreach ($clones as $cloneInfo) {
                    if ($cloneInfo->clones_count > 1) {
                        if ($cloneInfo instanceof Debtor) $cloneInfo = $cloneInfo->getOriginal();
                        else $cloneInfo = (array) $cloneInfo;
                        $q = $this->fileModel->debtors()
                            ->where('id', '<>', Arr::pull($cloneInfo, 'id'))
                            ->where((Arr::only($cloneInfo, $checkFields)));
                        if ($dealCheckFields && $this->checkType === CompanyCase::DEAL_CHECK_TYPE) {
                            $q->whereHas('deals', function ($q) use ($cloneInfo, $dealCheckFields){
                                $q->where((Arr::only($cloneInfo, $dealCheckFields)));
                            });
                        }
                        if ($dealCheckFields && $this->checkType === CompanyCase::FSSP_CHECK_TYPE) {
                            $q->whereHas('fsspDeals', function ($q) use ($cloneInfo, $dealCheckFields){
                                $q->where((Arr::only($cloneInfo, $dealCheckFields)));
                            });
                        }

                        $q->forceDelete();
                    }
                }
                $this->fileModel->updateItems()->save();
                switch ($this->checkType) {
                    case CompanyCase::ASSESSED_CHECK_TYPE:
                    case CompanyCase::FSSP_CHECK_TYPE:
                        $job = new CreateUploadedFileParserTaskJob($this->fileModel);
                        break;
                    case CompanyCase::FULL_CHECK_TYPE:
                        $job = new GetDadataCleanedAddressesJob($this->fileModel->id);
                        break;
                    case CompanyCase::DEAL_CHECK_TYPE:
                        $job = new ParseDebtorDealsJob($this->fileModel->id);
                        break;
                }
                dispatch($job);
            },
        ];
    }

    /**
     * @return int
     */
    public function batchSize(): int
    {
        return 100;
    }
}

<?php

namespace App\Imports;

use App\Models\FnsCourtData;
use Maatwebsite\Excel\Concerns\ToArray;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class IfnsImport implements ToArray, WithHeadingRow
{

    /**
     * @param array $array
     */
    public function array(array $array)
    {
        foreach ($array as $courtData) {
            $courtData = array_values($courtData);
            $fns = FnsCourtData::query()->firstOrNew([
                'ifns' => $courtData[4],
                'oktmo' => (int) $courtData[3],
                'name' => $courtData[0],
            ])->save();
        }
    }

    public function headingRow(): int
    {
        return 2;
    }
}

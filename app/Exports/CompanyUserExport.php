<?php

namespace App\Exports;

use App\Models\Users\CompanyUser;
use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class CompanyUserExport implements FromQuery, WithHeadings, WithMapping
{
    use Exportable;

    private $query;

    public function __construct(\Illuminate\Database\Eloquent\Builder $query)
    {
        $this->query = $query;
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return $this->query;
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'ID',
            'Email',
            'Группа',
            'Компании',
            'Телефон',
            'ФИО',
            'Информация',
        ];
    }

    /**
     * @param mixed|CompanyUser $c
     *
     * @return array
     */
    public function map($c): array
    {
        $companies = '';
        foreach ($c->companies as $company) {
            $companies .= ($companies ? ', ' : '') . $company->name;
        }
        return [
            $c->id,
            $c->email,
            CompanyUser::USER_TYPE_GROUPS[$c->user_group]['ru'],
            $companies,
            $c->phone,
            $c->name,
            $c->info,
        ];
    }
}

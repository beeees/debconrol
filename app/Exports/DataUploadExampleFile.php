<?php

namespace App\Exports;

use App\Utils\CompanyCaseDataUploadFormat;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;

class DataUploadExampleFile implements FromArray
{
    use Exportable;
    private $headers;
    public function __construct(int $format, int $checkType)
    {
        $this->headers = CompanyCaseDataUploadFormat::ALL[$checkType][$format];
    }


    /**
     * @return array
     */
    public function array(): array
    {
        return [$this->headers];
    }
}

<?php

namespace App\Exports;

use App\Models\Catalog\Court;
use Illuminate\Database\Eloquent\Builder;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class CourtRequisitesExport implements FromQuery, WithMapping, WithHeadings
{
    use Exportable;
    private $query;

    public function __construct(\Illuminate\Database\Eloquent\Builder $query)
    {
        $this->query = $query;
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return $this->query;
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'ID',
            'Суд',
            'Адрес суда',
            'Сайт суда',
            'Электронная почта',
            'Телефон',
            'Страница с реквизитами',
            'Получатель',
            'ИНН',
            'КПП',
            'Расчетный счет',
            'Счет ЕКС',
            'Банк',
            'БИК банка',
            'ОКТМО',
            'КБК',
        ];
    }

    /**
     * @param mixed $c
     *
     * @return array
     */
    public function map($c): array
    {
        /** @var Court $c */
        $result =  [
            $c->id,
            $c->name,
            $c->address,
            $c->url,
            $c->email,
            $c->phone,
        ];
        if ($c->requisite && $c->requisite->file) {
            return array_merge($result, [
                $c->requisite->source_url,
                $c->requisite->file,
            ]);
        }
        else if ($c->requisite) {
            return array_merge($result, [
                $c->requisite->source_url,
                $c->requisite->receiver_name,
                $c->requisite->inn,
                $c->requisite->kpp,
                $c->requisite->account,
                $c->requisite->kazna_account,
                $c->requisite->bank_name,
                $c->requisite->bik,
                $c->requisite->oktmo,
                $c->requisite->kbk,
            ]);
        } else return $result;
    }
}

<?php

namespace App\Exports;

use App\Exceptions\UnknownCaseCheckTypeException;
use App\Models\Catalog\Court;
use App\Models\Company\CompanyCase;
use App\Models\Debtor;
use App\Models\DebtorFsspDeal;
use App\Utils\CompanyCaseDataUploadFormat;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Arr;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class DebtorsExport implements FromQuery, WithHeadings,WithMapping, ShouldAutoSize, WithColumnFormatting
{
    use Exportable;
    private $query;
    private $case;

    public function __construct(\Illuminate\Database\Eloquent\Builder $query, CompanyCase $case)
    {
        $this->query = $query->with('courts.requisite');
        $this->case = $case;
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return $this->query;
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        if (($check = $this->case->check_type) === CompanyCase::FSSP_CHECK_TYPE) {
            return array_keys($this->getFsspCheckTypeExportFormat());
        }
        $h =  array_merge(CompanyCaseDataUploadFormat::ALL[$check][CompanyCaseDataUploadFormat::FORMAT_FULL],
            array_values(Debtor::DEBTORS_TABLE_COLUMNS_NAMES[$check])
        ) ;
        return $h;
    }

    /**
     * @param mixed $c
     *
     * @return array
     * @throws UnknownCaseCheckTypeException
     */
    public function map($c): array
    {
        /** @var Debtor $c */
        switch ($this->case->check_type) {
            case CompanyCase::ASSESSED_CHECK_TYPE:
                $result =  [
                    $c->mark,
                    $c->lastname,
                    $c->name,
                    $c->secondname,
                    $c->birthday,
                    $c->regionCode,
                ];
                foreach (Debtor::DEBTORS_TABLE_COLUMNS_NAMES[0] as $field => $name) {
                    $result[] = $c->$field;
                }
                break;
            case CompanyCase::FULL_CHECK_TYPE:
                $result =  [
                    $c->mark,
                    $c->getFullNameAttribute(),
                    $c->address ?: $c->loaded_address,
                ];
                if (($count = $c->getRelation('courts')->count()) >= 1) {
                    $result = array_fill(0, $count, $result);
                    foreach ($c->getRelation('courts')?:[] as $i => $court) foreach (Debtor::DEBTORS_TABLE_COLUMNS_NAMES[1] as $field => $name) {
                        $field = str_replace('court_', '', $field);
                        /** @var Court $court */
                        $result[$i][] = Arr::has($court->getOriginal(), $field) ? $court->$field : $court->getRelation('requisite')->$field;
                    }
                }
                break;
            case CompanyCase::DEAL_CHECK_TYPE:
                $deal = $c->deals()->first();
                $result = [
                    $c->mark,
                    $deal ? $deal->act_number : '',
                    $c->getFullNameAttribute(),
                    $c->birthday,
                    $deal ? $deal->deal_initiator : '',
                    $deal ? $deal->deal_link : '',
                    $deal ? $deal->deal_init_date : '',
                    $deal ? $deal->deal_number : '',
                ];
                foreach (Debtor::DEBTORS_TABLE_COLUMNS_NAMES[CompanyCase::DEAL_CHECK_TYPE] as $field => $name) {
                    if (strpos($field, $s = 'deal_') === 0) {
                        $field = substr($field,strlen($s));
                    }
                    $result[$field] = $deal->$field;
                }
                break;
            case CompanyCase::FSSP_CHECK_TYPE:
                $result =  array_values($this->getFsspCheckTypeExportFormat($c));
                break;
            default: throw new UnknownCaseCheckTypeException();
        }
        foreach ($result as $key => $v) {
            if ($v instanceof Carbon|| mb_stripos($key, 'дата') !== false)  try{
                $result[$key] = Carbon::parse($v)->format('d.m.Y');
            } catch(\Throwable $error) {}
        }
        return array_values($result);
    }


    /**
     * @param Debtor $debtor
     * @return array
     */
    private function getFsspCheckTypeExportFormat(?Debtor $debtor = null) : array
    {
        $d = $debtor ?: new Debtor();
        $check =  $d->id ? $d->checkResults()->firstOrNew([]) : new DebtorFsspDeal();
        $f =  [
            'Метка' => $d->mark,
            'ФИО должника'=> $d->getFullNameAttribute(),
            'Дата рождения' => $d->birthday ? Date::dateTimeToExcel($d->birthday) : null,

        ];
        foreach (Debtor::RESULT_COLUMNS_NAMES as $field => $name) {
            switch($field){
                case 'ip_date':
                    $f[$name] = $check->ip_date;
                    $f['СВ или СД'] = $check->ip_summary_case;
                    break;
                case 'id_type':
                    $f[$name] = $check->id_type_name . ($check->id_date ? ' от ' . $check->id_date : '');
                    continue;
                case 'ip_number':
                    $f[$name] = $check->$field ?: $d->fsspDeals()->firstOrNew()->deal_number;
                    continue;
                default:
                    $f[$name] = $check->$field;
            }
            if ($f[$name] instanceof Carbon) $f[$name] = Date::dateTimeToExcel($f[$name]);
        }
        return $f;
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {

        switch ($this->case->check_type) {
            case CompanyCase::FSSP_CHECK_TYPE:
                return [
                    'C' => NumberFormat::FORMAT_DATE_DDMMYYYY,
                    'E' => NumberFormat::FORMAT_DATE_DDMMYYYY,
                    'J' => NumberFormat::FORMAT_DATE_DDMMYYYY,
                ];
            default:
                return [];
        }
    }
}

<?php

namespace App\Exports;

use App\Console\Commands\FsspBailiffListParse;
use App\Models\Catalog\Bailiff;
use App\Models\Catalog\Court;
use Illuminate\Database\Eloquent\Builder;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class BailiffExport implements FromQuery, WithMapping, WithHeadings
{
    use Exportable;
    private $query;

    public function __construct(\Illuminate\Database\Eloquent\Builder $query)
    {
        $this->query = $query;
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return $this->query;
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return FsspBailiffListParse::TABLE_HEADERS;
    }

    /**
     * @param mixed $c
     *
     * @return array
     */
    public function map($c): array
    {
        /** @var Bailiff $c */
        $result =  [];
        foreach (FsspBailiffListParse::ARRAY_HEADERS as $field) {
            $result[$field] = $c->$field;
        }
        return $result;
    }
}

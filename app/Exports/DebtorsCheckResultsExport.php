<?php

namespace App\Exports;

use App\Models\Company\CompanyCase;
use App\Models\Debtor;
use App\Models\DebtorCheckResult;
use App\Utils\CompanyCaseDataUploadFormat;
use Illuminate\Database\Eloquent\Builder;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class DebtorsCheckResultsExport implements FromQuery, WithHeadings,WithMapping
{
    use Exportable;
    private $query;
    private $case;

    public function __construct(Builder $query, CompanyCase $case)
    {
        $this->query = $query;
        $this->case = $case;
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return $this->query;
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return array_merge(CompanyCaseDataUploadFormat::ALL[CompanyCaseDataUploadFormat::FORMAT_FULL],
            array_values(Debtor::RESULT_COLUMNS_NAMES)
        ) ;
    }

    /**
     * @param mixed $c
     *
     * @return array
     */
    public function map($c): array
    {
        /** @var DebtorCheckResult $c */
        $debtor = $c->debtors()->where('case_id', $this->case->id)->first();
        $result =  [
            $debtor->mark,
            $debtor->lastname,
            $debtor->name,
            $debtor->secondname,
            $debtor->birthday,
            $debtor->regionCode,
        ];
        foreach (Debtor::RESULT_COLUMNS_NAMES as $field => $name) {
            $result[] = $c->$field;
        }
        return $result;
    }
}

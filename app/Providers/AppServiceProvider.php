<?php

namespace App\Providers;

use App\Utils\AntikapchaApi;
use App\Utils\DadataApi;
use App\Utils\ParserApi;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ParserApi::class, function() {
            $client = new Client([
                'base_uri' => config('app.parser_url'),
            ]);
            return new ParserApi($client);
        });
        $this->app->singleton(DadataApi::class, function() {
            $token = config('services.dadata.token');
            $secret = config('services.dadata.secret');
            $headers = [
                "Content-Type" => "application/json",
                "Accept" => "application/json",
                "Authorization" => "Token " . $token,
            ];
            if ($secret) {
                $headers["X-Secret"] = $secret;
            }
            $client = new \GuzzleHttp\Client([
                "base_uri" => config('services.dadata.baseUrl'),
                "headers" => $headers,
            ]);
            return new DadataApi($client);
        });
        $this->app->singleton(AntikapchaApi::class, function() {
            $headers = [
                "Content-Type" => "application/json",
                "Accept" => "application/json",
            ];
            $client = new \GuzzleHttp\Client([
                "base_uri" => config('services.antikapcha.baseUrl'),
                "headers" => $headers,
            ]);
            return new AntikapchaApi($client);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

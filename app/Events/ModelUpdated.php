<?php

namespace App\Events;

use App\Models\AppModel;
use App\Models\CanAuthUser;
use Carbon\Carbon;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;

/**
 * Class UserUpdated
 * @package App\Events
 * @property AppModel model
 */
class ModelUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $model;
    public $changes;

    /**
     * Create a new event instance.
     *
     * @param AppModel $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
        $this->changes = $model->getDirty();
        foreach ($this->changes as $field => $value){
            if (Arr::exists($model->getRelations(), $field)) continue;
            if ($model->getOriginal($field, 'undefined') == $value)
            Arr::forget($this->changes, $field);
        }
        $this->changes = Arr::except($this->changes, $model->getHidden());

    }

}

<?php

namespace App\Mail;

use App\Models\Company\Company;
use App\Models\Kernel\PasswordResetToken;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TextMail extends Mailable
{
    use Queueable, SerializesModels;

    private $text;

    /**
     * Create a new message instance.
     *
     * @param string $message
     */
    public function __construct(string $message, string $subject)
    {
        $this->subject = $subject;
        $this->text = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.report', ['text' => $this->text, 'subject' => $this->subject]);
    }
}

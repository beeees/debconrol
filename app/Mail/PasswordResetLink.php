<?php

namespace App\Mail;

use App\Models\Company\Company;
use App\Models\Kernel\PasswordResetToken;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordResetLink extends Mailable
{
    use Queueable, SerializesModels;

    public $subject = 'Ссылка на восстановление пароля';
    private $tokenModel;
    private $token;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(PasswordResetToken $tModel, string $token)
    {
        $this->tokenModel = $tModel;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $domain = Company::query()->whereHas('users', function($q) {$q->whereEmail($this->tokenModel->email);})
            ->get(['subdomain'])->first();
        $domain = $domain ?$domain->subdomain : config('app.admin_subdomain');
        return $this->view('email.reset_link', [
            'title' => $this->subject,
            'link' => preg_replace('/(https?:\/\/)/', '$1' . $domain .'.', config('app.url')) . '/reset-password?'.http_build_query([
                'email' => $this->tokenModel->email,
                'token' => $this->token,
            ])
        ]);
    }
}

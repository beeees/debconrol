<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Admin extends ServiceUser
{
    use HasFactory;
    public const USER_GROUP = self::ADMIN_GROUP;
    protected $table = 'users';

}

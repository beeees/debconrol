<?php

namespace App\Models\Users;

use App\Models\CanAuthUser;
use App\Models\Company\Company;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class ServiceUser
 * @package App\Models\Users
 */
class ServiceUser extends CanAuthUser
{
    public const USER_TYPE_GROUPS = self::SERVICE_USER_GROUPS;
    use HasFactory;
    public const IS_COMPANY_USER = false;
    protected $table = 'users';
}

<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Manager extends ServiceUser
{
    use HasFactory;
    public const USER_GROUP = self::MANAGER_GROUP;
    protected $table = 'users';
}
<?php

namespace App\Models\Users;

use App\Models\CanAuthUser;
use App\Models\Company\Company;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class CompanyUser
 * @package App\Models\Users
 * @property  Collection|Company[] companies()
 */
class CompanyUser extends CanAuthUser
{
    public const IS_COMPANY_USER = true;
    public const USER_TYPE_GROUPS = self::COMPANY_USER_GROUPS;
    use HasFactory;
    protected $table = 'users';


    /**
     * @param array $companies
     * @throws \Exception
     */
    public function syncCompanies(array $companies): void
    {
        $this->companies()->sync($companies);
        foreach ($this->companies()->get() as $c) {
            if (!$c->user && $this->user_group == CompanyUser::BOSS_GROUP) {
                $c->user()->associate($this);
                $c->save();
            }
        }
    }

    /**
     * @return bool
     */
    public function isBlocked(): bool
    {
        $activeCompanies = $this->companies()
            ->where('is_active', true)
            ->whereHas('activePlan', function ($q) {
                $q->whereHas('companyPlan', function($cpq) { $cpq->where('is_active', true); });
            });
        if (!$activeCompanies->count()) return true;
        return parent::isBlocked();
    }
    public function toArray()
    {
        $companies = $this->companies()->get(['companies.id', 'companies.name']);
        return parent::toArray() + [
            'companies' => $companies,
        ];
    }
}

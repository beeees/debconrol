<?php

namespace App\Models\Users;

use App\Models\CanAuthUser;
use App\Models\ChangeHistory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class UserHistory
 * @package App\Models\Users
 * @property CanAuthUser user
 * @property int user_id
 */
class UserHistory extends ChangeHistory
{

    public const MODEL_TYPE = self::USER_MODEL_TYPE;

    protected $fillable = [
        'user_id', 'changes',
    ];

    /**
     * @return BelongsTo
     */
    public function user() : BelongsTo
    {
        return $this->belongModel(CanAuthUser::class);
    }

}

<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class AppModel
 * @package App\Models
 * @mixin Builder
 */
class AppModel extends Model
{
    public const STORAGE_DIRECTORY = '';
    protected const HISTORY_CLASS = ChangeHistory::class;
    use HasFactory;

    protected $casts = [
        'created_at' => 'datetime:d.m.Y H:i',
        'updated_at' => 'datetime:d.m.Y H:i',
        'deleted_at' => 'datetime:d.m.Y H:i',
        'expired_at' => 'datetime:d.m.Y H:i',
        'active_at' => 'datetime:d.m.Y H:i',
        'checked_at' => 'datetime:d.m.Y H:i',
        'finished_at' => 'datetime:d.m.Y H:i',
        'birthday' => 'datetime:d.m.Y',
        'ip_date' => 'datetime:d.m.Y',
        'ip_end_date' => 'datetime:d.m.Y',
        'id_date' => 'datetime:d.m.Y',
    ];

    /**
     * @return HasMany
     */
    public function histories() : HasMany
    {
        return $this->hasMany(static::HISTORY_CLASS, 'model_id', 'id');
    }
    public function getStoreDirectory(?string $directory = null) : string
    {
        $now = Carbon::now();
        return ($directory?:static::STORAGE_DIRECTORY) . DIRECTORY_SEPARATOR
            . $now->year . DIRECTORY_SEPARATOR
            . $now->month;
    }

    /**
     * @param array $attributes
     * @return array
     */
    protected function addDateAttributesToArray(array $attributes)
    {
        foreach ($this->getDates() as $key) {
            if (! isset($attributes[$key])) {
                continue;
            }

            $attributes[$key] = is_string($attributes[$key]) ? $attributes[$key] : $this->serializeDate(
                $this->asDateTime($attributes[$key])
            );
        }

        return $attributes;
    }
}

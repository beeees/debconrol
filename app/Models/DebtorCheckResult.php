<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class DebtorCheckResult
 * @package App\Models
 */
class DebtorCheckResult extends AppModel
{
    use SoftDeletes, HasFsNumber;
    protected $fillable = [
        'region',                       
        'address',                      
        'ip_date',                      
        'ip_number',                    
        'ip_summary_case',              
        'id_date',                      
        'id_type',                      
        'id_type_name',                 
        'id_number',                    
        'id_issuer',                    
        'ip_end_date',                  
        'ip_end_reason',                
        'subject_name',                 
        'subject_amount',               
        'subject_fees',                 
        'bailiff_department_name',      
        'bailiff_department_post_index',
        'bailiff_department_address',   
        'bailiff_fullname',             
        'bailiff_phones',               
    ];
    protected $dates = [
        'ip_date',
        'id_date',
        'ip_end_date',
    ];

    /**
     * @return BelongsToMany|Debtor|Debtor[]
     */
    public function debtors() : BelongsToMany
    {
        return $this->belongsToMany(Debtor::class, 'debtors_results');
    }

    /**
     * @param array $options
     * @return bool
     */
    public function save(array $options = [])
    {
        if ($this->isDirty('id_number')) $this->id_number = $this->formatFsNumber($this->id_number?:'');
        return parent::save($options);
    }

}

<?php

namespace App\Models;

use App\Events\ModelUpdated;
use App\Mail\PasswordResetLink;
use App\Models\Company\Company;
use App\Models\Kernel\PasswordResetToken;
use App\Models\Users\UserHistory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * Class User
 * @package App\Models
 * @property PasswordResetToken resetToken
 * @property int id
 * @property string email
 * @property string password
 * @property string phone
 * @property boolean is_blocked
 * @property boolean is_company_user
 * @property boolean is_super_admin
 * @property int user_group
 * @property string info
 * @property string name
 * @property string $rememberTokenName
 */
class CanAuthUser extends Authenticatable
{
    use HasFactory, Notifiable, HasApiTokens, SoftDeletes;
    public const MIN_PASSWORD_LENGTH = 6;
    public const ACCOUNTANT_GROUP = 4;
    public const LAWYER_GROUP = 16;
    public const OPERATOR_GROUP = 64;
    public const BOSS_GROUP = 256;
    public const MANAGER_GROUP = 1024;
    public const ADMIN_GROUP = 4096;
    public const USER_TYPE_GROUPS = [];
    public const SERVICE_USER_GROUPS = [
        self::ADMIN_GROUP => ['ru' => 'Администратор', 'en' => 'admin'],
        self::MANAGER_GROUP => ['ru' => 'Менеджер', 'en' => 'manager'],
    ];
    public const COMPANY_USER_GROUPS = [ //is_company_user = true
        self::BOSS_GROUP => ['ru' => 'Директор', 'en' => 'boss'],
        self::OPERATOR_GROUP => ['ru' => 'Оператор', 'en' => 'operator'],
        self::LAWYER_GROUP  => ['ru' => 'Юрист', 'en' => 'lawyer'],
        self::ACCOUNTANT_GROUP => ['ru' => 'Бухгалтер', 'en'  => 'accountant'],
    ];
    const STATUS_ACTIVE = 0;
    const STATUS_NOT_ACTIVE = 1;
    const STATUS_BLOCKED = 2;
    public const STATUSES = [
        self::STATUS_ACTIVE => 'Активный',
        self::STATUS_NOT_ACTIVE => 'Неактивный',
        self::STATUS_BLOCKED => 'Заблокированный',
    ];
    public const IS_COMPANY_USER = null;
    public const USER_GROUP = null;

    protected $table = 'users';
    protected $casts = [
        'created_at' => 'datetime:d.m.Y H:i',
        'updated_at' => 'datetime:d.m.Y H:i',
        'deleted_at' => 'datetime:d.m.Y H:i',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('company_user_scope', function (Builder $builder) {
            if (static::IS_COMPANY_USER !== null)
                $builder->where('is_company_user', static::IS_COMPANY_USER);
            if (static::USER_GROUP !== null)
                $builder->where('user_group', static::USER_GROUP);
            return $builder;
        });
    }

    /**
     * @param array $attributes
     * @return static|Model
     */
    public function fill(array $attributes)
    {
        $fill = parent::fill($attributes);
        $fill->is_company_user = static::IS_COMPANY_USER;
        return $fill;
    }

    public function fillPassword(array $attrs)
    {
        if ($p = Arr::get($attrs, 'password')) {
            $this->password = \Illuminate\Support\Facades\Hash::make($p);
        }
        return $this;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'info',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'is_company_user',
    ];

    /**
     * @return HasOne|PasswordResetToken
     */
    public function resetToken() : HasOne
    {
        return $this->hasOne(PasswordResetToken::class, 'email', 'email');
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $arr =  parent::toArray();
        $arr['user_type'] = Arr::get(Arr::get(
                $this->is_company_user ? self::COMPANY_USER_GROUPS : self::SERVICE_USER_GROUPS,
            $this->user_group, []), 'en', 'default');
        return $arr;
    }

    /**
     * @return HasMany
     */
    public function histories() : HasMany
    {
        return $this->hasMany(UserHistory::class, 'model_id', 'id')->orderBy('created_at');
    }

    /**
     * @return BelongsToMany|Company
     */
    public function companies(): BelongsToMany
    {
        return $this->belongsToMany(Company::class, 'company_users', 'user_id', 'company_id');
    }

    /**
     * @return bool
     */
    public function isCompanyUser() : bool
    {
        return $this->is_company_user;
    }
    /**
     * @return bool
     */
    public function isBlocked() : bool
    {
        return $this->is_blocked;
    }
    /**
     * @param array $options
     * @return bool
     */
    public function save(array $options = [])
    {
        $model = clone $this;
        if (parent::save($options)) {
            $model->id = $this->id;
            if ($r = request('reason')) $model->reason = $r;
            $model->forceFill(Arr::only($options, ['companies']));
            ModelUpdated::dispatch($model);
            return true;
        }
        return false;
    }


    public function sendPasswordResetMail()
    {
        $string = Str::random();
        $token = PasswordResetToken::query()->firstOrNew($this->only('email'), []);
        $token->token = \Illuminate\Support\Facades\Hash::make($string);
        $token->created_at = Carbon::now();
        $token->save();
        Mail::to($this)->send(new PasswordResetLink($token, $string));
    }


}

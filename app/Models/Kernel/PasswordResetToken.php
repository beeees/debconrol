<?php

namespace App\Models\Kernel;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PasswordResetToken
 * @package App\Models\Kernel
 * @property string token
 * @property string email
 */
class PasswordResetToken extends Model
{
    protected $table = 'password_resets';
    protected $fillable = ['email', 'token'];
    const UPDATED_AT = null;
}

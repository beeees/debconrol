<?php

namespace App\Models;

use App\Models\Debtor;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DebtorException
 * @package App\Models\Models
 * @property string info
 */
class DebtorException extends Model
{
    use HasFactory;
    protected $fillable = ['debtor_id', 'info'];
    public $timestamps = false;
    public $incrementing = false;

    public static function report(Debtor $debtor, \Throwable $e)
    {
        $debtor->exceptions()->save(new DebtorException([
            'info' => '['. $e->getFile(). ']['.$e->getLine().']['.$e->getMessage().']'
            ]));
    }
}

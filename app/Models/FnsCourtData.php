<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class FnsCourtData
 * @package App\Models
 * @property string name
 * @property int oktmo
 * @property int ifns
 */
class FnsCourtData extends Model
{
    use HasFactory;
    protected $fillable = [
        'ifns',
        'oktmo',
        'name'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class DadataAddressRecord extends Model
{
    use HasFactory;
    protected $casts = [
        'result' => 'array'
    ];
    public function fill(array $attributes)
    {
        if ($v = Arr::get($this->attributes, 'result')) $this->options['result'] = $v;
        return parent::fill($attributes);
    }

    protected $fillable = [
        'address',
        'result'
    ];

    public static function fillDebtorByResultArray(Debtor $debtor, ?array &$address) : void
    {
        if (!$address || $address['qc'] > 1) {
            $debtor->setError(Debtor::ERROR_BAD_ADDRESS);
            $address = null;
        } else {
            $debtor->address = $address['region'] . ', ' . $address['result'];
            $debtor->regionCode = (int)substr($address['region_kladr_id'], 0, 2);
        }
    }
}

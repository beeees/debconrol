<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class DadataFioRecord extends Model
{
    use HasFactory;
    protected $casts = [
        'result' => 'array'
    ];
    public function fill(array $attributes)
    {
        if ($v = Arr::get($this->attributes, 'result')) $this->options['result'] = $v;
        return parent::fill($attributes);
    }

    protected $fillable = [
        'fio',
        'result'
    ];

    public static function fillDebtorByResultArray(Debtor $debtor, ?array &$name) : void
    {
        if ($name && $name['qc'] < 2) {
            $debtor->lastname = $name['patronymic'] ?? '';
            $debtor->secondname = $name['surname'] ?? '';
            $debtor->name = $name['name'] ?? $debtor->fio;
            if ($name['qc'] == 1) {
                $debtor->setError(Debtor::ERROR_BAD_NAME);
            }
        }
    }
}

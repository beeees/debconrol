<?php

namespace App\Models\Catalog;

use App\Console\Commands\FsspBailiffListParse;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bailiff extends Model
{
    use HasFactory;

    /**
     * @return array
     */
    public function getFillable()
    {
        return array_merge(FsspBailiffListParse::ARRAY_HEADERS, [
            'region_code'
        ], parent::getFillable());
    }
}

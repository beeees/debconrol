<?php

namespace App\Models\Catalog;

use App\Models\AppModel;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Region
 * @package App\Models\Catalog
 * @property int code
 * @property string name
 * @property Court[]|Collection courts
 */
class Region extends AppModel
{
    use HasFactory;
    protected $primaryKey = 'code';
    protected $fillable = [
        'name',
        'code',
    ];

    /**
     * @return HasMany|Court
     */
    public function courts() : HasMany
    {
        return $this->hasMany(Court::class, 'region_code', 'code');
    }
}

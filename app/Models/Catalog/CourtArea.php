<?php

namespace App\Models\Catalog;

use App\Models\AppModel;
use App\Models\DadataAddressRecord;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Arr;
use function Matrix\add;
use function PHPSTORM_META\type;

/**
 * Class CourtArea
 * @package App\Models\Catalog
 * @property string locality
 * @property string address
 * @property Court court
 * @property string details
 * @method static static|Builder byAddress(DadataAddressRecord $address)
 */
class CourtArea extends AppModel
{
    use HasFactory;
    protected $fillable = [
        'locality',
        'address',
        'details'
    ];

    /**
     * @return BelongsTo|Court
     */
    public function court() : BelongsTo
    {
        return $this->belongsTo(Court::class);
    }

    public function scopeByAddress(Builder $builder, DadataAddressRecord $address): Builder
    {
        $address = $address->result;
        if (!($city = Arr::get($address, 'city') ?: Arr::get($address, 'area'))
            & (!($street = Arr::get($address,$t = 'street') ?: Arr::get($address, $t = 'settlement'))))
            return $builder->whereNull('court_areas.id');
        $types = [
            $address[$t.'_type_full'],
        ];
        if ($address[$t.'_type'] === 'проезд') {
            $types[] = 'пр-д';
            $types[] = 'прд';
        } else {
            $types[] = $address[$t.'_type'];
        }
        $types = implode('|', array_filter($types));
        $builder = $builder->whereRaw(<<<SQL
          concat(locality, ', ', address, ', ', details) like '%{$city}%'
          and concat(locality,', ', address, ', ', details) ~ '(\s|-|^|\.){$street}'
SQL
          . (is_null(Arr::get($address, $t.'_type')) ? '' :
                " and (concat(locality,', ', address, ', ', details) ~ '(${types})')"
        ));
        return $builder;
    }
}

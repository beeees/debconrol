<?php

namespace App\Models\Catalog;

use App\Console\Commands\RegionParsersInfo;
use App\Jobs\ReportJob;
use App\Models\AppModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;

/**
 * Class Court
 * @package App\Models\Catalog
 * @property  string name
 * @property string address
 * @property string phone
 * @property string email
 * @property string url
 * @property int region_code
 * @property int court_type
 * @property Region region
 * @property CourtRequisite requisite
 * @property CourtStructure[]|Collection structure
 * @property CourtArea[]|Collection area
 * @property Court main
 * @property  Court[]|Collection magistrates
 * @property int main_court_id
 * @method static Builder|static city()
 * @method static Builder|static magistrate()
 * @method static Builder|static supreme()
 * @method static Builder|static byUrl(string $href)
 * @method static Builder|static byName(string $name)
 */
class Court extends AppModel
{
    use HasFactory, SoftDeletes;
    public const MAGISTRATE_TYPE = 0;
    public const CITY_TYPE = 1;
    public const SUPREME_TYPE = 2;
    public const TYPES = [
        self::MAGISTRATE_TYPE => 'Участок мирового судьи',
        self::CITY_TYPE => 'Районый суд, городской суд',
        self::SUPREME_TYPE => 'Верховный суд',
    ];
    protected $fillable = [
        'name', 'address', 'phone', 'email', 'url', 'court_type'
    ];

    /**
     * @return BelongsTo|Region
     */
    public function region() : BelongsTo
    {
        return $this->belongsTo(Region::class, 'region_code', 'code');
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeCity(Builder $query) : Builder
    {
        return $query->where('court_type', self::CITY_TYPE);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeSupreme(Builder $query) : Builder
    {
        return $query->where('court_type', self::SUPREME_TYPE);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeMagistrate(Builder $query) : Builder
    {
        return $query->where('court_type', self::MAGISTRATE_TYPE);
    }

    public function scopeByUrl(Builder $query, string $href) : Builder{
        return $query->where('url', 'like', "%$href%")->city();
    }

    public function scopeByName(Builder $query, string $name) : Builder{
        return $query->where('name', 'like', "%$name%")->city();
    }

    /**
     * @return HasOne|CourtRequisite
     */
    public function requisite() : HasOne {
        return $this->hasOne(CourtRequisite::class, 'court_id');
    }

    /**
     * @return HasMany|CourtStructure
     */
    public function structure() : HasMany {
        return $this->hasMany(CourtStructure::class);
    }

    /**
     * @return HasMany|CourtArea
     */
    public function area() : HasMany
    {
        return $this->hasMany(CourtArea::class);
    }

    /**
     * @return BelongsTo|self
     */
    public function main() : BelongsTo
    {
        return $this->belongsTo(self::class, 'main_court_id');
    }

    /**
     * @return HasMany|self|self[]
     */
    public function magistrates() : HasMany
    {
        return $this->hasMany(self::class, 'main_court_id');
    }

    /**
     * @return string
     */
    public function fullName() : string
    {
        return $this->name . ' (' . Arr::get(Arr::get(RegionParsersInfo::REGIONS,(int) $this->region_code, []), 'name'). ')';
    }

    /**
     * @param array $options
     * @return bool
     */
    public function save(array $options = [])
    {
        if ($this->court_type === self::MAGISTRATE_TYPE
            && $old = self::query()->onlyTrashed()->where($this->only('region_code', 'name', 'court_type'))->first()) {
            $msg = '';
            if ($old->address !== $this->address && $old->address){
                $msg .= 'Изменился адрес суда ' . $this->fullName() . PHP_EOL;
            }
            if ($old->main_court_id !== $this->main_court_id) {
                $msg .= 'Изменился районный суд для суда ' . $this->fullName() . PHP_EOL;
            }
            if ($msg) dispatch(new ReportJob($msg));
        }
        return parent::save($options);
    }
}

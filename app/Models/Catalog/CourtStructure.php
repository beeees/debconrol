<?php

namespace App\Models\Catalog;

use App\Models\AppModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class CourtStructure
 * @package App\Models\Court
 * @property string work_position
 * @property string fio
 * @property string room
 * @property string phone
 * @property string email
 * @property Court court
 * @property string department
 * @property string source_url
 */
class CourtStructure extends AppModel
{
    use HasFactory;
    public const EXCEPT_MENU_KEYS = 'история|архив|фото|президиум';
    public const    STEP_MENU_KEYS = [
        [//link search titles
            ['труктур', ['рганиз']],
            ['контакт', ['информа']],
            ['суда', [ 'подразделения'], self::EXCEPT_MENU_KEYS],
            ['труктур', ['рганиз']],
            ['суда', ['остав', 'труктур'],self::EXCEPT_MENU_KEYS],
            ['коллеги', ['судебн'],],
            ['суда', ['остав', 'труктур'],self::EXCEPT_MENU_KEYS],
            ['контакт', ['информа']],
            ['актуальн', ['информа']],
            ['судьи', ['руководст']],
        ],
        [//second try link search titles
            ['суда', ['остав', 'труктур'], self::EXCEPT_MENU_KEYS],
            ['контакт', ['информа']],
            ['суда', [ 'подразделения'],self::EXCEPT_MENU_KEYS],
            [],
            [],
            [],
            [],
            [],
            [],
        ],
        [//dynamic menu link titles
            ['суда', ['подраздел','труктур','остав', 'аппарат',], self::EXCEPT_MENU_KEYS],
            ['контакт', ['информа'], self::EXCEPT_MENU_KEYS],
            ['суда', [ 'подразделения'], self::EXCEPT_MENU_KEYS],
            ['суда', ['подраздел','аппарат','остав'], self::EXCEPT_MENU_KEYS],
            ['суда', ['остав', 'аппарат',]],
            [],
            ['суда', ['штат',]],
            ['суда', ['остав',]],
        ],
        [
            [
                ['кадров'],
                ['аппарат'],
            ],
            [
                ['контакт', ['информа']],
            ],
        ],    
    ];
    public const HOT_KEYS = [
        'fio' => ['фио','имя','ф.и.о', 'ф. и. о.'],
        'work_position' => ['должность'],
        'room' => ['кабинет',],
        'phone' => ['телефон','номер',],
        'email' => ['почт',],
        'department' => ['отдел','атегория рассмотрения дел']
    ];
    protected $fillable = [
        'work_position', 'fio', 'room', 'phone', 'email', 'department','source_url',
    ];

    /**
     * @return BelongsTo|Court
     */
    public function court() : BelongsTo
    {
        return $this->belongsTo(Court::class);
    }
    public function save(array $options = [])
    {
        foreach ($this->fillable as $field) {
            $this->$field = trim(preg_replace('/&nbsp;/', ' ', $this->$field));
        }
        return parent::save($options);
    }
}

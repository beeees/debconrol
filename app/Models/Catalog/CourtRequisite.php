<?php

namespace App\Models\Catalog;

use App\Models\AppModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Arr;

/**
 * Class CourtRequisite
 * @package App\Models\Catalog
 * @property string receiver_name
 * @property string inn
 * @property string kpp
 * @property string account
 * @property string bank_name
 * @property string bik
 * @property string oktmo
 * @property string kbk
 * @property string kazna_account
 * @property string source_url
 * @property string target
 * @property string file
 * @property string by_ifns
 * @property Court court
 */
class CourtRequisite extends AppModel
{
    use HasFactory;
    public const STORAGE_DIRECTORY = 'court/requisite';
    public const    STEP_MENU_KEYS = [
        ['пошлина', 'государствен'],
        ['оплат|платежн', 'реквизит'],
        ['уплат|платежн', 'реквизит'],
        ['реквизит'],
    ];
    public const EXCEPT_MENU_KEYS = 'образец|залог|жалобы|кодекс|порядок|размер|уголовн|расчет|кассационн';
    public const REPLACES = [
        'в котором( +УФК +по +субъекту +(РФ|Российской Федерации))? +открыт счет' => '',
        'Налоговый орган' => '',
        'ОКАТМО' => '',
        'ОКАТО\s*.?\s*\d*' => ''
    ];
    public const REQUIRED_WORDS = [
        'receiver_name' => 'УФК|МИФНС|рф|по|росссии|управлени',
        'bank_name' => 'отделени|нб|цб|россии|рф|ркц|республик',
        'target' => 'оплата|пошлин|суд',
    ];
    public const HOT_KEYS = [
        'receiver_name' => [
            'Наименование +получателя( +платежа)?',
            "П?олучатель( +платежа)?",
            'наименование уфк( +по +субъекту)',
            'Получатель платежа',
            'Получатель'],
        'inn' => ['ИНН получателя( +платежа.?)?', 'И\s*Н\s*Н'],
        'kpp' => ['КПП получателя','КПП'],
        'bank_name' => [
            'Наименование +банка( +получателя)?( +(средств|платежа))?',
            'Учреждение( +банка)?:?',
            'Банк( +|[^о])(получателя)?( +(средств|платежа))?',
            'Наименование банка','банк ', 'местонахождение +банка',
            'аименование    банка', '-получатель', 'анк:',],
        'bik' => ['БИК( +Банка).?'],
        'oktmo' => ['ОКТМО.?','ОКАТМО.?'],
        'account' => ['Расчетный счет','Расчётный    счет', 'Номер +счета( +банка( +получателя( +(\(ЕКС\))?)?)?)', 'Расчётный',
            'Расчетный', 'РАСЧЕТНЫЙ СЧЕТ', 'Счет','Р/счет', 'р/с','Сч. №'],
        'kbk' => ['КБК', 'Код бюджетной классификации'],
        'kazna_account' => ['Казн[^\s]+ +счет', 'Казначейск', 'казначейск', 'Сч. +№ +401', 'Номер +получателя +платежа'],
        'target' => ['Назначение платежа', 'Назначение:', 'Назначение','Наименование платежа',],
    ];
    public const NUMBER_LENGTHS = [
        'account' => 20,
        'kazna_account' => 20,
        'kbk' => 20,
        'inn' => 10,
        'kpp' => 9,
        'bik' => 9,
        'oktmo' => 8,
    ];
    public const NUMBER_START_REGEXPS = [
        'account' => ['031'],
        'kazna_account' => ['401'],
        'kbk' => ['182', '322'],
    ];
    public const  STRINGS = [
        'receiver_name',
        'bank_name',
        'source_url',
        'target',
        ];
    protected $fillable = [
        'receiver_name',
        'inn',
        'kpp',
        'account',
        'bank_name',
        'bik',
        'oktmo',
        'kbk',
        'kazna_account',
        'source_url',
        'file',
        'by_ifns',
    ];

    public function save(array $options = [])
    {
        if ($this->account === $this->kazna_account) $this->kazna_account = null;
        if (strpos($this->account, '401') !== false) {
            $k = $this->kazna_account;
            $this->kazna_account = $this->account;
            $this->account = $k;
        }
        return parent::save($options); // TODO: Change the autogenerated stub
    }

    /**
     * @return BelongsTo|Court
     */
    public function court() : BelongsTo
    {
        return $this->belongsTo(Court::class);
    }

    /**
     * @param null|string $text
     * @param string $regionCode
     */
    public function readNumbersFromText(?string $text, string $regionCode)
    {
        if (!$text) return;
        foreach ($lengths = CourtRequisite::NUMBER_LENGTHS as $field => $length) {
            if (!$starts = Arr::get(CourtRequisite::NUMBER_START_REGEXPS, $field)) {
                switch ($field) {
                    case 'kpp':
                        $starts = [$this->inn ? substr($this->inn, 0,2) : $regionCode];  //кпп начинается на теже цифры что и ИНН
                        break;
                    case 'inn':
                        $starts = [$regionCode, ''];
                        break;
                    default:
                        $starts = [''];
                };
            }
            foreach ($starts as $p){
                $length = $lengths[$field] - strlen($p);
                $p = preg_replace('/([0-9])/u', '$1\s*([^0-9\-\.\w]{1,10})?', $p);
                if (preg_match(("/[^0-9\s]{1,10}\s*(($p)([0-9]\s*([^0-9\-\.\w]{1,10})?){{$length}})\s*([^0-9]{1,10}|$)/u"), $text, $m)) {
                    $text = str_replace($m[1], '', $text);
                    $this->$field = mb_eregi_replace('[^0-9]', '', $m[1]);
                    if ($this->$field) break;
                }
            }
        }
    }

}

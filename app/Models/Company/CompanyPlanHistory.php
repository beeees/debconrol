<?php

namespace App\Models\Company;

use App\Models\ChangeHistory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Arr;

/**
 * Class CompanyPlanHistory
 * @package App\Models\Company
 * @property int company_plan_id
 * @property CompanyPlan companyPlan
 */
class CompanyPlanHistory extends ChangeHistory
{
    public const MODEL_TYPE = self::COMPANY_PLAN_MODEL_TYPE;
    protected $fillable = [
        'company_id', 'changes',
    ];

    /**
     * @return BelongsTo
     */
    public function companyPlan() : BelongsTo
    {
        return $this->belongModel(CompanyPlan::class);
    }
    public function toArray()
    {
        $arr =  parent::toArray();
        if (count($vals = array_values(Arr::only($arr, ['interval', 'interval_type']))))
        $arr['interval_name'] = $this->companyPlan->getIntervalName(
            Arr::get($arr, 'interval', $this->companyPlan->interval),
            Arr::get($arr, 'interval_type', $this->companyPlan->interval_type)
        );
        return $arr;
    }
}

<?php

namespace App\Models\Company;

use App\Models\AppModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;

/**
 * Class CompanyActivePlan
 * @package App\Models
 * @property Company company
 * @property CompanyPlan companyPlan
 * @property int company_id
 * @property int company_plan_id
 * @property Carbon expired_at
 * @property Carbon active_at
 * @method static static|Builder active(bool $withoutDates = false)
 */
class CompanyActivePlan extends AppModel
{
    use SoftDeletes;
    /**
     * @var self
     */
    private $preview = null;

    protected $fillable = [
        'expired_at',
        'active_at',
    ];

    protected $with = [
        'companyPlan'
    ];

    /**
     * @param Builder $builder
     * @param bool $withoutDates
     * @return Builder
     */
    public function scopeActive(Builder $builder, bool $withoutDates = false): Builder
    {
        if ($withoutDates) return $builder->whereNull('expired_at')->whereNull('active_at');
        return $builder->whereNotNull('expired_at')->whereNotNull('active_at')->where('expired_at', '>', Carbon::now());
    }

    /**
     * @return bool
     */
    public function isActive() : bool
    {
        return $this->active_at && $this->expired_at && $this->expired_at->gt(Carbon::now());
    }
    /**
     * @return BelongsTo
     */
    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * @return BelongsTo
     */
    public function companyPlan() : BelongsTo
    {
        return $this->belongsTo(CompanyPlan::class);
    }

    /**
     * @param CompanyActivePlan|null $old
     * @return array
     */
    public function getHistoryChanges() : array
    {
        $dirty = [];
        $same = $this->preview && $this->preview->id === $this->id;
        if (!$same || !$this->active_at || $this->preview && $this->active_at->ne($this->preview->active_at)
            || !$this->expired_at || $this->preview && $this->expired_at->ne($this->preview->expired_at))
            $dirty = ['expired_at', 'active_at'];
        if (!$same || $this->preview && $this->company_plan_id !== $this->preview->company_plan_id)
            $dirty[] = 'company_plan_id';
        return Arr::only($this->toArray(), $dirty);
    }

    /**
     * @return null
     */
    public function getPreview()
    {
        return $this->preview;
    }

    /**
     * @param null $preview
     */
    public function setPreview($preview): void
    {
        $this->preview = $preview;
    }
}
<?php

namespace App\Models\Company;

use App\Events\ModelUpdated;
use App\Exceptions\AppException;
use App\Exceptions\CompanyHasActivePlanException;
use App\Jobs\CompanyUsersBlockJob;
use App\Models\AppModel;
use App\Models\CanAuthUser;
use App\Models\Company\CompanyActivePlan;
use App\Models\Users\CompanyUser;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;

/**
 * Class Company
 * @package App\Models\Company
 * @property Collection|CompanyActivePlan[] plans
 * @property CompanyActivePlan activePlan
 * @property CompanyUser user
 * @property CompanyUser[]|Collection users
 * @property CompanyCase[]|Collection cases
 * @method static static|Builder byUser(int $id)
 * @method static static|Builder active()
 */
class Company extends AppModel
{
    public const HISTORY_CLASS = CompanyHistory::class;
    public const STORAGE_DIRECTORY = 'company/logo';
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'sort',
        'subdomain',
        'discount',
        'contact_user_name',
        'phone',
        'email',
        'url',
        'requisites',
        'is_active',
    ];

    public function fill(array $attributes)
    {
        if (($logo = Arr::get($attributes, 'logo'))  && $logo instanceof UploadedFile) {
            $this->logo = $logo->store($this->getStoreDirectory(), 'public')?: $this->logo;
        }
        return parent::fill($attributes);
    }

    /**
     * @return HasMany|CompanyActivePlan
     */
    public function plans() : HasMany
    {
        return $this->hasMany(CompanyActivePlan::class);
    }

    /**
     * @param bool $withoutDates
     * @return HasOne|CompanyActivePlan
     */
    public function activePlan(bool $withoutDates = false) : HasOne
    {
        return $this->hasOne(CompanyActivePlan::class)->active($withoutDates);
    }
    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeActive(Builder $builder): Builder
    {
        return $builder->where('is_active', true)->has('activePlan');
    }

    public function user() : BelongsTo
    {
        return $this->belongsTo(CompanyUser::class);
    }

    /**
     * @return BelongsToMany|CanAuthUser|CompanyUser
     */
    public function users() : BelongsToMany
    {
        return $this->belongsToMany(CompanyUser::class, 'company_users', 'company_id', 'user_id');
    }

    /**
     * @return HasMany|CompanyCase
     */
    public function cases() : HasMany
    {
        return $this->hasMany(CompanyCase::class);
    }

    /**
     * @param Builder $query
     * @param int $id
     * @return Builder
     */
    public function scopeByUser(Builder $query, int $id) : Builder
    {
        return $query->whereHas('users', function($q) use($id){
            $q->whereKey($id);
        });
    }

    /**
     * @param CompanyPlan|null $plan
     * @param Carbon $from|null
     * @param Carbon|null $expired
     * @return \App\Models\Company\CompanyActivePlan|null
     * @throws \Exception
     */
    public function getActivePlan(?CompanyPlan $plan, Carbon $from = null, ?Carbon $expired = null) : ?CompanyActivePlan
    {
        if (is_null($plan)) return null;
        if (($current = $this->activePlan) && $plan->id === $current->company_plan_id) {
            $current->setPreview(clone $current);
            $current->active_at = $from;
            $current->expired_at = $expired ?: ($from ? $plan->expiredFrom($from) : null);
            return $current;
        }
        $active = new CompanyActivePlan();
        $active->setPreview($current);
        $active->companyPlan()->associate($plan);
        $active->company()->associate($this);
        $active->active_at = $from;
        $active->expired_at = $expired ?:($from ? $plan->expiredFrom($from): null);
        return $active;
    }

    public function toArray()
    {
        $arr =  parent::toArray();
        if ($this->activePlan) {
            if ($this->activePlan->expired_at){
                $arr['expired_at'] = $this->activePlan->expired_at->format('Y-m-d');
                $arr['expired_at_time'] = $this->activePlan->expired_at->format('H:i');
            }
            if ($this->activePlan->active_at) {
                $arr['active_at'] = $this->activePlan->active_at->format('Y-m-d');
                $arr['active_at_time'] = $this->activePlan->active_at->format('H:i');
            }
        } else {
            $arr['active_plan'] = $this->activePlan(true)->first();
        }
        $arr['user'] = $this->user ? $this->user->only(['id', 'email']) : [];
        return $arr;
    }

    /**
     * @param array $options
     * @return bool
     * @throws \Exception
     */
    public function save(array $options = [])
    {
        $model = clone $this;
        if (parent::save($options)) {
            $model->id = $this->id;
            if (Arr::has($options, 'activePlan')) {
                $newPlan = Arr::get($options, 'activePlan');
                $oldPlan = $this->activePlan ?: $this->activePlan(true)->first();
                if ($newPlan) {
                    $newPlan->company_id = $this->id;
                    $newPlan->save();
                }
                if ($oldPlan && (!$newPlan || $oldPlan->id !== $newPlan->id)) $oldPlan->delete();
                $model->plan = $newPlan ? $newPlan->getHistoryChanges() :  null;
                //Если заблочили компанию или ее активный тариф
                if (($model->isDirty('is_active') && !$model->is_active) ||
                    (!$model->plan && $oldPlan || $model->plan && !$newPlan->isActive()))
                    dispatch(new CompanyUsersBlockJob($this));
            }
            ModelUpdated::dispatch($model);
            return true;
        }
        return false;
    }
}

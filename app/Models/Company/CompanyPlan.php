<?php

namespace App\Models\Company;

use App\Events\ModelUpdated;
use App\Exceptions\CantModifyCaseByPlanException;
use App\Jobs\CompanyUsersBlockJob;
use App\Models\AppModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;

/**
 * Class CompanyPlan
 * @package App\Models\Models\Company
 * @property int sort
 * @property string name
 * @property double cost
 * @property int interval
 * @property int interval_type
 * @property int cases
 * @property int records
 * @property boolean full_check
 * @property boolean assessed_check
 * @property boolean deal_check
 * @property boolean fssp_check
 * @property boolean is_active
 */
class CompanyPlan extends AppModel
{
    protected const HISTORY_CLASS = CompanyPlanHistory::class;
    public const DAY_INTERVAL = 0;
    public const WEEK_INTERVAL = 1;
    public const MONTH_INTERVAL = 2;
    public const YEAR_INTERVAL = 3;
    public const INTERVAL_NAMES = [
        self::DAY_INTERVAL => 'день',
        self::WEEK_INTERVAL => 'неделя',
        self::MONTH_INTERVAL => 'месяц',
        self::YEAR_INTERVAL => 'год',
    ];
    use SoftDeletes;

    protected $fillable = [
        'sort', 'name', 'cost', 'interval','interval_type',
        'cases', 'records', 'full_check', 'assessed_check', 'deal_check', 'is_active',
    ];

    /**
     * @param array $attributes
     * @return AppModel|\Illuminate\Database\Eloquent\Model|void
     */
    public function fill(array $attributes)
    {
        /** @var self $result */
        $result = parent::fill($attributes);
        $result->fssp_check = $this->fssp_check && $this->deal_check;
        return $result;
    }

    /**
     * @param int|null $interval
     * @param int|null $interval_type
     * @return string
     */
    public function getIntervalName(?int $interval = null, ?int $interval_type = null) : string
    {
        $stringName = '';
        $interval = $interval ?? $this->interval;
        $intervalValue = $interval < 21 ? $interval : $interval % 10;
        switch ($interval_type ?? $this->interval_type) {
            case self::DAY_INTERVAL:
                if ($intervalValue == 1) $stringName = 'день';
                else if ($intervalValue >1 && $intervalValue < 5) $stringName = 'дня';
                else $stringName = 'дней';
                break;
            case self::WEEK_INTERVAL:
                if ($intervalValue == 1) $stringName = 'неделя';
                else if ($intervalValue >1 && $intervalValue < 5) $stringName = 'недели';
                else $stringName = 'недель';
                break;
            case self::MONTH_INTERVAL:
                if ($intervalValue == 1) $stringName = 'месяц';
                else if ($intervalValue >1 && $intervalValue < 5) $stringName = 'месяца';
                else $stringName = 'месяцев';
                break;
            case self::YEAR_INTERVAL:
                if ($intervalValue == 1) $stringName = 'год';
                else if ($intervalValue >1 && $intervalValue < 5) $stringName = 'года';
                else $stringName = 'лет';
                break;
        }
        return $interval . ' ' . $stringName;
    }
    public function toArray()
    {
        $this->interval_name = $this->getIntervalName();
        return parent::toArray();
    }

    /**
     * @param array $options
     * @return bool
     */
    public function save(array $options = [])
    {
        if ($this->isDirty($fields = ['interval', 'interval_type']))
            Arr::forget($this->original, $fields);
        $model = clone $this;
        if (parent::save($options)) {
            $model->id = $this->id;
            ModelUpdated::dispatch($model);
            if ($model->isDirty('is_active') && !$model->is_active)
                $model->blockRelatedCompanies();
            return true;
        }
        return false;
    }

    public function blockRelatedCompanies()
    {
        foreach (Company::active()->whereHas('activePlan', function($q) {
            $q->where('company_plan_id', $this->id);
        }) as $c)
            dispatch(new CompanyUsersBlockJob($c));
    }

    /**
     * @param CompanyCase $case
     * @throws CantModifyCaseByPlanException
     */
    public function checkCaseAccess(CompanyCase $case) : void
    {
        if (($this->assessed_check && $case->check_type === CompanyCase::ASSESSED_CHECK_TYPE
            || $this->full_check && $case->check_type === CompanyCase::FULL_CHECK_TYPE
            || $this->deal_check && $case->check_type === CompanyCase::DEAL_CHECK_TYPE
            || $this->fssp_check && $case->check_type === CompanyCase::FSSP_CHECK_TYPE))
                return;
        throw new CantModifyCaseByPlanException();
    }

    /**
     * @param Carbon $from
     * @return Carbon
     */
    public function expiredFrom(Carbon $from): Carbon
    {
        $from = clone $from;
        switch ($this->interval_type) {
            case self::DAY_INTERVAL:
                $from->addDays($this->interval);
                break;
            case self::WEEK_INTERVAL:
                $from->addWeeks($this->interval);
                break;
            case self::MONTH_INTERVAL:
                $from->addMonths($this->interval);
                break;
            case self::YEAR_INTERVAL:
                $from->addYears($this->interval);
                break;
        }
        return $from;
    }
}

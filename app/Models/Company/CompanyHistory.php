<?php

namespace App\Models\Company;

use App\Models\ChangeHistory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class CompanyHistory
 * @package App\Models\Company
 * @property Company company
 */
class CompanyHistory extends ChangeHistory
{
    public const MODEL_TYPE = self::COMPANY_MODEL_TYPE;
    protected $fillable = [
        'company_id', 'changes',
    ];

    /**
     * @return BelongsTo
     */
    public function company() : BelongsTo
    {
        return $this->belongModel(Company::class);
    }
}
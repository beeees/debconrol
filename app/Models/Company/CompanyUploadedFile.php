<?php

namespace App\Models\Company;

use App\Models\AppModel;
use App\Models\CanAuthUser;
use App\Models\Debtor;
use App\Models\Users\CompanyUser;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;

/**
 * Class CompanyUploadedFile
 * @package App\Models\Company
 * @property CompanyUser user
 * @property CompanyCase companyCase
 * @property Company company
 * @property Debtor[]|Collection debtors
 * @property int items
 * @property int progress_items
 * @property string task_type
 * @property string task_code
 * @property string file
 * @property int case_id
 */
class CompanyUploadedFile extends AppModel
{
    public const STORAGE_DIRECTORY = 'company/files';
    use SoftDeletes;
    public $timestamps = [
        'finished_at'
    ];
    protected $fillable = [
        'format', 'progress_items', 'items', 'finished_at','batch',
    ];

    public function fill(array $attributes)
    {
        if (($file = Arr::get($attributes, 'file'))  && $file instanceof UploadedFile) {
            $this->file = $file->store($this->getStoreDirectory(), 'public')?: $this->file;
        }
        return parent::fill($attributes);
    }

    /**
     * @return BelongsTo|CompanyUser
     */
    public function user() : BelongsTo
    {
        return $this->belongsTo(CompanyUser::class, 'user_id');
    }

    /**
     * @return BelongsTo|CompanyCase
     */
    public function companyCase() : BelongsTo
    {
        return $this->belongsTo(CompanyCase::class, 'case_id');
    }

    /**
     * @return HasOneThrough|Company
     */
    public function company() : HasOneThrough
    {
        return $this->hasOneThrough(Company::class, CompanyCase::class, 'company_id', 'case_id');
    }

    /**
     * @return HasMany|Debtor
     */
    public function debtors() : HasMany
    {
        return $this->hasMany(Debtor::class);
    }

    /**
     * @return HasMany|Debtor
     */
    public function badDebtors() : HasMany
    {
        return $this->hasMany(Debtor::class)->whereNotNull('error');
    }

    /**
     *
     */
    public function updateItems() : self
    {
        $this->items = $this->debtors()->count();
        return $this;
    }


}

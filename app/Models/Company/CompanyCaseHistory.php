<?php

namespace App\Models\Company;

use App\Models\ChangeHistory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Arr;

/**
 * Class CompanyCaseHistory
 * @package App\Models\Company
 * @property CompanyCase companyCase
 */
class CompanyCaseHistory extends ChangeHistory
{
    public const MODEL_TYPE = self::COMPANY_CASE_TYPE;
    protected $fillable = [
        'company_id', 'changes',
    ];

    /**
     * @return BelongsTo
     */
    public function companyCase() : BelongsTo
    {
        return $this->belongModel(CompanyCase::class);
    }
}

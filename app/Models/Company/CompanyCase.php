<?php

namespace App\Models\Company;

use App\Events\ModelUpdated;
use App\Models\AppModel;
use App\Models\CanAuthUser;
use App\Models\Debtor;
use App\Models\DebtorCheckResult;
use App\Models\Users\CompanyUser;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CompanyCase
 * @package App\Models\Company
 * @property Company company
 * @property int company_id
 * @method static Builder|static byUser(CanAuthUser $user)
 * @property Collection|CompanyUploadedFile[] files
 * @property Collection|Debtor[] debtors
 * @property Carbon checked_at
 * @property int check_type
 * @property int related_case_id
 * @property bool fssp_check_option
 * @property bool deal_check_option
 */
class CompanyCase extends AppModel
{
    public const HISTORY_CLASS = CompanyCaseHistory::class;
    public const HOURS_PER_CHECK_DEBTOR = 72;
    public const ASSESSED_CHECK_TYPE = 0;
    public const FULL_CHECK_TYPE = 1;
    public const DEAL_CHECK_TYPE = 2;
    public const FSSP_CHECK_TYPE = 3;
    public const CHECK_TYPES = [
        self::ASSESSED_CHECK_TYPE => 'Оценка',
        self::FULL_CHECK_TYPE => 'Полная',
        self::DEAL_CHECK_TYPE => 'Судебные дела',
        self::FSSP_CHECK_TYPE => 'Проверка по ФССП',
    ];
    use SoftDeletes;

    protected $table = 'cases';

    protected $fillable = [
        'sort',
        'name',
        'is_active',
        'check_type',
        'checked_at',
        'related_case_id',
        'fssp_check_option',
        'deal_check_option',
    ];

    public $timestamps = [
        'checked_at'
    ];

    /**
     * @return BelongsTo|Company
     */
    public function company() : BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * @param Builder $builder
     * @param CanAuthUser
     * @return Builder
     */
    public function scopeByUser(Builder $builder, CanAuthUser $user) : Builder
    {
        return $builder->whereIn('company_id', $user->companies()->get(['companies.id'])->pluck('id')->toArray());
    }

    /**
     * @return HasMany
     */
    public function files() : HasMany
    {
        return $this->hasMany(CompanyUploadedFile::class, 'case_id');
    }

    /**
     * @return HasMany|Debtor
     */
    public function debtors() : HasMany
    {
        return $this->hasMany(Debtor::class, 'case_id');
    }

    public function save(array $options = [])
    {
        $model = clone $this;
        if (parent::save($options)) {
            $model->id = $this->id;
            ModelUpdated::dispatch($model);
            return true;
        }
        return false;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return parent::toArray() + [
            'checked_available_timestamp' => $this->checked_at ? $this->checked_at->addHours(self::HOURS_PER_CHECK_DEBTOR)->timestamp : null,
        ];
    }
}

<?php

namespace App\Models;

use App\Models\Company\Company;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Arr;

/**
 * Class DebtorDeal
 * @package App\Models
 * @property string  act_number
 * @property string  court_urls
 * @property Carbon  deal_init_date
 * @property string|null  deal_number
 * @property Carbon|null  deal_start_date
 * @property Carbon|null  deal_end_date
 * @property string|null  deal_verdict
 * @property string|null  deal_initiator
 * @property string|null  loaded_initiator
 * @property string|null  deal_link
 * @property Debtor debtor
 * @property int fs_number
 * @method static Builder|static finished()
 */
class DebtorDeal extends Model
{
    use HasFactory,HasFsNumber;
    public $timestamps = false;
    protected $fillable = [
        'act_number',
        'court_urls',
        'loaded_initiator',
        'deal_init_date',
        'deal_number',
        'deal_start_date',
        'deal_end_date',
        'deal_verdict',
        'deal_initiator',
        'deal_link',
        'fs_number',
    ];
    protected $dates = [
        'deal_start_date',
        'deal_end_date',
        'deal_init_date',
    ];
    protected $touches = [
        'debtor',
    ];

    /**
     * @return BelongsTo
     */
    public function debtor() : BelongsTo
    {
        return $this->belongsTo(Debtor::class);
    }
    public function scopeFinished(Builder $builder) : Builder
    {
        return $builder->where(function($q) {
            $q->orWhereNotNull('deal_initiator')
                ->orWhereNotNull('deal_verdict')
                ->orWhereNotNull('deal_link')
                ->orWhereNotNull('deal_start_date')
                ->orWhereNotNull('deal_end_date');
        });
    }

    /**
     * @return array
     */
    public function getCasts()
    {
        return array_merge($this->casts, [
            'deal_start_date' => 'datetime:d.m.Y',
            'deal_init_date' => 'datetime:d.m.Y',
            'deal_end_date' => 'datetime:d.m.Y',
        ]);
    }

    /**
     * @return null|string
     */
    public function getParseDomain() : ?string
    {
        preg_match('/(https?:)?(\/\/)?([^\s\/\?]+)+/u', $this->court_urls??'', $domains);
        return Arr::first($domains);
    }

    /**
     * @return array
     */
    public function getDealNumberSearchParams() :array
    {
        $deals = [$this->deal_number ?: ''];
        if ($this->deal_number){
            $m = [];
            if (preg_match('/([\d\-\w]+)(\/)(\d\d\d\d)(.*)/ui', $this->deal_number, $m)) {
                $deals[] = $m[1].$m[2].$m[3];
                $deals[] = $m[1];
                $deals = array_unique($deals);
            }
        }
        return $deals;
    }

    /**
     * @param Company|null $company
     * @return string
     */
    public function getInitiatorSearchParams(?string $companyName = null) :string
    {
        $param = $this->loaded_initiator ? $this->loaded_initiator : ($companyName ? $companyName : '');
        if (preg_match('/"(.*)")/', $param, $m)) {
            $param = $m[1];
        }
        return $param;
    }

    /**
     * @return bool
     */
    public function checkSameInitiator() : bool
    {
        $cn = Company::query()->whereHas('cases', function($q) {
            $q->whereHas('debtors', function($q2) {
                $q2->whereKey($this->debtor_id);
            });
        })->first()->name;
        $li = $this->loaded_initiator;
        $di = $this->deal_initiator;
        $cli = preg_replace('/(\'|")/', '', $li);//cleared loaded initiator
        $ccn = preg_replace('/(\'|")/', '', $cn); //cleared company name
        $result = (!$di && !$li)
            || $li && $di && ((mb_stripos($li , $di) !== false) || (mb_stripos($di , $li) !== false))
            || $li && $di && ((mb_stripos($cli , $di) !== false) || (mb_stripos($di , $cli) !== false))
            || $li && (mb_stripos($li, $cn) !== false || mb_stripos($cli, $cn) !== false)
            || !$li && $di && (mb_stripos($di, $cn) !== false || mb_stripos($di, $ccn) !== false)
            || (mb_stripos($cn, $di . $li) !== false || mb_stripos($cn, $di . $cli) !== false )
            || (mb_stripos($ccn, $di . $li) !== false || mb_stripos($ccn, $di . $cli) !== false )
            || !$di;
        if ($result && $di) $this->deal_initiator = $li ?: $cn;
        return $result;
    }

    /**
     * @param array $options
     * @return bool
     */
    public function save(array $options = [])
    {
        if ($this->isDirty('fs_number')) $this->fs_number = $this->formatFsNumber($this->fs_number?:'');
        return parent::save($options);
    }

}

<?php

namespace App\Models;

use App\Models\DebtorFsspDeal;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class ParserTask
 * @package App\Models
 * @property string task_code
 * @property string task_type
 * @property int progress_items`
 * @property int items
 */
class ParserTask extends AppModel
{
    use HasFactory;
    protected $fillable = [
        'task_type',
        'task_code',
        'finished_at',
        'items',
        'progress_items',
    ];
    public $timestamps = [
        'finished_at',
    ];
    /**
     * @return BelongsToMany|DebtorFsspDeal|DebtorFsspDeal[]
     */
    public function debtorsFsspDeals() : BelongsToMany
    {
        return $this->belongsToMany(DebtorFsspDeal::class, 'fssp_tasks');
    }
}

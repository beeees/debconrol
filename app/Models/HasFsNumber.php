<?php

namespace App\Models;

/**
 * Trait HasFsNumber
 * @package App\Models
 */
trait HasFsNumber
{
    public static function formatFsNumber(string $number) {
        return str_replace('№', '', $number);
    }
}
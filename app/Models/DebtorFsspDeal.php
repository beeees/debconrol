<?php

namespace App\Models;

use App\Models\Debtor;
use App\Models\HasFsNumber;
use App\Models\ParserTask;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class DebtorFsspDeal
 * @package App\Models\Company
 * @property string deal_number
 * @property string fs_number
 * @property bool closed
 */
class DebtorFsspDeal extends Model
{
    use HasFactory, HasFsNumber;
    public $timestamps = false;

    protected $fillable = ['deal_number', 'fs_number', 'closed'];

    protected $touches = [
        'debtor',
    ];
    public const STATE_FIND = 0;
    public const STATE_NOT_FIND = 1;
    public const STATE_FINISHED = 2;
    public const STATE_SD = 3;
    public const STATE_SV = 4;
    /**
     * @return BelongsTo
     */
    public function debtor() : BelongsTo
    {
        return $this->belongsTo(Debtor::class);
    }

    /**
     * @return BelongsToMany|ParserTask|ParserTask[]
     */
    public function parserTasks() : BelongsToMany
    {
        return $this->belongsToMany(ParserTask::class, 'fssp_tasks');
    }

    /**
     * @param array $options
     * @return bool
     */
    public function save(array $options = [])
    {
        if ($this->isDirty('fs_number')) $this->fs_number = $this->formatFsNumber($this->fs_number?:'');
        return parent::save($options);
    }
}

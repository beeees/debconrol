<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Auth;

/**
 * Class ChangeHistory
 * @package App\Models
 * @property string changes
 * @property CanAuthUser changer
 * @property int author_id
 * @property int model_id
 * @property int model_type
 */
class ChangeHistory extends AppModel
{
    public const MODEL_TYPE = null;
    public const USER_MODEL_TYPE=1;
    public const COMPANY_PLAN_MODEL_TYPE=2;
    public const COMPANY_MODEL_TYPE=3;
    public const COMPANY_CASE_TYPE=4;
    public const SERVICE_USER_CHANGER_NAME = 'Администрация' ;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('model_type_scope', function (Builder $builder) {
            if (static::MODEL_TYPE !== null)
                $builder->where('model_type', static::MODEL_TYPE);
            return $builder;
        });
    }

    public function getTable()
    {
        return 'change_histories';
    }

    /**
     * @param array $changes
     * @return bool
     */
    public function loadChangesFromArray(array $changes) : bool
    {
        $this->setAttribute('changes',  json_encode($changes));
        return (bool) $changes;
    }

    /**
     * @param array $attributes
     * @return $this
     */
    public function fill(array $attributes)
    {
        $this->model_type = static::MODEL_TYPE;
        return parent::fill($attributes);
    }

    /**
     * @return BelongsTo
     */
    public function author() : BelongsTo
    {
        return $this->belongsTo(CanAuthUser::class);
    }

    /**
     * @param string $classname
     * @return BelongsTo
     */
    protected function belongModel(string $classname) : BelongsTo
    {
        return $this->belongsTo($classname, 'model_id', 'id');
    }

    /**
     * @param array $options
     * @return bool
     */
    public function save(array $options = [])
    {
        if (!$this->author_id) $this->author_id = Auth::id();
        return parent::save($options);
    }

    public function toArray()
    {
        $arr = parent::toArray();
        $arr['author'] = $this->changer && $this->changer->isCompanyUser()
            ? $this->changer->name
            : self::SERVICE_USER_CHANGER_NAME;
        $arr = array_merge($arr, json_decode($this->getAttribute('changes'), true) ?? []);
        return $arr;
    }
}

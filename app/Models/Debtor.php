<?php

namespace App\Models;

use App\Models\Catalog\Court;
use App\Models\Catalog\Region;
use App\Models\Company\Company;
use App\Models\Company\CompanyCase;
use App\Models\Company\CompanyUploadedFile;
use App\Models\DebtorFsspDeal;
use App\Models\DebtorException;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Expression;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

/**
 * Class Debtor
 * @package App\Models
 * @property CompanyCase companyCase
 * @property Company company
 * @property CompanyUploadedFile file
 * @property Court[]|Collection courts
 * @property DebtorDeal[]|Collection deals
 * @property DebtorFsspDeal[]|Collection fsspDeals
 * @property Region|null region
 * @property int case_id
 * @method static Builder|static byFio(string $fio)
 * @method static Builder|static byAddress(string $address)
 * @method static Builder|static readyForCheckCourt()
 */
class Debtor extends AppModel
{
    use SoftDeletes;
    public const ERROR_BAD_NAME = 0;
    public const ERROR_NO_ADDRESS = 1;
    public const ERROR_NO_NAME = 2;
    public const ERROR_BAD_ADDRESS = 3;
    public const ERROR_NO_DEAL_URL = 4;
    public const ERROR_UNKNOWN_PARSER = 5;
    public const ERROR_PARSE_FAIL = 6;
    public const ERROR_NO_DEAL_DATA = 7;
    public const ERROR_NO_DEAL_DETAILS_LOAD = 8;

    public static function error(int $e) {
        return Arr::get([
            self::ERROR_NO_ADDRESS => 'Остутствует адрес',
            self::ERROR_BAD_ADDRESS => 'Нераспознанный адрес',
            self::ERROR_NO_NAME => 'Отсутствует имя',
            self::ERROR_BAD_NAME => 'Необходимо подтвердить корректность ФИО',
            self::ERROR_NO_DEAL_URL => 'Отсутствует информация о сайте',
            self::ERROR_UNKNOWN_PARSER => 'Не удалось распарсить сайт суда',
            self::ERROR_PARSE_FAIL => 'Ошибка при обработке данных',
            self::ERROR_NO_DEAL_DATA => 'Не удалось найти данные по должнику на сайте суда',
            self::ERROR_NO_DEAL_DETAILS_LOAD => 'Не удалось загрузить подробную  информацию о деле',
        ], $e, null);
    }
    public const HOURS_PER_CHECK_DEBTOR = 72;
    public const DEBTORS_TABLE_COLUMNS_NAMES = [
        CompanyCase::ASSESSED_CHECK_TYPE => [
            'ip' => 'Количество ИП',
            'sum' => 'Сумма по ИП',
            'regions' => 'Кол-во регионов с ИП',
            'ended' => 'Кол-во прекращенных ИП',
            'repeatly' => 'Кол-во повторных ИП',
            'gibdd' => 'Кол-во ИП(ГИБДД)',
            'gibdd_sum' => 'Сумма ИП (ГИБДД)',
            'zkh' => 'Кол-во ИП(ЖКХ)',
            'zkh_sum' => 'Сумма ИП (ЖКХ)',
            'credit' => 'Кол-во ИП(Кредиты)',
            'credit_sum' => 'Сумма ИП (Кредиты)',
            'ipoteka' => 'Кол-во ИП(Ипотека)',
            'ipoteka_sum' => 'Сумма ИП (Ипотека)',
        ],
        CompanyCase::FULL_CHECK_TYPE => [
            'court_name' => 'Суд',
            'court_address' => 'Адрес суда',
            'court_phone' => 'Телефон суда',
            'court_inn' => 'ИНН',
            'court_kpp' => 'КПП',
            'court_bik' => 'БИК',
            'court_account' => 'Счет',
            'court_kbk' => 'КБК',
            'court_kazna_account' => 'Казначейский счет',
            'court_oktmo' => 'ОКТМО',
            'court_bank_name' => 'Банк',
            'court_receiver_name' => 'Получатель',
        ],
        CompanyCase::DEAL_CHECK_TYPE => [
            'deal_act_number' => 'Номер договора',
            'deal_court_urls' => 'Сайт суда',
            'deal_deal_initiator' => 'Истец',
            'deal_deal_init_date' => 'Дата подачи в суд',
            'deal_deal_number' => 'Номер дела',
            'deal_fs_number' => 'Номер ФС или ВС',
            'deal_deal_start_date' => 'Дата дела',
            'deal_deal_end_date' => 'Дата завершения',
            'deal_deal_verdict' => 'Судебное решение',
        ],
        CompanyCase::FSSP_CHECK_TYPE => [
            'check_result_requisites' => 'Реквизиты исполнительного документа',
            'check_result_ip_number' => 'Номер ИП',
            'check_result_id_number' => 'Номер исполнительного документа',
            'check_result_ip_date' => 'Дата ИП',
            'check_result_ip_summary_case' => 'СВ или СД',
            'check_result_subject_name' => 'Предмет исполнения',
            'check_result_date_reason' => 'Дата и причина окончания ИП',
            'check_result_subject_amount' => 'Сумма задолженности',
            'check_result_bailiff_department_name' => 'Название ОСП',
            'check_result_bailiff_fullname' => 'ФИО пристава',
            'check_result_bailiff_phones' => 'Телефоны пристава',
            'check_result_ip_end_date' => 'Дата завершения',
        ]
    ];
    public const RESULT_COLUMNS_NAMES = [
        'ip_number' => 'Номер ИП',
        'ip_date' => 'Дата ИП',
        'id_type' => 'Вид, дата принятия ИД',
        'id_number' => 'Номер ИД',
        'id_issuer' => 'Наименование органа, выдавшего ИД',
        'ip_end_date' => 'Дата окончания ИП',
        'ip_end_reason' => 'Причина окончания ИП',
        'subject_name' => 'Предмет исполнения',
        'subject_amount' => 'Сумма непогашенной задолженности',
        'bailiff_department_name' => 'Отдел судебных приставов',
        'bailiff_fullname' => 'ФИО пристава-исполнителя',
        'region' => 'Регион/область',
    ];
    protected $fillable = [
        'name',
        'lastname',
        'secondname',
        'mark',
        'finished_at',
        'regionCode',
        'birthday',
        'loaded_address',
        'address',
        'error',
        'fio',
    ];
    public $timestamps = [
        'birthday',
        'finished_at',
    ];

    public function getFillable()
    {
        return  array_merge($this->fillable, array_keys(self::DEBTORS_TABLE_COLUMNS_NAMES[0]));
    }

    /**
     * @return array
     */
    public function getFioSearchParams() : array
    {
        $parts = explode(' ', $this->getFullNameAttribute());
        $fios = [
            $this->getFullNameAttribute()
        ];
        if (isset($parts[1])) $parts[1] = mb_strtoupper(mb_substr($parts[1], 0,1)) . '.';
        if (isset($parts[2])) $parts[2] = mb_strtoupper(mb_substr($parts[2], 0,1)) . '.';
        $fios[] = implode(' ', $parts);
        if (count($parts) > 1)
            $fios[] = $parts[0] .' '. Arr::get($parts, 1, '').Arr::get($parts,2,'');
        $fios[] = $parts[0];
        return array_unique($fios);
    }

    /**
     * @param int $e
     */
    public function setError(int $e)
    {
        $errors = json_decode($this->error, true) ?? [];
        $errors[] = $e;
        $this->error = json_encode(array_unique($errors));
    }
     /**
     * @return BelongsTo|CompanyUploadedFile
     */
    public function file() : BelongsTo
    {
        return $this->belongsTo(CompanyUploadedFile::class, 'company_uploaded_file_id');
    }

    /**
     * @return BelongsTo|CompanyCase
     */
    public function companyCase() : BelongsTo
    {
        return $this->belongsTo(CompanyCase::class, 'case_id');
    }

    /**
     * @return HasOneThrough|Company
     * @deprecated
     */
    public function company() : HasOneThrough
    {
        return $this->hasOneThrough(Company::class, CompanyCase::class, 'company_id', 'case_id');
    }

    /**
     * @return BelongsToMany
     */
    public function checkResults() : BelongsToMany
    {
        return $this->belongsToMany(DebtorCheckResult::class, 'debtors_results');
    }

    /**
     * @return HasOne
     */
    public function dadataFio() : HasOne
    {
        $instance = $this->newRelatedInstance(DadataFioRecord::class);
        $foreignKey = 'fio';
        $localKey = 'fio';
        return $this->newHasOne($instance->newQuery(), $this,  $instance->getTable().'.'.$foreignKey, DB::raw("lower(debtors.$localKey)"));
    }

    /**
     * @return HasOne
     */
    public function dadataAddress() : HasOne
    { $instance = $this->newRelatedInstance(DadataAddressRecord::class);

        $foreignKey = 'address';

        $localKey = 'loaded_address';

        return $this->newHasOne($instance->newQuery(), $this, $instance->getTable().'.'.$foreignKey, DB::raw("lower(debtors.$localKey)"));
    }

    /**
     * @return BelongsToMany|Court
     */
    public function courts() : BelongsToMany
    {
        return $this->belongsToMany(Court::class, 'debtor_courts', 'debtor_id', 'court_id')
            ->withTrashed();
    }

    /**
     * @return HasMany|DebtorDeal
     */
    public function deals() : HasMany
    {
        return $this->hasMany(DebtorDeal::class)->orderBy('id');
    }

    /**
     * @return HasMany|DebtorFsspDeal
     */
    public function fsspDeals() : HasMany
    {
        return $this->hasMany(DebtorFsspDeal::class)->orderBy('id');
    }
    /**
     * @return HasMany|DebtorException
     */
    public function exceptions() : HasMany
    {
        return $this->hasMany(DebtorException::class);
    }

    /**
     * @return BelongsTo|Region
     */
    public function region() : BelongsTo
    {
        return $this->belongsTo(Region::class, 'regionCode', 'code');
    }

    /**
     * Get an attribute from the model.
     *
     * @param  string  $key
     * @return mixed
     */
    public function getAttribute($key)
    {
        if (! $key) {
            return;
        }
        if ($key instanceof Expression) {
            if ($key->getValue() === 'lower(debtors.loaded_address)') return trim(mb_strtolower($this->getAttribute('loaded_address')));
            if ($key->getValue() === 'lower(debtors.fio)') return trim(mb_strtolower($this->getAttribute('fio')));
        }
        if (array_key_exists($key, $this->attributes) ||
            array_key_exists($key, $this->casts) ||
            $this->hasGetMutator($key) ||
            $this->isClassCastable($key)) {
            return $this->getAttributeValue($key);
        }
        if (method_exists(self::class, $key)) {
            return;
        }

        return $this->getRelationValue($key);
    }
    /**
     * @param Builder $query
     * @param string $fio
     * @return Builder
     */
    public function scopeByFio(Builder $q, string $fio) : Builder
    {
        $fio = '(' . implode('|' , preg_split('/\W+/ui', $fio)). ')';
        return $q->where(function($query) use ($fio) {
            $query->orWhereRaw('concat(lastname,name,secondname,fio) ~* \'' . $fio .'\'');
        });
    }
    /**
     * @param Builder $query
     * @param string $address
     * @return Builder
     */
    public function scopeByAddress(Builder $query, string $a) : Builder
    {
        $a = mb_strtolower($a);
        $query->where(function($q) use ($a){
            $parts = explode(' ', trim($a));
            foreach ($parts as $i => $part) if ($part) {
                $q->where(DB::raw('lower(concat(loaded_address,\' \', address))'), '~*', "$part", 'or');
            }
        });
        return $query;
    }

    public function getFullNameAttribute() {
        return trim($this->secondname . ' ' . $this->name . ' '. $this->lastname) ?: $this->fio;
    }
    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeReadyForCheckCourt(Builder $builder) : Builder
    {
        return $builder->where(function($q) {
            $q->orWhereNull('error')
            ->orWhere('error', (json_encode([Debtor::ERROR_BAD_NAME])));
        });
    }

    /**
     * @return Debtor
     */
    public function finish() : self
    {
        $this->finished_at = Carbon::now();
        return $this;
    }
    public function toArray()
    {
        $arr = parent::toArray();
        $arr['error']= null;
        foreach (json_decode($this->error)?:[] as $e) {
            $arr['error'][] = self::error($e);
        }
        if ($arr['check_name']  = array_search(self::ERROR_BAD_NAME, $arr['error'] ??[]) !== false)
            $arr['dadata_fio'] = Arr::get($arr, 'dadata_fio', $this->dadataFio()->first());
        $arr['full_name'] = $this->getFullNameAttribute()?: '';
        return $arr;
    }
}

<?php

namespace App\Exceptions;

use Exception;

class CompanyHasActivePlanException extends AppException
{
    public function __construct(?string $message = null, int $responseCode = 400)
    {
        $message = $message ?? trans('responses.exceptions.company.repeatPlan');
        parent::__construct($message, $responseCode);
    }
}

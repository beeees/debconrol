<?php
/**
 * Created by PhpStorm.
 * User: jura120596
 * Date: 3/12/21
 * Time: 4:11 PM
 */

namespace App\Exceptions;


use App\Models\Company\CompanyCase;
use Carbon\Carbon;

class DebtorsCheckUnavailableException extends AppException
{
    public function __construct(?string $message = null, int $responseCode = 400)
    {
        $message = $message ?? trans('responses.exceptions.case.check_unavailable', [
            'time' => Carbon::now()->addHours(CompanyCase::HOURS_PER_CHECK_DEBTOR)->format('d.m.Y H:i')
        ]);
        parent::__construct($message, $responseCode);
    }

}
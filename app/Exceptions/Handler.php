<?php

namespace App\Exceptions;

use App\Jobs\ErrorReportJob;
use Carbon\Carbon;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Throwable $e
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws Throwable
     */
    public function render($request, Throwable $e)
    {
        if ($e instanceof ValidationException) {
            throw new HttpResponseException(response([
                'message' => trans('responses.exceptions.validation.default'),
                'errors' => $e->errors(),
            ], $e->status));
        }
        //404 ошибки отправляем только в 12-ом часу, иначе их приходит много, можно совсем заглушить
        if (!($e instanceof NotFoundHttpException) || Carbon::now()->hour === 12)
            dispatch(new ErrorReportJob($e, (request() ? request()->getRequestUri() . '\n' : ''), true));
        return parent::render($request, $e);
    }
}

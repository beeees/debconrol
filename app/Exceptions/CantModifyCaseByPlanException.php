<?php

namespace App\Exceptions;

use Exception;

class CantModifyCaseByPlanException extends AppException
{
    public function __construct(?string $message = null, int $responseCode = 400)
    {
        $message = $message ?? trans('responses.exceptions.case.canModify');
        parent::__construct($message, $responseCode);
    }
}

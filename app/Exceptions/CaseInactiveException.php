<?php

namespace App\Exceptions;

use Exception;

class CaseInactiveException extends AppException
{
    public function __construct(?string $message = null, int $responseCode = 400)
    {
        $message = $message ?? trans('responses.exceptions.case.inactive');
        parent::__construct($message, $responseCode);
    }
}

<?php
namespace App\Exceptions;

/**
 * Class LargeHtmlException
 * @package App\Exceptions
 */
class LargeHtmlException extends AppException
{
    public function __construct(?string $message = null, int $responseCode = 400)
    {
        $message = $message ?? trans('responses.exceptions.case.large_html');
        parent::__construct($message, $responseCode);
    }
}
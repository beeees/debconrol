<?php
namespace App\Exceptions;

/**
 * Class UnknownCaseCheckTypeException
 * @package App\Exceptions
 */
class UnknownCaseCheckTypeException extends AppException
{
    public function __construct(?string $message = null, int $responseCode = 500)
    {
        $message = $message ?? trans('responses.exceptions.case.unknown_case_check_type');
        parent::__construct($message, $responseCode);
    }
}
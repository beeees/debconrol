<?php
namespace App\Exceptions;

/**
 * Class BadDebtorsFileFormatException
 * @package App\Exceptions
 */
class BadDebtorsFileFormatException extends AppException
{
    public function __construct(?string $message = null, int $responseCode = 400)
    {
        $message = $message ?? trans('responses.exceptions.case.bad_file');
        parent::__construct($message, $responseCode);
    }
}
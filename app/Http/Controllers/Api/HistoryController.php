<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\CanAuthUser;
use App\Models\Company\Company;
use App\Models\Company\CompanyActivePlan;
use App\Models\Company\CompanyCase;
use App\Models\Company\CompanyPlan;
use App\Models\Users\CompanyUser;
use App\Models\Users\ServiceUser;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HistoryController extends Controller
{
    /**
     * @var ServiceUser|CompanyUser
     */
    private $user;
    public function __construct()
    {
        $this->middleware('auth:auth-api')->except(['user']);
        $this->user = Auth::user();
    }

    public function index(string $domain, string $model, int $id) : JsonResponse
    {
        return $this->$model($id);
    }

    public function user(int $id) : JsonResponse
    {
        $query = CanAuthUser::query();
        if (!$this->isAdmin()) $query = CompanyUser::query();
        if ($this->isBoss()) $query->whereHas('companies', function($q) {
            $q->byUser(Auth::id());
        });
        if(!$this->isBoss() && Auth::user()->is_company_user) $list = null;
        else $list = $query->findOrFail($id)->histories()->paginate();
        return $this->response([trans('response.controllers.history.user'), $list]);
    }
    public function plan(int $id) : JsonResponse
    {
        if (!$this->isAdmin() && !$this->isManager()) $this->notFound();
        $list = CompanyPlan::query()->findOrFail($id)->histories()->paginate();
        return $this->response([trans('response.controllers.history.plan'), $list]);
    }
    public function company(int $id) : JsonResponse
    {
        $query = Company::query();
        if (!$this->isAdmin() && !$this->isManager()) $query->byUser(Auth::id());
        $list = ($c = $query->findOrFail($id))->histories()->paginate();
        $items = $list->items();
        foreach ($items?:[] as $k => $v) $items[$k] = $v->toArray();
        $users = CompanyUser::withTrashed()
            ->whereIn('id', Arr::pluck($items, 'user_id'))
            ->get(['id','name', 'email'])->keyBy('id');
        $plans = CompanyPlan::query()->withTrashed()->get()->keyBy('id');
        foreach ($items as $k => $v) {
            if ($uid = Arr::get($v, 'user_id')) {
                $list->items()[$k]['user'] = $users->find($uid);
            }
            if (Arr::get($v, 'plan'))
                $list->items()[$k]['plan_name'] = ($p = $plans->get(Arr::get($v['plan'],'company_plan_id', -1))) ? $p->name : null;
        }
        return $this->response([trans('response.controllers.history.company'), $list]);
    }
    public function case(int $id) : JsonResponse
    {
        if ($this->isAdmin() || $this->isManager()) $this->notFound();
        $list = CompanyCase::query()->whereHas('company', function($q) {
            $q->whereIn('id', $this->user->companies()->get(['companies.id'])->pluck('id')->toArray());
        })->findOrFail($id)->histories()->paginate();
        return $this->response([trans('response.controllers.history.case'), $list]);
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Exports\CourtRequisitesExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Court\CourtFilterRequest;
use App\Http\Requests\Court\FileRequest;
use App\Imports\IfnsImport;
use App\Imports\RequisitesImport;
use App\Models\Catalog\Court;
use App\Models\Catalog\Region;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class CourtController
 * @package App\Http\Controllers\Api
 */
class CourtController extends Controller
{
    /**
     * CourtController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:admins')->only('importCourts');
        $this->middleware('auth:auth-api')->except('importCourts');
    }

    /**
     * @return JsonResponse
     */
    public function getMeta() : JsonResponse
    {
        return $this->response([
            trans('responses.controllers.court.getMeta'),
            [
                'regions' => Region::query()->whereHas('courts')->get(),
                'court_types' => Court::TYPES,
                'supreme_type' => Court::SUPREME_TYPE,
                'last_update' => Carbon::now()->format('d.m.Y'),
            ]
        ]);
    }

    /**
     * @param $code
     * @return JsonResponse
     */
    public function regionCourts($domain, $code) : JsonResponse
    {
        $r = Region::query()->findOrFail((int) $code);
        $courts = $r->courts()->groupBy('court_type')->selectRaw('court_type, count(*) count, min(name) as name, min(id) as id')
            ->get()
            ->toArray();
        foreach ($courts as $i => $court){
            if($court['court_type'] != Court::SUPREME_TYPE || $court['count'] != 1) {
                $courts[$i]['name'] = Court::TYPES[$court['court_type']];
                unset($courts[$i]['id']);
            }
        }
        return $this->response([trans('responses.controllers.court.region_courts'),[
            'region' => $r,
            'courts' => $courts,
        ]]);
    }

    /**
     * @param CourtFilterRequest $request
     * @return JsonResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function index(CourtFilterRequest $request)
    {
        $list = $request->prepareQuery(Court::query());
        if ($request->export) {
            if ($code = $request->region_code) $name = Region::query()->findOrFail($code)->name;
            else $name = 'Все_регионы';
            return Excel::download(new CourtRequisitesExport($list), $name . '_рекизиты_'. Carbon::now()->format('d_m_Y_H_i'). '.xlsx');
        }
        return $this->response([trans('responses.controllers.court.index'), $list->paginate(((int) $request->per_page) ?? null)]);
    }

    /**
     * @param $domain
     * @param $id
     * @return JsonResponse
     */
    public function show($domain, $id) : JsonResponse
    {
        $court = Court::query()->with('requisite', 'structure')->findOrFail((int)$id);
        $court->load('main');
        return $this->response([
            trans('responses.controllers.court.show'),
            $court
        ]);
    }

    /**
     * @param $domain
     * @param $id
     * @return JsonResponse
     */
    public function area($domain, $id) : JsonResponse
    {
        $court = Court::query()->findOrFail((int)$id);
        return $this->response([
            trans('responses.controllers.court.area'),
            $court->area()->paginate(((int) request()->per_page) ?? null)
        ]);
    }

    /**
     * @param FileRequest $request
     * @return JsonResponse
     */
    public function importCourts(FileRequest $request) : JsonResponse
    {
        Excel::import(new RequisitesImport(), $request->file);
        return $this->response([trans('responses.controllers.court.file.imported')]);
    }

    /**
     * @param FileRequest $request
     * @return JsonResponse
     */
    public function ifnsImport(FileRequest $request) : JsonResponse
    {
        Excel::import(new IfnsImport(), $request->file);
        return $this->response([trans('responses.controllers.court.file.imported')]);
    }
}

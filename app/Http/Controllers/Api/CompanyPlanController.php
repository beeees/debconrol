<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\AppRequest;
use App\Http\Requests\Company\CompanyPlanCreateRequest;
use App\Http\Requests\Company\CompanyPlanUpdateRequest;
use App\Models\Company\CompanyPlan;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class CompanyPlanController
 * @package App\Http\Controllers\Api
 */
class CompanyPlanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin-managers')->except('getMeta');
    }
    public function getMeta() : JsonResponse
    {
        return $this->response([
            trans('responses.controllers.company_plan.getMeta'),
            [
                'periods' => CompanyPlan::INTERVAL_NAMES,
            ]
        ]);
    }
    public function index(AppRequest $request) : JsonResponse
    {
        $query = $request->prepareQuery(CompanyPlan::query());
        return $this->response([
            trans('responses.controllers.company_plan.index'),
            $query->paginate((int)request()->per_page ?: null)]);
    }
    public function store(CompanyPlanCreateRequest $request) : JsonResponse
    {
        return $this->response([
            trans('responses.controllers.company_plan.store'),
            CompanyPlan::query()->firstOrCreate($request->validated())
        ]);
    }
    public function show(string $domain, $id) : JsonResponse
    {
        return $this->response([
            trans('responses.controllers.company_plan.show'),
            CompanyPlan::query()->findOrFail((int)$id)
        ]);
    }
    public function update(CompanyPlanUpdateRequest $request,string $domain,  $id) : JsonResponse
    {
        return $this->response([
            trans('responses.controllers.company_plan.update'),
            ($m = CompanyPlan::query()->findOrFail((int)$id))->forceFill($request->validated())->save(),
            $m
        ]);
    }

}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\CanCheckCaseAccess;
use App\Http\Controllers\Controller;
use App\Http\Requests\AppRequest;
use App\Http\Requests\Company\CompanyCaseCreateRequest;
use App\Http\Requests\Company\CompanyCaseUpdateRequest;
use App\Models\CanAuthUser;
use App\Models\Company\Company;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class CompanyCaseController extends Controller
{
    use CanCheckCaseAccess;
    public function __construct()
    {
        $this->middleware('auth:clients');
    }

    /**
     * @param Company $company
     * @return CanAuthUser
     */
    public static function checkCompanyAccessAndFail(Company $company)
    {
        return $company->users()->findOrFail(Auth::id());
    }

    public function checkCanModifyCaseAndFail(Company $company) : void
    {
        $this->checkCompanyAccessAndFail($company);
        if (!$this->isBoss() && !$this->isOperator()) $this->notFound();
    }
    /**
     * @param AppRequest $request
     * @param Company $company
     * @return JsonResponse|\Illuminate\Http\Response
     */
    public function index(AppRequest $request, string $domain, Company $company)
    {
        $this->checkCompanyAccessAndFail($company);
        $query = $request->prepareQuery($this->getAccessedCasesQuery($request)->byUser(Auth::user()));
        $query->withCount('debtors');
        return $this->response([trans('responses.controllers.case.index'),
            $query->paginate(((int) request()->per_page) ?: null)]);
    }

    /**
     * @param CompanyCaseCreateRequest $request
     * @param Company $company
     * @return JsonResponse
     * @throws \App\Exceptions\CantModifyCaseByPlanException
     */
    public function store(CompanyCaseCreateRequest $request, string $domain, Company $company) : JsonResponse
    {
        $this->checkCanModifyCaseAndFail($company);
        $case = $this->getAccessedCasesQuery($request)->newModelInstance($request->validated());
        $company->activePlan->companyPlan->checkCaseAccess($case);
        $company->cases()->save($case);
        return $this->response([trans('responses.controllers.case.store'), $case], 201);
    }
    /**
     * @param Company $company
     * @return JsonResponse
     */
    public function show(AppRequest $request, string $domain, Company $company,$caseId) : JsonResponse
    {
        $this->checkCompanyAccessAndFail($company);
        $case = $this->getAccessedCasesQuery($request)->findOrFail((int) $caseId);
        return $this->response([trans('responses.controllers.case.show'), $case]);
    }

    /**
     * @param CompanyCaseUpdateRequest $request
     * @param Company $company
     * @param $caseId
     * @return JsonResponse
     * @throws \App\Exceptions\CantModifyCaseByPlanException
     */
    public function update(CompanyCaseUpdateRequest $request,string $domain,  Company $company, $caseId) : JsonResponse
    {
        $this->checkCanModifyCaseAndFail($company);
        $case = $this->getAccessedCasesQuery($request)->findOrFail((int)$caseId)->fill($request->validated());
        $company->activePlan->companyPlan->checkCaseAccess($case);
        $case->save();
        return $this->response([trans('responses.controllers.case.update'), $case]);
    }

    /**
     * @param Company $company
     * @param $caseId
     * @return JsonResponse
     */
    public function destroy(AppRequest $request, string $domain, Company $company, $caseId) : JsonResponse
    {
        $this->checkCanModifyCaseAndFail($company);
        $case = $this->getAccessedCasesQuery($request)->findOrFail((int)$caseId)->delete();
        return $this->response([trans('responses.controllers.case.delete'), $case]);
    }
}

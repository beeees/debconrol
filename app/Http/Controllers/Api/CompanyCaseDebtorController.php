<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\CaseInactiveException;
use App\Exceptions\DebtorsCheckUnavailableException;
use App\Exceptions\UnknownCaseCheckTypeException;
use App\Exports\DebtorsCheckResultsExport;
use App\Exports\DebtorsExport;
use App\Http\Controllers\CanCheckCaseAccess;
use App\Http\Controllers\Controller;
use App\Http\Requests\AppRequest;
use App\Http\Requests\Company\CompanyCaseDebtorCheckResultsRequest;
use App\Http\Requests\Company\CompanyCaseDebtorsActionRequest;
use App\Http\Requests\Company\CompanyCaseDebtorsRequest;
use App\Http\Requests\Company\CompanyCaseDebtorUpdateRequest;
use App\Jobs\CreateCaseDebtorsParserTaskJob;
use App\Jobs\CreateUploadedFileParserTaskJob;
use App\Jobs\GetDadataCleanedAddressesJob;
use App\Jobs\ParseDebtorDealsJob;
use App\Models\Catalog\Court;
use App\Models\Catalog\CourtArea;
use App\Models\Company\CompanyCase;
use App\Models\DadataAddressRecord;
use App\Models\DadataFioRecord;
use App\Models\Debtor;
use App\Models\DebtorCheckResult;
use App\Models\Users\CompanyUser;
use Carbon\Carbon;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use Maatwebsite\Excel\Facades\Excel;

class CompanyCaseDebtorController extends Controller
{
    use CanCheckCaseAccess;
    /**
     * CaseManageController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:clients')->except('getMeta');
    }

    public function getMeta() {
        return $this->response([
            trans('responses.controllers.case.debtor.getMeta'),
            [
                'debtor_columns' => Debtor::DEBTORS_TABLE_COLUMNS_NAMES[CompanyCase::ASSESSED_CHECK_TYPE],
                'result_columns' => Debtor::RESULT_COLUMNS_NAMES,
                'court_debtor_columns' => Debtor::DEBTORS_TABLE_COLUMNS_NAMES[CompanyCase::FULL_CHECK_TYPE],
                'court_results_columns' => Debtor::DEBTORS_TABLE_COLUMNS_NAMES[CompanyCase::FULL_CHECK_TYPE],
                'deal_debtor_columns' => Debtor::DEBTORS_TABLE_COLUMNS_NAMES[CompanyCase::DEAL_CHECK_TYPE],
                'deal_results_columns' => Debtor::DEBTORS_TABLE_COLUMNS_NAMES[CompanyCase::DEAL_CHECK_TYPE],
                'fssp_debtor_columns' => Debtor::DEBTORS_TABLE_COLUMNS_NAMES[CompanyCase::FSSP_CHECK_TYPE],
                'fssp_results_columns' => Debtor::DEBTORS_TABLE_COLUMNS_NAMES[CompanyCase::FSSP_CHECK_TYPE],
            ]
        ]);
    }

    /**
     * @param CompanyCaseDebtorsRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function index($domain, $company, $case, CompanyCaseDebtorsRequest $request)
    {
        $query = $request->prepareQuery(($case = $this->getAccessedCase($request))->debtors()->getQuery());
        if ($request->export) {
            return Excel::download(new DebtorsExport($query, $case), 'Должники_'. Carbon::now()->format('d_m_Y_H_i'). '.xlsx');
        }
        if ($case->check_type === CompanyCase::FULL_CHECK_TYPE) $query->with('courts.requisite');
        if ($case->check_type === CompanyCase::DEAL_CHECK_TYPE) $query->with('deals', 'exceptions');
        if ($case->check_type === CompanyCase::FSSP_CHECK_TYPE) $query->with('fsspDeals', 'exceptions', 'checkResults');
        return $this->response([
            trans('responses.controllers.case.debtor.index'),
            $query->paginate(((int) $request->per_page) ?: null)
        ]);
    }

    /**
     * @param $domain
     * @param $company
     * @param $case
     * @param $debtor
     * @param AppRequest $request
     * @return JsonResponse
     */
    public function area($domain, $company, $case, $debtor, AppRequest $request):JsonResponse
    {
        $case = $this->getAccessedCase($request);
        $debtor = $case->debtors()->with('dadataAddress')->findOrFail($debtor);
        $areas = CourtArea::query()
            ->whereIn('court_id', $debtor->courts()->get(['courts.id'])->pluck('id')->toArray())
            ->byAddress($debtor->getRelation('dadataAddress'))
            ->get();
        return $this->response([
            trans('responses.controllers.case.debtor.area'),
            $areas
        ]);
    }

    /**
     * @param CompanyCaseDebtorsActionRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function destroy(CompanyCaseDebtorsActionRequest $request) : JsonResponse
    {
        if (!$this->isBoss() && !$this->isOperator()) $this->notFound();
        return $this->response([
            trans('responses.controllers.case.debtor.delete'),
            $this->getAccessedCase($request)->debtors()
                ->whereIn('id', $request->ids)
                ->delete()
        ]);
    }

    public function update($domain, $company, $case, $debtor, CompanyCaseDebtorUpdateRequest $request) : JsonResponse
    {
        if (!$this->isBoss() && !$this->isOperator()) $this->notFound();
        $case = $this->getAccessedCase($request);
        $debtor = $case->debtors()->with('dadataFio')->findOrFail((int)$debtor);
        $debtor->fill($request->only(['mark', 'fio', 'loaded_address'])+ ['error' => null]);
        if ($address = $debtor->isDirty('loaded_address')) {
            $debtor->address = null;
            $debtor->courts()->detach();
        }
        if ($debtor->deals()->whereKey($request->main_deal_id)->count())
            $debtor->deals()->whereKeyNot($request->main_deal_id)->delete();
        if ($request->deal_id && $url = $request->court_url) {
            $debtor->deals()->whereKey($request->deal_id)->update(['court_urls' => $url]);
        }
        if(!$address && $debtor->isDirty('fio')) {
            $fio = $debtor->getRelation('dadataFio');
            if ($fio->result['result'] === trim($debtor->fio)) {
                $fio = $fio->result;
                DadataFioRecord::fillDebtorByResultArray($debtor, $fio);
            } else {
                $parts = explode(' ', trim($debtor->fio));
                $debtor->secondname =  Arr::get($parts, 0, '');
                $debtor->name = Arr::get($parts, 1, '');
                $debtor->lastname =  Arr::get($parts, 2, '');
            }
            $debtor->save();
        } else {
            if ($address) {
                $debtor->finished_at = null;
                dispatch(new GetDadataCleanedAddressesJob(0, $debtor->id))->delay(5);
            }
            $debtor->save();
        }

        return $this->response([
            trans('responses.controllers.case.debtor.update'),
            true,
            $debtor->refresh()
        ]);
    }

    /**
     * @param CompanyCaseDebtorsActionRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     * @throws DebtorsCheckUnavailableException
     * @throws CaseInactiveException
     * @throws UnknownCaseCheckTypeException
     */
    public function refresh(CompanyCaseDebtorsActionRequest $request) : JsonResponse
    {
        if (!$this->isBoss() && !$this->isOperator()) $this->notFound();
        $case = $this->getAccessedCase($request);
        if (!$case->is_active) throw new CaseInactiveException();
        if ($case->checked_at && Carbon::now()->subHours(CompanyCase::HOURS_PER_CHECK_DEBTOR)->lt($case->checked_at)) {
            throw new DebtorsCheckUnavailableException();
        }
        $query = $request->prepareQuery($case->debtors()->getQuery());
            $toUpdate = $query->update(['finished_at' => null]);
        if ($toUpdate > 0) $case->fill(['checked_at' => Carbon::now()])->save();
        $debtors = $query->with('file')->get()->keyBy('id');
        $response = $this->response([
            trans('responses.controllers.case.debtor.refresh'),
            $toUpdate > 0,
            $case
        ]);
        $files =[];
        foreach ($ids = ($request->ids?: $debtors->pluck('id')->toArray()) as $id) {
            /** @var Debtor $d */
            if (!$d = $debtors->get($id)) continue;
            switch ($case->check_type) {
                case CompanyCase::ASSESSED_CHECK_TYPE:
                    $file = Arr::get($files, $d->company_uploaded_file_id, $d->file()->first());
                    dispatch(new CreateUploadedFileParserTaskJob($files[$file->id] = $file, $ids))->delay(5);
                    return $response;
                case CompanyCase::FULL_CHECK_TYPE:
                    dispatch(new GetDadataCleanedAddressesJob(0, $id))->delay(5);
                    break;
                case CompanyCase::DEAL_CHECK_TYPE:
                    dispatch(new ParseDebtorDealsJob($d->company_uploaded_file_id, 10, null, $ids, true))->delay(5);
                    return $response;
                case CompanyCase::FSSP_CHECK_TYPE:
                    dispatch(new CreateCaseDebtorsParserTaskJob($ids, $case->check_type));
                    return $response;
                default: throw new UnknownCaseCheckTypeException();
            }
        }
        foreach ($files as $file) {
            $file->update(['finished_at' => null]);
        }
        return $response;
    }

    /**
     * @param CompanyCaseDebtorCheckResultsRequest $request
     * @return JsonResponse|\Illuminate\Http\Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws UnknownCaseCheckTypeException
     */
    public function results(CompanyCaseDebtorCheckResultsRequest $request)
    {
        $case = $this->getAccessedCase($request);
        switch ($case->check_type) {
            case CompanyCase::FSSP_CHECK_TYPE:
                $query = $case->debtors()
                    ->whereHas('fsspDeals')
                    ->whereHas('checkResults')
                    ->with('fsspDeals', 'checkResults')
                    ->getQuery();
                $query = $request->prepareDebtorQuery($query);
                break;
            case CompanyCase::ASSESSED_CHECK_TYPE:
                $query = DebtorCheckResult::query()->whereHas('debtors', function($q)  use ($case){
                    $q->whereHas('companyCase', function($q2) use($case){ $q2->whereKey($case->id);});
                })->with('debtors', function($q) use ($case){$q->where('case_id', $case->id);});
                $query = $request->prepareQuery($query);
                break;
            case CompanyCase::FULL_CHECK_TYPE:
                $query = $case->debtors()->whereHas('courts')->with('courts.requisite')->getQuery();
                $query = $request->prepareDebtorQuery($query);
                break;
            case CompanyCase::DEAL_CHECK_TYPE:
                $query = $case->debtors()->whereHas('deals', function($q) {
                    $q->whereNotNull('deal_start_date')
                        ->orWhereNotNull('deal_end_date')
                        ->orWhereNotNull('deal_verdict');
                })->with('deals')->getQuery();
                $query = $request->prepareDebtorQuery($query);
                break;
            default: throw new UnknownCaseCheckTypeException();
        }
        if ($request->export) {
            return Excel::download(new DebtorsCheckResultsExport($query, $case), 'Результаты_проверки_'. str_replace(' ', '_', $case->name) . '_'. Carbon::now()->format('d_m_Y_H_i'). '.xlsx');
        }
        return $this->response([
            trans('responses.controllers.case.debtor.results'),
            $query->paginate(((int) $request->per_page) ?: null)
        ]);
    }
}

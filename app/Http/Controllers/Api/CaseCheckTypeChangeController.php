<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\AppException;
use App\Http\Controllers\CanCheckCaseAccess;
use App\Http\Controllers\Controller;
use App\Http\Requests\AppRequest;
use App\Http\Requests\Company\DealCaseToFsspRequest;
use App\Jobs\CreateCaseDebtorsParserTaskJob;
use App\Jobs\CreateUploadedFileParserTaskJob;
use App\Jobs\GetParserTaskResultsJob;
use App\Jobs\ParseDebtorDealsJob;
use App\Models\Catalog\Court;
use App\Models\Company\Company;
use App\Models\Company\CompanyCase;
use App\Models\Company\CompanyUploadedFile;
use App\Models\DebtorFsspDeal;
use App\Models\Debtor;
use App\Models\DebtorDeal;
use App\Utils\CompanyCaseDataUploadFormat;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CaseCheckTypeChangeController extends Controller
{

    use CanCheckCaseAccess;
    /**
     * CaseManageController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:clients')->except([]);
    }

    /**
     * @param DealCaseToFsspRequest $request
     * @param string $domain
     * @param Company $company
     * @param $caseId
     * @return JsonResponse
     * @throws AppException
     */
    public function dealToFssp(DealCaseToFsspRequest $request, string $domain, Company $company, $caseId) : JsonResponse
    {
        CompanyCaseController::checkCompanyAccessAndFail($company);
        $case  = $this->getAccessedCase($request);
        if (DebtorDeal::query()->whereIn('debtor_id', $request->debtors)
            ->whereRaw('(deal_number is NULL and fs_number is NULL)')
            ->count()) throw new AppException("Все выбранные должники должны иметь номер дела", 400);
        $debtors =  Debtor::query()
            ->leftJoin(DB::raw('debtor_deals dd'), 'debtors.id', '=', 'dd.debtor_id')
            ->whereKey($request->debtors)
            ->where('case_id', $caseId)
            ->get([DB::raw('debtors.*, deal_number')]);
        $fsspCase = new CompanyCase($case->only('name', 'is_active'));
        $fsspCase->name .=': ФССП';
        $fsspCase->check_type = CompanyCase::FSSP_CHECK_TYPE;
        $fsspCase->company_id= $case->company->id;
        $fsspCase->related_case_id = $case->id;
        if ($company->cases()->where('name', $fsspCase->name)->exists()) {
            $fsspCase->name .= ' ('. ($company->cases()->where('name', '~*', $fsspCase->name)->count() + 1 ).')';
        }
        $fsspCase->save();
        $file = new CompanyUploadedFile([
            'format' => CompanyCaseDataUploadFormat::FORMAT_MIX,
            'items' => $debtors->count(),
        ]);
        $file->file = 'not_found.xlsx';
        $file->user()->associate(Auth::user());
        $file->companyCase()->associate($fsspCase);
        $file->save();
        $ids = [];
        foreach ($debtors as $d) {
            $debtor = new Debtor(Arr::except($d->toArray(),'finished_at'));
            $debtor->case_id = $fsspCase->id;
            $debtor->company_uploaded_file_id = $file->id;
            $debtor->error = null;
            $debtor->save();
            foreach ($d->deals()->get() as $deal) $debtor->fsspDeals()->create([
                'deal_number' => $deal->deal_number,
                'fs_number' => $deal->fs_number,
            ]);
            $ids[] = $debtor->id;
        }
        dispatch(new CreateCaseDebtorsParserTaskJob($ids, CompanyCase::FSSP_CHECK_TYPE));
        return $this->response(['Новый портфель успешно создан', $fsspCase]);

    }
    /**
     * @param DealCaseToFsspRequest $request
     * @param string $domain
     * @param Company $company
     * @param $caseId
     * @return JsonResponse
     * @throws AppException
     */
    public function fsspToDeal(DealCaseToFsspRequest $request, string $domain, Company $company, $caseId) : JsonResponse
    {
        CompanyCaseController::checkCompanyAccessAndFail($company);
        $case  = $this->getAccessedCase($request);
        if ($case->debtors()->whereIn('debtors.id', $request->debtors)
            ->whereRaw('(name is NULL or birthday is NULL)')
            ->count()) throw new AppException("Все выбранные должники иметь ФИО и дату рождения", 400);
        $debtors =  Debtor::query()
            ->leftJoin(DB::raw('debtor_deals dd'), 'debtors.id', '=', 'dd.debtor_id')
            ->whereKey($request->debtors)
            ->where('case_id', $caseId)
            ->get([DB::raw('debtors.*, deal_number')]);
        $dealCase = new CompanyCase($case->only('name', 'is_active'));
        $dealCase->name .=': Судебные дела';
        $dealCase->check_type = CompanyCase::DEAL_CHECK_TYPE;
        $dealCase->company_id= $case->company->id;
        $dealCase->related_case_id = $case->id;
        if ($company->cases()->where('name', $dealCase->name)->exists()) {
            $dealCase->name .= ' ('. ($company->cases()->where('name', '~*', $dealCase->name)->count() + 1 ).')';
        }
        $dealCase->save();
        $file = new CompanyUploadedFile([
            'format' => CompanyCaseDataUploadFormat::FORMAT_MIX,
            'items' => $debtors->count(),
        ]);
        $file->file = 'not_found.xlsx';
        $file->user()->associate(Auth::user());
        $file->companyCase()->associate($dealCase);
        $file->save();
        $ids = [];
        foreach ($debtors as $d) {
            $debtor = new Debtor(Arr::except($d->toArray(),'finished_at'));
            $debtor->case_id = $dealCase->id;
            $debtor->company_uploaded_file_id = $file->id;
            $debtor->error = null;
            $debtor->save();
            foreach ($d->fsspDeals()->get() as $deal) $debtor->deals()->create([
                'deal_number' => $deal->deal_number,
                'fs_number' => $deal->fs_number,
                'court_urls' => Court::query()->whereRaw(
                    'exists(select * from debtor_check_results where id_issuer is not NULL and courts.name ~* id_issuer)'
                )->select(['url'])->firstOrNew()->url ?: ''
            ]);
            $ids[] = $debtor->id;
        }
        dispatch(new ParseDebtorDealsJob($file->id));
        return $this->response(['Новый портфель успешно создан', $dealCase]);

    }
}

<?php

namespace App\Http\Controllers\Api;


use App\Console\Commands\FsspBailiffListParse;
use App\Exports\BailiffExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Bailiff\BailiffFilterRequest;
use App\Models\Catalog\Bailiff;
use App\Models\Catalog\Region;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class BailiffController
 * @package App\Http\Controllers\Api
 */
class BailiffController extends Controller
{
    /**
     * CourtController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:admins')->only('importCourts');
        $this->middleware('auth:auth-api')->except('importCourts');
    }


    /**
     * @param BailiffFilterRequest $request
     * @return JsonResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function index(BailiffFilterRequest $request)
    {
        $list = $request->prepareQuery(Bailiff::query());

        if ($request->export) {
            if ($code = $request->region_code) $name = Region::query()->findOrFail($code)->name;
            else $name = 'Все_ОСП';
            return Excel::download(new BailiffExport($list), $name . '_рекизиты_'. Carbon::now()->format('d_m_Y_H_i'). '.xlsx');
        }
        return $this->response([trans('responses.controllers.court.index'), [
                'bailiffs' => $list->get(),
                'region' => Region::query()->find($request->region_code),
                'bailiffs_headers' => array_combine(FsspBailiffListParse::ARRAY_HEADERS, FsspBailiffListParse::TABLE_HEADERS),
            ]
        ]);
    }


}
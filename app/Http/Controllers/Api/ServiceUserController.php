<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\ServiceUserCreateRequest;
use App\Http\Requests\User\ServiceUserUpdateRequest;
use App\Http\Requests\User\UserFilterRequest;
use App\Models\CanAuthUser;
use App\Models\Users\ServiceUser;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ServiceUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admins')->except('getMeta');
    }
    public function getMeta() {
        return $this->response([trans('responses.controllers.user.meta'), [
            'statuses' => ServiceUser::STATUSES,
            'groups' => ServiceUser::USER_TYPE_GROUPS,
        ]]);
    }
    public function index(UserFilterRequest $request) : JsonResponse
    {
        $query = $request->prepareQuery(ServiceUser::query());
        return $this->response([trans('responses.controllers.user.index'),
            $query
                ->paginate(
                (int) $request->input('per_page'),
                ['id','name', 'email','is_blocked', 'phone', 'user_group'])
        ]);
    }
    public function store(ServiceUserCreateRequest $request) : JsonResponse
    {
        $u = ServiceUser::query()->newModelInstance()->forceFill($request->validated());
        $u->sendPasswordResetMail();
        return $this->response([trans('responses.controllers.user.store'),$u->save(), $u], 201);
    }
    public function update(ServiceUserUpdateRequest $request,string $domain,  int $user) : JsonResponse
    {
        $user = ServiceUser::query()->findOrFail($user);
        $u = $user->forceFill($request->validated());
        return $this->response([trans('responses.controllers.user.update'),$u->update(), $u]);
    }

    /**
     * @param mixed $user
     * @return JsonResponse
     */
    public function show(string $domain, $user) : JsonResponse
    {
        return $this->response([trans('responses.controller.user.show'), ServiceUser::query()->findOrFail($user)]);
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\CaseInactiveException;
use App\Exports\DataUploadExampleFile;
use App\Http\Controllers\CanCheckCaseAccess;
use App\Http\Controllers\Controller;
use App\Http\Requests\AppRequest;
use App\Http\Requests\Company\CheckFilesProgressRequest;
use App\Http\Requests\Company\ImportDataFileRequest;
use App\Imports\DebtorsImport;
use App\Jobs\CheckFileProcessStatusJob;
use App\Models\Company\CompanyCase;
use App\Models\Company\CompanyUploadedFile;
use App\Models\Users\CompanyUser;
use App\Utils\CompanyCaseDataUploadFormat;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Psy\Util\Json;
use function Symfony\Component\String\u;

class CompanyCaseUploadedFileController extends Controller
{
    use CanCheckCaseAccess;
    /**
     * CaseManageController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:clients');
    }

    /**
     * @return JsonResponse
     */
    public function formats(string $domain, $company, $case): JsonResponse
    {
        return $this->response([trans('responses.controllers.case.formats'), CompanyCaseDataUploadFormat::ALL]);
    }

    public function example(string $domain, $company, $case, $format)
    {
        if (($format != (int)$format)) $this->notFound();
        return Excel::download(new DataUploadExampleFile(
            (int)$format, CompanyCase::query()->findOrFail((int)$case)->check_type),
            'Пример_файла.xlsx');
    }

    /**
     * @param AppRequest $request
     * @return JsonResponse
     */
    public function index(AppRequest $request) : JsonResponse
    {
        $case = $this->getAccessedCase($request);
        $query = $request->prepareQuery($case->files()->getQuery());
        $query->with('user');
        return $this->response([
            trans('responses.controllers.case.file.index'),
            $query->paginate(((int) $request->per_page) ?: null),
        ]);
    }

    /**
     * @param ImportDataFileRequest $request
     * @return JsonResponse
     * @throws CaseInactiveException
     */
    public function store(ImportDataFileRequest $request): JsonResponse
    {
        if (!$this->isBoss() && !$this->isOperator()) $this->notFound();
        $case = $this->getAccessedCase($request);
        if (!$case->is_active) throw new CaseInactiveException();
        $uploadedFile = CompanyUploadedFile::query()->newModelInstance($request->validated());
        $uploadedFile->companyCase()->associate($case);
        $uploadedFile->user()->associate($request->user());
        $uploadedFile->save();
        Excel::import(new DebtorsImport($uploadedFile), Storage::disk('public')->path($uploadedFile->file));
        $uploadedFile = $uploadedFile->refresh()->load('badDebtors');
        $uploadedFile->badDebtorsCount = $uploadedFile->badDebtors->count();
        return $this->response([
            trans('responses.controllers.case.file.store'),
            $uploadedFile
        ]);
    }

    public function show(AppRequest $request) : JsonResponse
    {
        $file = $this->getAccessedCase($request)->files()->findOrFail((int)($request->route()->parameter('file')));
        return $this->response([trans('responses.controllers.case.file.show'), $file]);
    }

    public function checkProgress(CheckFilesProgressRequest $request) : JsonResponse
    {
        if (!$this->isBoss() && !$this->isOperator()) $this->notFound();
        $files = $this->getAccessedCase($request)->files();
        if ($ids = $request->get('files')) {
            asort($ids);
            $files->whereIn('company_uploaded_files.id', $ids);
            $files->update(['batch' => Arr::last($ids)]);
        }
        foreach ($files->get() as $file)
            $this->dispatchNow(new CheckFileProcessStatusJob($file));
        $progress = (clone $files)->first([DB::raw('sum(progress_items) as progress_items, sum(items) as items')]);
        $finished = !$files->whereNull('finished_at')->exists();
        return $this->response([
            trans('responses.controllers.case.file.check'),
            $progress ? $progress->toArray() + compact('finished') : null,
        ]);
    }

}

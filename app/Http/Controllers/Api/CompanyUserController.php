<?php

namespace App\Http\Controllers\Api;


use App\Exports\CompanyUserExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\AppRequest;
use App\Http\Requests\User\CompanyUserCreateRequest;
use App\Http\Requests\User\CompanyUserFilterRequest;
use App\Http\Requests\User\CompanyUserUpdateRequest;
use App\Models\Company\Company;
use App\Models\Users\CompanyUser;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class CompanyUserController
 * @package App\Http\Controllers\Api
 */
class CompanyUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:auth-api');
    }

    public function getMeta()
    {
        return $this->response([trans('responses.controllers.user.meta'), [
            'statuses' => CompanyUser::STATUSES,
            'groups' => CompanyUser::USER_TYPE_GROUPS,
            'companies' => $this->getAvailableCompanies(),
        ]]);
    }

    /**
     * @return Collection|null
     */
    protected function getAvailableCompanies() : ?Collection
    {
        $q = null;
        if ($this->isBoss()) $q =  Auth::user()->companies()->active();
        if (Auth::user()->is_company_user === false) $q = Company::query();
        if ($q) return $q->get(['companies.id', 'companies.name']);
        return null;
    }

    public function checkByEmail() : JsonResponse
    {
        $u = null;
        return $this->response([
            trans('responses.controllers.user.checkByEmail'),
            !request('email')
                ? false
                : $u = CompanyUser::query()->where(request()->only('email')?: '')->first(['id', 'email']),
            $u
        ], $u ? 200 : 404);
    }
    public function getQuery() : Builder
    {
        if (!$this->isAdmin() && !$this->isManager() && !$this->isBoss()) $this->notFound();
        $query = CompanyUser::query();
        if (!$this->isAdmin() && !$this->isManager())
            $query = $query->whereHas('companies', function($q){
                $q->whereKey(Company::query()->byUser(Auth::id())->get(['id'])->pluck(['id'])->toArray());
            });
        return $query;
    }

    /**
     * @param CompanyUserFilterRequest $request
     * @return JsonResponse|\Illuminate\Http\Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function index(CompanyUserFilterRequest $request)
    {
        $query = $request->prepareQuery($this->getQuery());
        $query->with('companies', function($q) {
            $q->select(['companies.id', 'companies.name']);
        });
        if ($request->export === 'true')
            return Excel::download(new CompanyUserExport($query),
                'Выгрузка_пользователей_' . Carbon::now()->format('d_m_Y_H_i') . '.xlsx');
        return $this->response([trans('responses.controllers.user.index'),
            $query
                ->paginate(
                    (int)$request->input('per_page'),
                    ['id', 'name', 'email', 'is_blocked', 'phone', 'user_group'])
        ]);
    }

    public function store(CompanyUserCreateRequest $request): JsonResponse
    {
        if (!$this->isAdmin() && !$this->isManager() && !$this->isBoss()) $this->notFound();
        $u = CompanyUser::query()->newModelInstance()->forceFill($valid = $request->validated());
        $u->fillPassword($valid);
        $u->save(Arr::only($request->input(), 'companies'));
        if ($c = $this->validateCompaniesIds($request)) {
            $u->syncCompanies($c);
        }
        $u->sendPasswordResetMail();
        return $this->response([trans('responses.controllers.user.store'), $u], 201);
    }
    private function validateCompaniesIds(AppRequest $request) : array
    {
        if (!$request->companies) return [];
        if (!$this->isAdmin() && !$this->isManager()) {
            $c = array_intersect($request->companies, !$this->isBoss() ?[]
                : array_values(Auth::user()->companies()->get(['companies.id'])->pluck('id')->toArray()));
        } else {
            $c = $request->companies;
        }
        return array_unique($c);
    }
    public function update(CompanyUserUpdateRequest $request,string $domain,  int $user): JsonResponse
    {
        $user = $this->getQuery()->findOrFail($user);
        $u = $user->forceFill($valid = $request->validated());
        $u->fillPassword($valid);
        if (($c = $this->validateCompaniesIds($request)) || $request->has('companies')) {
            $u->syncCompanies($c);
        }
        $u->save(Arr::only($request->input(), 'companies'));
        return $this->response([trans('responses.controllers.user.update'), $u]);
    }

    /**
     * @param mixed $user
     * @return JsonResponse
     */
    public function show(string $domain, $user): JsonResponse
    {
        $user = $this->getQuery()
            ->with('companies')
            ->findOrFail($user);
        return $this->response([
            trans('responses.controller.user.show'),
            $user
        ]);
    }
}
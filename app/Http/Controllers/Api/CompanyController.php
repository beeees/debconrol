<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Middleware\ConvertNullStringsToNull;
use App\Http\Requests\AppRequest;
use App\Http\Requests\Company\CompanyCreateRequest;
use App\Http\Requests\Company\CompanyFilterRequest;
use App\Http\Requests\Company\CompanyUpdateRequest;
use App\Models\Company\Company;
use App\Models\Company\CompanyPlan;
use App\Models\Users\CompanyUser;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin-managers')->only('store');
        $this->middleware('auth:auth-api')->except('store');
        $this->middleware(ConvertNullStringsToNull::class)->only(['store', 'update']);
    }
    public function getMeta() : JsonResponse
    {
        return $this->response([
            trans('responses.controllers.company.getMeta'),
            [
                'plans' => CompanyPlan::query()->where('is_active', true)->get(),
            ]
        ]);
    }

    /**
     * @return Builder|Company
     */
    protected function getUserCompanyQuery() : Builder
    {
        $query = Company::query();
        if (!$this->isAdmin() && !$this->isManager()) $query->byUser(Auth::id());
        return $query;
    }
    /**
     * @param CompanyFilterRequest $request
     * @return JsonResponse
     */
    public function index(CompanyFilterRequest $request) : JsonResponse
    {
        $query = $request->prepareQuery($this->getUserCompanyQuery());
        $query->with('activePlan');
        return $this->response([
            trans('responses.controllers.company.store'),
            $query->paginate((int) $request->per_page ?: null)
        ]);
    }

    /**
     * @param CompanyCreateRequest $request
     * @return JsonResponse
     * @throws \App\Exceptions\CompanyHasActivePlanException
     * @throws \Exception
     */
    public function store(CompanyCreateRequest $request) : JsonResponse
    {
        $company = Company::query()->newModelInstance($request->validated());
        $activePlan = $company->getActivePlan($request->getCompanyPlan(), $request->active_at, $request->expired_at);
        $company->save(compact('activePlan'));
        return $this->response([
            trans('responses.controllers.company.store'),
            $company
        ]);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function show(string $domain, $id) : JsonResponse
    {
        $query = $this->getUserCompanyQuery();
        $company = $query->findOrFail((int)$id)->load('activePlan');
        return $this->response([
            trans('responses.controllers.company.show'),
            $company,
        ]);
    }

    /**
     * @param CompanyUpdateRequest $request
     * @param $id
     * @return JsonResponse
     * @throws \App\Exceptions\CompanyHasActivePlanException
     * @throws \Exception
     */
    public function update(CompanyUpdateRequest $request,string $domain,  $id) : JsonResponse
    {
        $company = $this->getUserCompanyQuery()->findOrFail((int)$id);
        if (!$company->users()->whereKey(Auth::id())->count() && !$this->isAdmin() && !$this->isManager()) throw $this->notFound();
        $company->fill($request->validated());
        if ($user = $request->getUser()) {
            $company->user()->associate($user);
            if ($company->users()->whereKey($user->id)->count() == 0) $company->users()->attach($user);
        } elseif (is_null($user)) {
            $company->user()->disassociate();
        }
        $activePlan = $company->getActivePlan($request->getCompanyPlan(), $request->active_at, $request->expired_at);
        $company->save(compact('activePlan'));
        return $this->response([
            trans('responses.controllers.company.update'),
            $company->load('activePlan')
        ]);
    }
}

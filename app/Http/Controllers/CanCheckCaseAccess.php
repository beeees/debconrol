<?php
namespace App\Http\Controllers;


use App\Http\Requests\AppRequest;
use App\Models\Company\CompanyCase;
use App\Models\Users\CompanyUser;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

trait CanCheckCaseAccess
{
    /**
     * @param AppRequest $request
     * @return CompanyCase|Builder
     */
    private function getAccessedCasesQuery(AppRequest $request): Builder
    {
        if ($user = CompanyUser::query()->find(Auth::id())) {
            $cases = CompanyCase::query()->byUser($user);
        }
        $cases =(isset($cases) ? $cases : CompanyCase::query());
        $cases->where('company_id', is_object($c = $request->route()->parameter('company')) ? $c->id : $c);
        return $cases;
    }
    private function getAccessedCase(AppRequest $request): CompanyCase
    {
        $caseId = $request->route()->parameter('case');
        return $this->getAccessedCasesQuery($request)->findOrFail((int) $caseId);
    }
}
<?php

namespace App\Http\Controllers;

use App\Http\Requests\AppRequest;
use App\Models\Company\Company;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

class VueController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @param AppRequest $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(AppRequest $request)
    {
        $sd = $request->getSubdomain();
        if ($request->isAdminSubdomain() || Company::query()->active()->where('subdomain', $sd)->count()) {
            return view('app');
        }
        throw new NotFoundHttpException();
    }
}

<?php

namespace App\Http\Requests\Company;

use App\Http\Requests\AppRequest;
use App\Models\Company\CompanyPlan;
use App\Models\Users\CompanyUser;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;

/**
 * Class CompanyCreateRequest
 * @package App\Http\Requests\Company
 * @property Carbon expired_at
 * @property Carbon active_at
 * @property mixed expired_at_time
 * @property mixed active_at_time
 */
class CompanyCreateRequest extends AppRequest
{

    public function requiredRules(): array
    {
        return [
            'name' => 'string|unique:companies,name',
            'subdomain' => 'regex:/^([a-zA-Z\-0-9]+)$/|string|unique:companies,subdomain',
            'company_plan_id' => 'exists:company_plans,id,is_active,TRUE',
        ];
    }

    public function sometimesRules(): array
    {
        return [
            'is_active' => 'boolean',
            'active_at' => 'date_format:Y-m-d|nullable',
            'active_at_time' => 'date_format:H:i|nullable',
            'expired_at' => $this->input('expired_at') === 'null' ? '' :'date_format:Y-m-d|nullable',
            'expired_at_time' => $this->input('expired_at_time') === 'null' ? '' :'date_format:H:i|nullable',
            'discount' => 'numeric|min:0|max:100|nullable',
            'sort' => 'int|min:0|nullable',
            'logo' => 'image|nullable',
            'contact_user_name' => 'string|max:255|nullable',
            'phone' => 'string|max:255|regex:/(\+7[0-9]{10})/|nullable',
            'email' => 'string|max:255|email|nullable',
            'url' => 'string|max:255|url|nullable',
            'requisites' => 'string|max:5000|nullable',
        ];
    }

    /**
     * @return CompanyPlan|null
     */
    public function getCompanyPlan() : ?CompanyPlan
    {
        return (($id = $this->company_plan_id) !== null)
            ? CompanyPlan::query()->findOrFail((int)$id)
            : null;
    }

    public function __get($key)
    {
        switch ($key) {
            case 'expired_at':
                if (is_null($exp = parent::__get($key))) return null;
                return Carbon::parse($exp . ' ' . (parent::__get('expired_at_time') ?: ''));
            case 'active_at':
                if (is_null($exp = parent::__get($key))) return null;
                return Carbon::parse($exp. ' ' . parent::__get('active_at_time'));
        }
        return parent::__get($key);
    }
    public function messages()
    {
        return [
            'company_plan_id.exists' => 'Выберите тариф из списка',
            'company_plan_id.required' => 'Выберите тариф из списка',
        ];
    }
}
<?php

namespace App\Http\Requests\Company;

use App\Http\Requests\AppRequest;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class DealCaseToFsspRequest
 * @package App\Http\Requests\Company
 * @property int[] debtors
 */
class DealCaseToFsspRequest extends AppRequest
{
    public function requiredRules(): array
    {
        return [
            'debtors' => 'array',
            'debtors.*' => 'int|min:1'
        ];
    }
}

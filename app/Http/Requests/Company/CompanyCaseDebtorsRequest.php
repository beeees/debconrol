<?php

namespace App\Http\Requests\Company;

use App\Exceptions\AppException;
use App\Models\Company\CompanyCase;
use App\Models\Debtor;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

/**
 * Class CompanyCaseDebtorsRequest
 * @property mixed mark
 * @property mixed regionCode
 * @property mixed birthday
 * @property mixed fio
 * @property mixed address
 * @property int errors
 * @package App\Http\Requests\Company
 */
class CompanyCaseDebtorsRequest extends CompanyCaseDebtorsActionRequest
{
    use CanHasDebtorDeal, CanHasFsspDeal;
    protected $useOrder = false;
    public function sometimesRules(): array
    {
        return parent::sometimesRules() + [
            'mark' => 'string|nullable',
            'fio' => 'string|nullable',
            'birthday' => 'date_format:Y-m-d',
            'regionCode' => 'string|nullable',
            'address' => 'string|nullable',
            'errors' => 'int|in:0,1|nullable',
        ] + $this->getRulesByCaseType();
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function prepareQuery(Builder $query) : Builder
    {
        /**@var Debtor $query*/
        $query =  parent::prepareQuery($query);
        if ($this->mark) $query->where('mark', '~*', $this->mark);
        if ($this->regionCode) $query->where('regionCode', 'like', "%{$this->regionCode}%");
        if ($this->birthday) $query->where('birthday', '>=', $b = Carbon::parse($this->birthday)->setTime(0,0,0))
            ->where('birthday','<', (clone  $b)->addDay());
        if ($this->fio) $query->byFio($this->fio);
        if ($this->address) $query->byAddress($this->address);
        if ($this->errors !== null)
            $query->whereRaw('length(coalesce(error, \'\')) '.($this->errors == 0 ? '=' : '<>').' 0');
        if (array_intersect(array_keys($this->validated()), array_keys($this->getRulesByCaseType())))
            $this->prepareQueryByCaseType($query);
        $query = $this->orderDebtorQuery($query);
        return $query;
    }
}

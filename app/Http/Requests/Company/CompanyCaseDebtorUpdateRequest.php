<?php

namespace App\Http\Requests\Company;

use App\Exceptions\UnknownCaseCheckTypeException;
use App\Http\Requests\AppRequest;
use App\Models\Company\CompanyCase;
use Illuminate\Support\Arr;

/**
 * Class CompanyCaseDebtorUpdateRequest
 * @package App\Http\Requests\Company
 * @property int main_deal_id
 * @property int deal_id
 * @property string court_url
 */
class CompanyCaseDebtorUpdateRequest extends AppRequest
{
    /**
     * @return array
     * @throws UnknownCaseCheckTypeException
     */
    public function requiredRules(): array
    {
        $case = CompanyCase::query()->find(($this->route('case')));
        switch ($case->check_type) {
            case CompanyCase::FULL_CHECK_TYPE :
                return [
                    'mark' => 'string|max:255|min:5',
                    'fio' => 'string|max:255|min:5',
                    'loaded_address' => 'string|max:255|min:5',
                ];
            case CompanyCase::DEAL_CHECK_TYPE:
                return Arr::has($this->input(), 'main_deal_id') ? [
                    'main_deal_id' => 'int',
                ] : [
                    'deal_id' => 'int',
                    'court_url' => 'url'
                ];
            default: throw new UnknownCaseCheckTypeException();
        }
    }
}
<?php

namespace App\Http\Requests\Company;


use App\Models\DebtorCheckResult;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

/**
 * Class CompanyCaseDebtorCheckResultsRequest
 * @package App\Http\Requests\Company
 * @property int errors
 */
class CompanyCaseDebtorCheckResultsRequest extends CompanyCaseDebtorsActionRequest
{
    use CanHasDebtorDeal, CanHasFsspDeal;
    public function sometimesRules(): array
    {
        return parent::sometimesRules() + [
                'fio' => 'string|nullable',
                'birthday' => 'date_format:Y-m-d|nullable',
                'ip_number' => 'string|nullable',
                'ip_date' => 'date_format:Y-m-d|nullable',
                'id_type' => 'string|nullable',
                'id_number' => 'string|nullable',
                'id_issuer' => 'string|nullable',
                'ip_end_date' => 'date_format:Y-m-d|nullable',
                'ip_end_reason' => 'string|nullable',
                'subject_name' => 'string|nullable',
                'subject_amount' => 'numeric|nullable',
                'bailiff_department_name' => 'string|nullable',
                'bailiff_fullname' => 'string|nullable',
                'region' => 'string|nullable',
                'address' => 'string|nullable',
                'court_address' => 'string|nullable',
                'court_phone' => 'string|nullable',
                'court' => 'string|nullable',
                'errors' => 'int|in:0,1|nullable',
            ]+ $this->getRulesByCaseType();
    }

    public function prepareQuery(Builder $query): Builder
    {
        /**@var DebtorCheckResult $query*/
        $query =  parent::prepareQuery($query);

        foreach ((['ip_number', 'id_type','id_number', 'id_issuer','ip_end_reason','subject_name',
            'bailiff_department_name','bailiff_fullname','region',]) as $string)
            if ($this->$string) $query->where(DB::raw("lower($string)"), 'like', '%' . mb_strtolower($this->$string).'%');
        foreach (['subject_amount'] as $number) {
            if ($this->$number != null) $query->where($number, $this->$number);
        }
        foreach (['ip_date', 'ip_end_date'] as $date)
            if ($this->$date) $query->where($date, '>=', $b = Carbon::parse($this->$date)->setTime(0,0,0))
                ->where('birthday','<', (clone  $b)->addDay());
        if ($this->fio || $this->birthday || $this->address) $query->whereHas('debtors', function($q) {
            if ($this->fio) $q->byFio($this->fio);
            if ($this->address) $q->byAddress($this->address);
            if ($this->birthday) $q->where('birthday', '>=', $b = Carbon::parse($this->birthday)->setTime(0,0,0))
                ->where('birthday','<', (clone  $b)->addDay());
        });
        return $query;
    }
    public function prepareDebtorQuery(Builder $query) : Builder
    {
        /**@var DebtorCheckResult $query*/
        $query =  parent::prepareQuery($query);
        if ($this->fio) $query->byFio($this->fio);
        if ($this->address) $query->byAddress($this->address);
        if ($this->court_address || $this->court || $this->court_phone) $query->whereHas('courts', function($q) {
            if ($a = strtolower($this->court_address)) $q->whereRaw("lower(address) like '%{$a}%'");
            if ($a = strtolower($this->court)) $q->whereRaw("lower(name) like '%{$a}%'");
            if ($a = strtolower($this->court_phone)) $q->whereRaw("phone like '%{$a}%'");
        });
        if ($this->errors !== null)
            $query->whereRaw('length(coalesce(error, \'\')) '.($this->errors == 0 ? '=' : '<>').' 0');
        if (array_intersect(array_keys($this->validated()), array_keys($this->getRulesByCaseType())))
            $this->prepareQueryByCaseType($query);
        $query = $this->orderDebtorQuery($query);
        return $query;
    }
}

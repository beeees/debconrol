<?php

namespace App\Http\Requests\Company;

use App\Http\Requests\AppRequest;
use App\Models\Company\CompanyCase;
use App\Models\Company\CompanyPlan;

class CompanyCaseCreateRequest extends AppRequest
{
    /**
     * @return array
     */
    public function requiredRules(): array
    {
        return [
            'name' => 'string|unique:cases,name,0,id,deleted_at,NULL',
            'check_type' => 'int|in:'.implode(',', array_keys(CompanyCase::CHECK_TYPES)),
            'is_active' => 'boolean',
            'fssp_check_option' => 'boolean',
            'deal_check_option' => 'boolean',
        ];
    }

    /**
     * @return array
     */
    public function sometimesRules(): array
    {
        return [
            'sort' => 'int|min:0|nullable',
        ];
    }
}

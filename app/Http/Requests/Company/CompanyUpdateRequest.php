<?php

namespace App\Http\Requests\Company;


use App\Models\Users\CompanyUser;
use Illuminate\Support\Arr;

/**
 * Class CompanyUpdateRequest
 * @package App\Http\Requests\Company
 */
class CompanyUpdateRequest extends CompanyCreateRequest
{
    public function sometimesRules(): array
    {
        return Arr::except(array_merge($r = parent::sometimesRules()+parent::requiredRules(),[
            'name' => $r['name'] . ',' . $p = $this->route()->parameter('company'),
            'user_id' => 'int|exists:users,id,is_company_user,true|nullable',
        ]), 'subdomain');
    }
    public function requiredRules(): array
    {
        return [];
    }

    /**
     * @return CompanyUser|null
     */
    public function getUser() : ?CompanyUser
    {
        return CompanyUser::query()->find((int)$this->user_id);
    }
}
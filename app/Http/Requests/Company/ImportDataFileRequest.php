<?php

namespace App\Http\Requests\Company;

use App\Http\Requests\AppRequest;
use App\Models\Company\CompanyCase;
use App\Utils\CompanyCaseDataUploadFormat;
use Illuminate\Http\UploadedFile;

/**
 * Class ImportDataFileRequest
 * @package App\Http\Requests\Company
 * @property int format
 * @property UploadedFile file
 */
class ImportDataFileRequest extends AppRequest
{

    public function requiredRules(): array
    {
        $case = CompanyCase::query()->findOrFail((int)$this->route('case'));
        return [
            'format' => 'int|in:' . implode(',', array_keys(CompanyCaseDataUploadFormat::ALL[$case->check_type])),
            'file' => 'mimetypes:application/zip,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,' .
                'application/vnd.ms-excel,application/octet-stream,text/plain'
        ];
    }
    public function messages()
    {
        return [
            'file.mimetypes' => 'Тип загруженного файла не соответствует требованиям.'
        ];
    }
}

<?php

namespace App\Http\Requests\Company;

use Illuminate\Support\Arr;

class CompanyCaseUpdateRequest extends CompanyCaseCreateRequest
{
    public function requiredRules(): array
    {
        return [];
    }

    public function sometimesRules(): array
    {
        return Arr::except(array_merge(parent::sometimesRules(), $p = parent::requiredRules(), [
            'name' => str_replace('0', $this->route()->parameter('case'),$p['name'])
        ]), 'check_type');
    }
}
<?php

namespace App\Http\Requests\Company;
use App\Models\DebtorFsspDeal;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class CanHasDebtorDeal
 * @package App\Http\Requests\Company
 * @property Carbon|null min_deal_date
 * @property Carbon|null max_deal_date
 * @property string bailiff_fullname
 * @property string bailiff_department_name
 * @property string bailiff_department_address
 * @property string deal_number
 * @property int deal_state
 * @property bool is_closed
 */
trait CanHasFsspDeal
{
    public function fsspRules() {
        return [
            'min_deal_date' => 'date_format:Y-m-d|nullable',
            'max_deal_date' => 'date_format:Y-m-d|nullable',
            'bailiff_fullname' => 'string|max:255|nullable',
            'bailiff_department_name' => 'string|max:255|nullable',
            'deal_number' => 'string|max:255|nullable',
            'deal_state' => 'int',
            'is_closed' => 'nullable|boolean'
        ];
    }
    public function prepareFsspDealsQuery(Builder $query): Builder
    {
        $query->whereHas('fsspDeals', function($query) {
            if ($this->deal_number) $query->where('deal_number', '~*', $this->deal_number);
            if ($this->is_closed) $query->where('closed', $this->is_closed);
        });
        $status = null;
        if ($this->has('deal_state')) {
            switch ((int) $this->deal_state) {
                case DebtorFsspDeal::STATE_FINISHED:
                case DebtorFsspDeal::STATE_SD:
                case DebtorFsspDeal::STATE_SV:
                    $status = (int) ($this->deal_state);
                    break;
            }
        }
        if($withCheck = ($this->min_deal_date
                || $this->max_deal_date
                || $this->bailiff_department_name
                || $this->bailiff_department_address
                || $this->bailiff_department_name
                || ($status !== null))){
            $query->whereHas('checkResults', function ($q) use ($status){
                if ($this->min_deal_date) {
                    $this->min_deal_date = Carbon::parse($this->min_deal_date)->setTime(0,0,0);
                    $q->where('ip_date','>=', $this->min_deal_date);
                }
                if ($this->max_deal_date) {
                    $this->max_deal_date = Carbon::parse($this->max_deal_date)->setTime(23,59,59);
                    $q->where('ip_date','<=', $this->max_deal_date);
                }
                if ($this->bailiff_fullname) $q->where('bailiff_fullname','~*', $this->bailiff_fullname);
                if ($this->bailiff_department_address) $q->where('bailiff_department_address','~*', $this->bailiff_department_address);
                if ($this->bailiff_department_name) $q->where('bailiff_department_name','~*', $this->bailiff_department_name);
                if ($status === DebtorFsspDeal::STATE_FINISHED) {
                    $q->whereNotNull('ip_end_date');
                } else if ($status === DebtorFsspDeal::STATE_SD) {
                    $q->where('ip_summary_case', 'like', '%СД%');
                } else if ($status === DebtorFsspDeal::STATE_SV) {
                    $q->where('ip_summary_case', 'like', '%СВ%');
                }
            });
        }
        if ($this->has('deal_state')) {
            switch ((int) $this->deal_state) {
                case DebtorFsspDeal::STATE_FIND:
                    if (!$withCheck) $query->whereHas('checkResults');
                    break;
                case DebtorFsspDeal::STATE_NOT_FIND:
                    $query->whereDoesntHave('checkResults');
                    break;
            }
        }
        return $query;
    }
}
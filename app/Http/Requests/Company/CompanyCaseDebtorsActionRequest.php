<?php

namespace App\Http\Requests\Company;

use App\Http\Requests\AppRequest;
use App\Models\Company\CompanyCase;
use App\Models\Debtor;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

/**
 * Class CompanyCaseDebtorsActionRequest
 * @package App\Http\Requests\Company
 * @property array|int[] ids
 */
class CompanyCaseDebtorsActionRequest extends AppRequest
{

    /** @var CompanyCase */
    protected $case = null;
    protected $useOrder = false;
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function sometimesRules() : array
    {
        $this->getCase();
        return [
            'ids' => 'array',
            'ids.*' => 'int',
        ];
    }

    public function prepareQuery(Builder $query): Builder
    {
        $query = parent::prepareQuery($query);
        if ($this->ids) $query->whereIn('id', $this->ids);
        return $query;
    }

    /**
     * @return array
     */
    public function getRulesByCaseType() : array
    {
        switch ($this->case->check_type) {
            case CompanyCase::DEAL_CHECK_TYPE : return $this->dealRules();
            case CompanyCase::FSSP_CHECK_TYPE : return $this->fsspRules();
        }
        return [];
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function prepareQueryByCaseType(Builder $query) : Builder
    {
        switch ((int)$this->case->check_type) {
            case CompanyCase::DEAL_CHECK_TYPE : return $this->prepareDebtorDealsQuery($query);
            case CompanyCase::FSSP_CHECK_TYPE : return $this->prepareFsspDealsQuery($query);
        }
        return $query;
    }

    /**
     * @return CompanyCase
     */
    public function getCase() : CompanyCase
    {
        if (!$this->case) $this->case = CompanyCase::query()->findOrFail((int) $this->route('case'));
        return $this->case;
    }


    /**
     * @param $query
     * @return mixed
     */
    public function orderDebtorQuery($query)
    {
        if ($column = $this->input('sortBy', 'id')) {
            switch ($column) {
                case "full_name" : $column = 'concat(secondname,name,lastname, fio)';break;
                case "errors": $column = 'error'; break;
                default:
                    if ((array_search($column, (array_keys(Debtor::query()->first()->getOriginal())))) === false) $column = null;
                    break;
            }
            if($column) $query->orderBy(DB::raw($column), 'true' === $this->input('sortDesc') ? 'desc' : 'asc');
        }
        return $query;
    }
}

<?php

namespace App\Http\Requests\Company;

use Illuminate\Foundation\Http\FormRequest;

class CompanyPlanUpdateRequest extends CompanyPlanCreateRequest
{
    public function requiredRules(): array
    {
        return [];
    }

    public function sometimesRules(): array
    {
        return array_merge(parent::sometimesRules(), $p = parent::requiredRules(), [
            'name' => $p['name'] . ',' . $this->route()->parameter('plan'),
        ]);
    }
}
<?php

namespace App\Http\Requests\Company;

use App\Http\Requests\AppRequest;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Validation\Rule;

/**
 * Class CompanyFilterRequest
 * @package App\Http\Requests\Company
 * @property int company_plan_id
 * @property boolean is_active
 * @property string name
 */
class CompanyFilterRequest extends AppRequest
{
    /**
     * @return array
     */
    public function sometimesRules(): array
    {
        return [
            'company_plan_id' => 'int|exists:company_plans,id',
            'is_active' => $this->validateBoolean(),
            'name' => 'string|nullable'
        ];
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function prepareQuery(Builder $query): Builder
    {
        if ($this->has('is_active')) {
            $query->where($this->only('is_active'));
        }
        if ($this->has('company_plan_id')) {
            $query->whereHas('activePlan', function ($q) {
                $q->where($this->only('company_plan_id'));
            });
        }
        if ($name = $this->name) {
            $query->where('name', 'like', '%' . $name . '%');
        }
        return parent::prepareQuery($query);
    }
}

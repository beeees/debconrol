<?php

namespace App\Http\Requests\Company;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class CanHasDebtorDeal
 * @package App\Http\Requests\Company
 * @property Carbon|null min_deal_init_date
 * @property Carbon|null max_deal_init_date
 * @property string act_number
 * @property string deal_number
 * @property string court_urls
 * @property int deal_state
 */
trait CanHasDebtorDeal
{
    public function dealRules() {
        return [
            'min_deal_init_date' => 'date_format:Y-m-d|nullable',
            'max_deal_init_date' => 'date_format:Y-m-d|nullable',
            'act_number' => 'string|max:255|nullable',
            'deal_number' => 'string|max:255|nullable',
            'court_urls' => 'string|max:255|nullable',
            'deal_state' => 'int|in:0,1',
        ];
    }
    public function prepareDebtorDealsQuery(Builder $query): Builder
    {
        return $query->whereHas('deals', function ($q) {
            if ($this->min_deal_init_date) {
                $this->min_deal_init_date = Carbon::parse($this->min_deal_init_date)->setTime(0,0,0);
                $q->where('deal_init_date','>=', $this->min_deal_init_date);
            }
            if ($this->max_deal_init_date) {
                $this->max_deal_init_date = Carbon::parse($this->max_deal_init_date)->setTime(23,59,59);
                $q->where('deal_init_date','<=', $this->max_deal_init_date);
            }
            if ($this->act_number) $q->where('act_number','~', $this->act_number);
            if ($this->deal_number) $q->where('deal_number','~', $this->deal_number);
            if ($this->court_urls) $q->where('court_urls','~', $this->court_urls);
            if ($this->deal_state !== null)
                if ($this->deal_state == 0) $q->whereNull('deal_end_date');
                else $q->whereNotNull('deal_end_date');
        });
    }
}
<?php

namespace App\Http\Requests\Company;

use App\Http\Requests\AppRequest;

/**
 * Class CheckFilesProgressRequest
 * @package App\Http\Requests\Company
 * @property array files
 */
class CheckFilesProgressRequest extends AppRequest
{
    public function requiredRules(): array
    {
        return [
            'files' => 'array',
            'files.0' => '',
            'files.*' => 'int',
        ];
    }
}

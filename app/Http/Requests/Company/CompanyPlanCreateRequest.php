<?php

namespace App\Http\Requests\Company;

use App\Http\Requests\AppRequest;
use App\Models\Company\CompanyCase;
use App\Models\Company\CompanyPlan;

class CompanyPlanCreateRequest extends AppRequest
{
    /**
     * @return array
     */
    public function requiredRules(): array
    {
        return [
            'interval' => 'int|min:1',
            'interval_type' => 'int|in:' . implode(',', array_keys(CompanyPlan::INTERVAL_NAMES)),
            'name' => 'string|unique:company_plans,name',
            'full_check' => 'boolean',
            'assessed_check' => 'boolean',
            'deal_check' => 'boolean',
            'fssp_check' => 'boolean',
            'is_active' => 'boolean',
            'cost' => 'numeric|min:0'
        ];
    }

    /**
     * @return array
     */
    public function sometimesRules(): array
    {
        return [
            'sort' => 'int|min:0|nullable',
            'cases' => 'int|min:0|nullable',
            'records' => 'int|min:0|nullable',
        ];
    }
}

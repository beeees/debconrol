<?php

namespace App\Http\Requests\User;

use App\Http\Requests\AppRequest;
use App\Models\CanAuthUser;
use App\Models\Users\ServiceUser;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ServiceUserCreateRequest extends UserCreateRequest
{
    public function rules()
    {
        return parent::rules() +[
            'user_group' => 'int|in:' . implode(',', array_keys(ServiceUser::USER_TYPE_GROUPS))
        ];
    }
}

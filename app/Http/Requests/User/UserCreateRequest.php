<?php

namespace App\Http\Requests\User;

use App\Http\Requests\AppRequest;
use App\Models\CanAuthUser;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;


/**
 * @property mixed is_blocked
 * @property mixed user_group
 * @property string password
 */
abstract class UserCreateRequest extends AppRequest
{
    public function getPasswordRule(): array
    {
        return [
//            'password' => 'string|confirmed|min:'.CanAuthUser::MIN_PASSWORD_LENGTH,
        ];
    }
    public function requiredRules(): array
    {
        return [
            'name' => 'string|min:10',
            'email' => 'email|unique:users,email',
            'phone' => 'regex:/(\+7[0-9]{10})/|unique:users,phone',
        ] + ($this->confirmed() ? $this->getPasswordRule() : []);
    }
    public function sometimesRules(): array
    {
        return [
            'is_blocked' => 'boolean',
            'info' => 'string|min:1|nullable',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'ФИО',
        ];
    }

}

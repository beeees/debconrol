<?php

namespace App\Http\Requests\User;

use App\Models\Users\CompanyUser;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class CompanyUserFilterRequest
 * @package App\Http\Requests\User
 * @property int company_id
 */
class CompanyUserFilterRequest extends UserFilterRequest
{
    public const USER_GROUPS = CompanyUser::USER_TYPE_GROUPS;

    public function sometimesRules(): array
    {
        return parent::sometimesRules() + [
            'company_id' => 'int'
        ];
    }

    /**
     * @param Builder|CompanyUser $query
     * @return Builder
     */
    public function prepareQuery(Builder $query): Builder
    {
        $query = parent::prepareQuery($query);
        if ($this->company_id) {
            $query->whereHas('companies', function($q) {
                $q->whereKey($this->company_id);
            });
        }
        return $query;
    }
}

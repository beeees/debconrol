<?php

namespace App\Http\Requests\User;

use App\Models\Company\Company;
use App\Models\Users\CompanyUser;
use Illuminate\Support\Arr;

/**
 * Class CompanyUserCreateRequest
 * @package App\Http\Requests\User
 * @property array companies
 */

class CompanyUserCreateRequest extends UserCreateRequest
{
    public function requiredRules(): array
    {
        return parent::requiredRules() + [
            'user_group' => 'int|in:' . implode(',', array_keys(CompanyUser::USER_TYPE_GROUPS)),
            'companies' => 'required|array',
            'companies.0' => 'required',
            'companies.*' => 'required|int|exists:companies,id,is_active,TRUE',
        ];
    }
    public function validated()
    {
        return Arr::except(parent::validated(), 'companies');
    }
}

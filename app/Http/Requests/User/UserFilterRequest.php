<?php

namespace App\Http\Requests\User;

use App\Http\Requests\AppRequest;
use App\Models\CanAuthUser;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class UserFilterRequest
 * @property string email
 * @property int status
 * @property mixed user_group
 * @package App\Http\Requests\User
 */
class UserFilterRequest extends AppRequest
{
    public const USER_GROUPS = CanAuthUser::SERVICE_USER_GROUPS;
    public const STATUSES = CanAuthUser::STATUSES;
    /**
     * @return array
     */
    public function sometimesRules(): array
    {
        return [
            'status' => 'int|in:' . implode(',', array_keys(static::STATUSES)),
            'email'=> 'string|nullable',
            'user_group' => 'int|in:' . implode(',', array_keys(static::USER_GROUPS)),
        ];
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function prepareQuery(Builder $query) : Builder
    {

        if ($this->email) $query->where('email', 'like', '%'. $this->email . '%');
        if ($this->status !== null) {
            switch ((int)$this->status) {
                case CanAuthUser::STATUS_ACTIVE:
                    $query->where(['is_blocked' => false])->whereHas('tokens');
                    break;
                case CanAuthUser::STATUS_NOT_ACTIVE:
                    $query->where(['is_blocked' => false])->whereDoesntHave('tokens');
                    break;
                case CanAuthUser::STATUS_BLOCKED:
                    $query->where(['is_blocked' => true]);
            }
        }
        if ($this->user_group) $query->where(['user_group' => $this->user_group]);
        return parent::prepareQuery($query);
    }
}

<?php

namespace App\Http\Requests\User;
/**
 * Class UserUpdateRequest
 * @package App\Http\Requests\User
 */

abstract class UserUpdateRequest extends UserCreateRequest
{
    public const ROUTE_PARAM_NAME = 'id';
    /**
     * @return array
     */
    public function sometimesRules(): array
    {
        $rules = parent::sometimesRules()+parent::requiredRules();
        return array_merge($rules, [
            'email' => $rules['email'] . ',' . $id = $this->route()->parameter(static::ROUTE_PARAM_NAME),
            'phone' => $rules['phone'] . ',' . $id,
        ]);
    }
    public function requiredRules(): array
    {
        return [];
    }
}

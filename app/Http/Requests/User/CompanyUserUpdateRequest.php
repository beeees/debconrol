<?php

namespace App\Http\Requests\User;

use App\Models\Users\CompanyUser;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

/**
 * Class CompanyUserUpdateRequest
 * @package App\Http\Requests\User
 * @property array companies
 */
class CompanyUserUpdateRequest extends UserUpdateRequest
{
    public const ROUTE_PARAM_NAME = 'company';
    public function sometimesRules(): array
    {
        return parent::sometimesRules() + [
                'user_group' => 'int|in:' . implode(',', array_keys(CompanyUser::USER_TYPE_GROUPS)),
                'companies' => 'array',
                'companies.*' => 'int|exists:companies,id,is_active,TRUE',
            ];
    }
    public function validated()
    {
        return Arr::except(parent::validated(), 'companies');
    }
}

<?php

namespace App\Http\Requests\User;


use App\Models\Users\ServiceUser;

/**
 * Class ServiceUserUpdateRequest
 * @package App\Http\Requests\User
 */
class ServiceUserUpdateRequest extends UserUpdateRequest
{
    public const ROUTE_PARAM_NAME = 'service';

    public function rules()
    {
        return parent::rules() +[
                'user_group' => 'int|in:' . implode(',', array_keys(ServiceUser::USER_TYPE_GROUPS))
            ];
    }
}

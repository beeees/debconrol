<?php

namespace App\Http\Requests\Court;


use App\Http\Requests\AppRequest;
use Illuminate\Http\UploadedFile;

/**
 * Class ImportCourtDataRequest
 * @package App\Http\Requests\Court
 * @property UploadedFile file
 */
class FileRequest extends AppRequest
{
    public function requiredRules(): array
    {
        return [
            'file' => 'mimetypes:application/zip,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel'
        ];
    }
    public function messages()
    {
        return [
            'file.mimetypes' => 'Тип загруженного файла не соответствует требованиям.'
        ];
    }
}
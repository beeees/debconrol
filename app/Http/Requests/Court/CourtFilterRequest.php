<?php

namespace App\Http\Requests\Court;

use App\Http\Requests\AppRequest;
use App\Models\Catalog\Court;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class CourtFilterRequest
 * @package App\Http\Requests\Court
 * @property int region_code
 * @property int court_type
 */
class CourtFilterRequest extends AppRequest
{
    public function sometimesRules(): array
    {
        return [
            'court_type' => 'int|in:' . implode(',', array_keys(Court::TYPES)),
            'region_code' => 'int|exists:regions,code',
        ];
    }

    public function prepareQuery(Builder $query): Builder
    {
        $query->where($this->only(array_keys($this->sometimesRules())));
        return parent::prepareQuery($query);
    }
}

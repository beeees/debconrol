<?php

namespace App\Http\Requests\Bailiff;

use App\Http\Requests\AppRequest;
use App\Models\Catalog\Court;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class BailiffFilterRequest
 * @package App\Http\Requests\Court
 * @property int region_code
 */
class BailiffFilterRequest extends AppRequest
{
    public function sometimesRules(): array
    {
        return [
            'region_code' => 'int|exists:regions,code',
        ];
    }

    public function prepareQuery(Builder $query): Builder
    {
        $query->where($this->only(array_keys($this->sometimesRules())));
        return parent::prepareQuery($query);
    }
}

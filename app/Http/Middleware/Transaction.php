<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Transaction
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (array_search($request->getMethod(), ['PUT','POST','DELETE']) === false) {
            return $next($request);
        }
        DB::beginTransaction();
        $response = $next($request);
        if ($response->exception) {
            DB::rollBack();
        } else {
            DB::commit();
        }
        return $response;
    }
}

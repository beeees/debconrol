<?php

namespace App\Jobs;

use App\Exceptions\UnknownCaseCheckTypeException;
use App\Models\Company\CompanyCase;
use App\Models\Company\CompanyUploadedFile;
use App\Models\Debtor;
use App\Models\DebtorCheckResult;
use App\Models\ParserTask;
use App\Utils\CompanyCaseDataUploadFormat;
use Carbon\Carbon;
use Facades\App\Utils\ParserApi;
use function GuzzleHttp\Promise\task;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class GetParserTaskResultsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $fileModel;
    private $task;

    /**
     * Create a new job instance.
     *
     * @param CompanyUploadedFile $file
     * @param ParserTask|null $task
     */
    public function __construct(?CompanyUploadedFile $file = null, ?ParserTask $task = null)
    {
        $this->fileModel = $file;
        $this->task = $task;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $files = $this->fileModel ? [$this->fileModel]: [];
        if (!$files && !$this->task) $files = CompanyUploadedFile::query()
            ->whereHas('debtors', function ($q) {
                $q->whereNull('finished_at');
            })->get();
        foreach ($files as $file){
            if (!$file->task_code) continue;
            info('Check task - ' . $file->task_code);
            $this->handleTask($file->task_code, $file->companyCase->check_type, $file->debtors(), $file->task_type);
        }
        $tasks = [];
        if ($this->task && !$this->fileModel) {
            $tasks = [$this->task];
        } elseif (!$this->task && !$this->fileModel) {
            $tasks = ParserTask::query()->whereHas('debtorsFsspDeals', function($q) {
                $q->whereHas('debtor', function($q2) {
                    $q2->whereNull('finished_at');
                });
            })->get();
        }
        foreach ($tasks as $task) {
            $progress_items = $this->handleTask($task->task_code, CompanyCase::FSSP_CHECK_TYPE, Debtor::query()
                ->whereHas('fsspDeals', function($q) use ($task){
                    $q->whereHas('parserTasks', function ($q2) use ($task){
                        $q2->whereKey($task->id);
                    });
                }),
                $task->task_type
            );
            $task->update(
                compact('progress_items') +
                [
                    'finished_at' => $progress_items === $task->items ? Carbon::now() : null,
                ]
            );
        }
    }

    public function handleTask(string $taskCode, int $checkType, $debtorsQueryIn, $taskType = null) : int
    {
        $progress_items = 0;
        $categoryMatch = function($category, $subject) {
            return preg_match("/\W$category\W|^$category$|^$category\W|\W$category$/", mb_strtolower($subject));
        };
        $response = ParserApi::getTaskData($taskCode);
        foreach ($response['items'] as $i => $item) {
            if (!$item['entity']['completed']) continue;
            $debtorsQuery = clone $debtorsQueryIn;
            try {
                $field = null;
                if ($checkType === CompanyCase::ASSESSED_CHECK_TYPE) {
                    $debtorsQuery->where((array_filter([
                        'regionCode' => $item['entity']['regionCode'] . '',
                        'name' => $item['entity']['name'],
                        'secondname' => $item['entity']['secondname'],
                        'lastname' => $item['entity']['lastname'],
                        'birthday' => preg_match('/[0-9]{2}\.[0-9]{2}\.[0-9]{4}/', $v = $item['entity']['birthday'])
                            ? Carbon::parse($v) : null,
                    ])));
                } else if ($checkType === CompanyCase::FSSP_CHECK_TYPE) {
                    $isDocumentType = $taskType === CompanyCaseDataUploadFormat::PARSER_API_PATH[CompanyCaseDataUploadFormat::FORMAT_FS_VS];
                    if (!Arr::get($item['entity'],'number') || $isDocumentType && !Arr::has($item['entity'], ['type'])
                        || !$isDocumentType && Arr::has($item['entity'], ['type']))
                        continue;
                    $field = $isDocumentType ? 'fs_number' : 'deal_number';
                    $debtorsQuery->whereHas('fsspDeals', function($q) use ($item, $field) {
                        $q->where($field, $item['entity']['number']);
                    });
                } else {
                    throw new UnknownCaseCheckTypeException();
                }
                $debtors = $debtorsQuery->whereNull('finished_at')->get();
                $progress_items += $debtors->count();
                /** @var Debtor $debtor */
                foreach ($debtors as $debtor) {
                    if (count($item['result'])) {
                        $results = [];
                        $debtorData = [];
                        foreach ($item['result'] as $result) {
                            $results[] = $r = DebtorCheckResult::query()->newModelInstance([
                                'region' => $result['region'],
                                'address' => $result['address'],
                                'ip_date' => ($d = $result['enforcementProceeding']['date']) ? Carbon::parse($d) : null,
                                'ip_number' => $result['enforcementProceeding']['number'],
                                'ip_summary_case' => $result['enforcementProceeding']['summaryCase'],
                                'id_date' => ($d = $result['executiveDocument']['date']) ? Carbon::parse($d) : null,
                                'id_type' => $result['executiveDocument']['type'],
                                'id_type_name' => $result['executiveDocument']['typeName'],
                                'id_number' => $result['executiveDocument']['number'],
                                'id_issuer' => $result['executiveDocument']['issuer'],
                                'ip_end_date' => ($d = $result['terminationEnforcementProceeding']['date']) ? Carbon::parse($d) : null,
                                'ip_end_reason' => $result['terminationEnforcementProceeding']['reason'],
                                'subject_name' => $result['subjectExecution']['name'],
                                'subject_amount' => $result['subjectExecution']['amount'],
                                'subject_fees' => $result['subjectExecution']['enforcementFees'],
                                'bailiff_department_name' => $result['bailiffDepartament']['name'],
                                'bailiff_department_post_index' => $result['bailiffDepartament']['postIndex'],
                                'bailiff_department_address' => $result['bailiffDepartament']['address'],
                                'bailiff_fullname' => $result['bailiff']['fullname'],
                                'bailiff_phones' => implode(',', $result['bailiff']['phones']),

                            ]);
                            if ($amount = $r->subject_amount) $debtorData['sum'] = Arr::get($debtorData, 'sum', 0) + $amount;
                            if ($r->ip_number) {
                                $debtorData['ip'] = Arr::get($debtorData, 'ip', 0) +1;
                                if ($r->region) $debtorData['regions'] = array_merge(Arr::get($debtorData, 'regions', []), [$r->region]);
                                if (($r->ip_end_date || $r->ip_end_reason)) $debtorData['ended'] = Arr::get($debtorData, 'ended', 0) +1;
                                if ($r->subject_name) {
                                    foreach ([
                                                 'zkh' => 'жкх',
                                                 'gibdd' => 'гибдд',
                                                 'credit' => 'кредит',
                                                 'ipoteka' => 'ипотека',
                                             ] as $category => $alias)
                                        if ($categoryMatch($alias, $r->subject_name)) {
                                            $debtorData[$category] = Arr::get($debtorData, $category, 0) +1;
                                            //gibdd_sum, zkh_sum, credit_sum, ipoteka_sum
                                            if ($amount) $debtorData[$category.'_sum'] = Arr::get($debtorData, $category.'_sum', 0) + $amount;
                                        }
                                }
                            }
                        }
                        DB::beginTransaction();
                        $transactionStarted = true;
                        if ($results) $debtor->checkResults()->saveMany($results);
                        $debtorData['repeatly'] = $debtor->checkResults()->where(function($q) {
                            $q->orWhereNotNull('ip_end_date')
                                ->orWhereNotNull('ip_end_reason');
                        })->groupByRaw('concat(id_type, \'_\', id_number)')->count();
                        $debtorData['regions'] = count(array_unique($debtorData['regions']));
                        if (!$debtor->getFullNameAttribute()) {
                            $debtorData += Arr::only($result, ['lastname', 'name', 'secondname', 'regionCode']) + [
                                'birthday' => ($d = $result['birthday']) ? Carbon::parse($d) : null,
                            ];
                        }
                        $debtor->fill($debtorData)->save();
                        if ($item['entity']['completed']) $debtor->finish()->save();
                        if ($checkType === CompanyCase::FSSP_CHECK_TYPE && $debtor && $results) {
                            if ($results && $field) {
                                $debtor->fsspDeals()->where($field, $item['entity']['number'])->update(['closed' => false]);
                            }
                        }
                        DB::commit();
                    } else if ($checkType === CompanyCase::FSSP_CHECK_TYPE && $debtor && ($old = $debtor->checkResults()->count())){
                        $debtor->checkResults()->delete();
                        //Если до удаления были записи а мы их удалили, то ставим отметку о том, что дело прекращено
                        if ($old > 0 && $field) {
                            $debtor->fsspDeals()->where($field, $item['entity']['number'])->update(['closed' => true]);
                        }
                    }
                }
            } catch (\Throwable $e) {
                if (isset($transactionStarted)) DB::rollBack();
                info($e->getMessage());
            }
        }
        return $progress_items;
    }
}

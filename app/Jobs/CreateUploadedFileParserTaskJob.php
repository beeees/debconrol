<?php

namespace App\Jobs;

use App\Models\Company\CompanyCase;
use App\Models\Company\CompanyUploadedFile;
use Facades\App\Utils\ParserApi;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CreateUploadedFileParserTaskJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $fileModel;
    private $ids;

    /**
     * Create a new job instance.
     *
     * @param CompanyUploadedFile $file
     * @param array|null $ids
     */
    public function __construct(CompanyUploadedFile $file, ?array $ids = null)
    {
        $this->fileModel = $file;
        $this->ids = $ids;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle()
    {
        $count = $this->fileModel->debtors()->count();
        if (!$count) return;
        $batchSize = 100;
        $task = [];
        $lastId = 0;
        $checkType = $this->fileModel->companyCase()->first()->check_type;
        do {
            $items = $this->fileModel->debtors()->orderBy('id')->limit($batchSize)->where('id', '>', $lastId);
            if ($checkType == CompanyCase::FSSP_CHECK_TYPE)
                $items->with('fsspDeals');
            if ($this->ids) $items->whereIn('debtors.id', $this->ids);
            $items= $items->get();
            $lastId = $items->count() ? $items->get($items->count() - 1)->id : $lastId;
            if (!$task) {
                $task = ParserApi::createTask($items, $this->fileModel->format, $checkType);
            } else {
                $task = ParserApi::appendToTask($items, $task['taskCode'], $checkType);
            }
            $count -= $items->count();
        } while($count > 0);
        $this->fileModel->task_code = $task['taskCode'];
        $this->fileModel->task_type = $task['taskType'];
        $this->fileModel->save();
    }
}

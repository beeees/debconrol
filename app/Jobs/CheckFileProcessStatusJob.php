<?php

namespace App\Jobs;

use App\Exceptions\AppException;
use App\Exceptions\UnknownCaseCheckTypeException;
use App\Models\Company\CompanyCase;
use App\Models\Company\CompanyUploadedFile;
use Carbon\Carbon;
use Facades\App\Utils\ParserApi;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CheckFileProcessStatusJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $fileModel;
    /**
     * Create a new job instance.
     *
     * @param CompanyUploadedFile $file
     */
    public function __construct(?CompanyUploadedFile $file = null)
    {
        $this->fileModel = $file;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws UnknownCaseCheckTypeException
     */
    public function handle()
    {
        $files = $this->fileModel ? [$this->fileModel] : CompanyUploadedFile::query()->whereNull('finished_at')->whereHas('companyCase')->get();
        foreach ($files as $file)
            switch ($file->companyCase()->first()->check_type) {
                case CompanyCase::ASSESSED_CHECK_TYPE:
                case CompanyCase::FSSP_CHECK_TYPE:
                    $this->handleNotFullCheckCaseFile($file);
                    break;
                case CompanyCase::FULL_CHECK_TYPE:
                    $this->handleFullCheckCaseFile($file);
                    break;
                case CompanyCase::DEAL_CHECK_TYPE:
                    $this->handleFullCheckCaseFile($file);
                    break;
                default: throw new UnknownCaseCheckTypeException();
            }

    }
    public function handleDealsCheckCaseFile(CompanyUploadedFile $file) {

        $data = [
            'progress_items' => $file->debtors->whereNotNull('finished_at')->count(),
        ];
        if (!$file->debtors()->whereNull('finished_at')->count())
            $data['finished_at'] = Carbon::now();
        $file->fill($data)->save();
    }
    public function handleFullCheckCaseFile(CompanyUploadedFile $file) {

        $data = [
            'progress_items' => $file->debtors->whereNotNull('finished_at')->count(),
        ];
        if (!$file->debtors()->readyForCheckCourt()->whereNull('finished_at')->count())
            $data['finished_at'] = Carbon::now();
        $file->fill($data)->save();
    }

    public function handleNotFullCheckCaseFile(CompanyUploadedFile $file) {
        if ($file->task_code && ($status = ParserApi::getTaskStatus($file->task_code))){
            $data = [
                'progress_items' => $status['entries']['completed'],
            ];
            if ($status['finished']) $data['finished_at'] = Carbon::now();
            $file->fill($data)->save();
            if ($file->finished_at) dispatch(new GetParserTaskResultsJob($this->fileModel));
        }
    }
}

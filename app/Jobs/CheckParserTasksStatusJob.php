<?php

namespace App\Jobs;

use App\Exceptions\AppException;
use App\Exceptions\UnknownCaseCheckTypeException;
use App\Models\Company\CompanyCase;
use App\Models\Company\CompanyUploadedFile;
use App\Models\ParserTask;
use App\Utils\CompanyCaseDataUploadFormat;
use Carbon\Carbon;
use Facades\App\Utils\ParserApi;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;

class CheckParserTasksStatusJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $task;
    /**
     * Create a new job instance.
     *
     * @param CompanyUploadedFile $task
     */
    public function __construct(?ParserTask $task = null)
    {
        $this->task = $task;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws UnknownCaseCheckTypeException
     */
    public function handle()
    {
        $tasks = $this->task ? [$this->task] :
            ParserTask::query()->whereNull('finished_at')->get();
        $error = null;
        foreach ($tasks as $task) {
            if ((is_int(array_search($task->task_type, CompanyCaseDataUploadFormat::PARSER_API_PATH)))) {
                try {
                    $this->handleAssessedAndFsspCaseTask($task);
                } catch (\Throwable $e ) {
                    $error = $e;
                }
            } else {
                $error = new UnknownCaseCheckTypeException();
            }
        }
        if ($error) throw $error;

    }

    public function handleAssessedAndFsspCaseTask(ParserTask $task) {
        if (($task->task_code) && ($status = ParserApi::getTaskStatus($task->task_code))){
            $data = [
                'progress_items' => $status['entries']['completed'],
            ];
            if ($status['finished']) $data['finished_at'] = Carbon::now();
            $task->fill($data)->save();
            if ($task->finished_at) dispatch(new GetParserTaskResultsJob(null, $task));
        }
    }
}

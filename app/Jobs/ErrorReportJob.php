<?php

namespace App\Jobs;

use App\Mail\TextMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Mail;

class ErrorReportJob extends ReportJob
{
    protected $error;

    /**
     * Create a new job instance.
     *
     * @param \Throwable|string $e
     * @param string $msg
     * @param bool $onlyDev
     */
    public function __construct(\Throwable $error, string $msg, bool $onlyDev = false)
    {
        parent::__construct($msg, $onlyDev);
        $this->error = [
            'msg' => $error->getMessage(),
            'trace' => $error->getTraceAsString(),
        ];
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->error['msg'] . PHP_EOL . $this->error['trace'];
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return 'Ошибка обработки данных на сайте '. config('app.name');
    }

}

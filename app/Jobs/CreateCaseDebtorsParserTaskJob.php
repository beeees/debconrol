<?php

namespace App\Jobs;

use App\Exceptions\UnknownCaseCheckTypeException;
use App\Models\Company\CompanyCase;
use App\Models\Company\CompanyUploadedFile;
use App\Models\DebtorFsspDeal;
use App\Models\Debtor;
use App\Models\ParserTask;
use App\Utils\CompanyCaseDataUploadFormat;
use Carbon\Carbon;
use Facades\App\Utils\ParserApi;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CreateCaseDebtorsParserTaskJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $debtors;
    private $checkType;

    /**
     * Create a new job instance.
     *
     * @param array|int[] $debtors
     * @param int $caseCheckType
     * @throws UnknownCaseCheckTypeException
     */
    public function __construct(array $debtors, int $caseCheckType)
    {
        if ($caseCheckType !== CompanyCase::FSSP_CHECK_TYPE) {
            throw new UnknownCaseCheckTypeException();
        }
        $this->debtors = array_unique(array_values($debtors));
        $this->checkType = $caseCheckType;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle()
    {
        $count = count($this->debtors);
        if (!$count) return;
        $formatDealsData = [];
        $formatDealsIds = [];
        $debtors = Debtor::query()->with('fsspDeals', 'deals')->whereIn('debtors.id', $this->debtors)
            ->orderBy('id');
        $items = $debtors->get();
        foreach ($items as $item) {
            $deals = $item->fsspDeals()->get();
            if ($deals && count($deals)) {
                foreach ($deals as $deal) {
                    /** @var DebtorFsspDeal $deal */
                    if($deal->deal_number) {
                        $formatDealsData[CompanyCaseDataUploadFormat::FORMAT_PROCEEDING][] = [
                            'number' => $deal->deal_number,
                        ];
                        $formatDealsIds[CompanyCaseDataUploadFormat::FORMAT_PROCEEDING][] = $deal->id;
                    } else if ($deal->fs_number) {
                        $formatDealsData[CompanyCaseDataUploadFormat::FORMAT_FS_VS][] = [
                            'number' => $deal->fs_number,
                            'type' => 0,
                            'regionCode' => $item->regionCode ?: 0,
                        ];
                        $formatDealsIds[CompanyCaseDataUploadFormat::FORMAT_FS_VS][] = $deal->id;
                    }
                }
            }
            $debtors->update(['finished_at' => null]);
        }
        foreach ($formatDealsData as $format => $items) {
            $task = ParserApi::createFsspTask($items, $format);
            $task = new ParserTask([
                'task_code' => $task['taskCode'],
                'task_type' => $task['taskType'],
                'items' => count($items),
            ]);
            $task->save();
            $task->debtorsFsspDeals()->sync(($formatDealsIds[$format]));
            dispatch(new GetParserTaskResultsJob(null, $task));
        }
    }
}

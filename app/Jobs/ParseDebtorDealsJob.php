<?php

namespace App\Jobs;

use App\Console\Commands\Parsers\RegionParsers\RegionCourtParser;
use App\Console\Commands\Parsers\SudrfParser;
use App\Models\Company\CompanyUploadedFile;
use App\Models\Debtor;
use App\Models\DebtorDeal;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\Builder;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class ParseDebtorDealsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $fileId;
    protected $limit = 50;
    protected $lastId = null;
    protected $ids = [];
    protected $reparse = false;
    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 1200;

    /**
     * Create a new job instance.
     *
     * @param int $fileId
     * @param int $limit
     * @param int|null $lastId
     * @param array|null $ids
     * @param bool $reparse
     */
    public function __construct(int $fileId, int $limit = 10, ?int $lastId = null, ?array $ids = null, bool $reparse = false)
    {
        $this->fileId = $fileId;
        $this->lastId = $lastId;
        $this->limit = $limit;
        $this->ids = $ids;
        $this->reparse = $reparse;
    }

    /**
     * @param Builder|Debtor $query
     * @return Builder|Debtor
     */
    protected function prepareQuery($query)
    {
        return $query->whereHas('deals');
    }
    /**
     * Execute the job.
     *
     * @return void
     * @throws \Throwable
     */
    public function handle()
    {
        $idsWhere = $this->ids ?  function($q) { $q->whereIn('debtors.id', $this->ids);}: [];
        ($file = CompanyUploadedFile::query()->find($this->fileId));
        $debtors = $file ? $file->debtors() : Debtor::query();
        if (($count = $this->prepareQuery($debtors)->where($idsWhere)->count())) {
            try {
                $debtors = $this->prepareQuery($debtors)
                    ->where($idsWhere)
                    ->orderBy('id')->limit($this->limit);
                if (!is_null($this->lastId))
                    $debtors->where('debtors.id', '>', $this->lastId);
                $debtors = $debtors->get();
                if (!$debtors->count()) return;
                $this->handleBatch($debtors);
                if ($file) $file->updateItems()->save();
                dispatch(new static($this->fileId, $this->limit, $this->lastId, $this->ids, $this->reparse));
            } catch (\Throwable $e) {
                if ($this->lastId) dispatch(new static($this->fileId, $this->limit, $this->lastId+1, $this->ids, $this->reparse));
                dispatch_now(new ErrorReportJob($e, $file ? 'Ошибка обработки для портфеля №' . $file->case_id : 'Ошибка обработки данных по делам должников ' .json_encode($this->ids)));
                throw $e;
            }
        }
    }

    /**
     * @param Collection|Debtor[] $debtors
     */
    protected function handleBatch($debtors) : void
    {
        foreach ($debtors as $debtor) {
            /** @var DebtorDeal $deal */
            $deal = $debtor->deals()->first();
            if (!($url = $deal->getParseDomain())) {
                $debtor->setError(Debtor::ERROR_NO_DEAL_URL);
            }
            $parser = RegionCourtParser::getParserByCourtUrl($url);
            if (!$parser && strpos($url, '.sudrf.ru') !== false) $parser = new SudrfParser($url);
            if (!$parser) $debtor->setError(Debtor::ERROR_UNKNOWN_PARSER);
            else {
                if (!$debtor->regionCode && ($r = $parser->getRegionCode())) $debtor->regionCode = $r;
                if (($dn = $deal->deal_number) && $debtor->regionCode) {
                    $exists = 0 < Debtor::query()->where($debtor->only(['case_id', 'regionCode']))
                            ->whereHas('deals', function ($q) use($dn){
                                $q->where('deal_number', $dn);
                            })->whereKeyNot($debtor->id)->count();
                    if ($exists) {
                        $debtor->forceDelete();
                        continue;
                    }
                }
                echo $debtor->id;
                if (!$this->reparse) {
                    $parser->checkAndFillDeal($deal, $debtor);
                } else {
                    try {
                        DB::beginTransaction();
                        $deal = new DebtorDeal($deal->only([
                            'loaded_initiator',
                            'act_number',
                            'court_urls',
                            'deal_init_date',
                            'deal_number',
                        ]));
                        $debtor->deals()->delete();
                        $debtor->deals()->save($deal);
                        $debtor->error = null;
                        $debtor->finished_at = null;
                        $debtor->save();
                        $parser->checkAndFillDeal($deal, $debtor);
                        DB::commit();
                    } catch (\Throwable $e) {
                        dispatch_now(new ErrorReportJob($e, 'Ошибка обработки повторной обработки должника из портфеля №' . $debtor->case_id));
                        DB::rollBack();
                        return;
                    }
                }
                if (($docsDeal = $debtor->deals()->where('deal_number', '~*', 'м\-\d+')->first())
                    && preg_match('/м\-([\d\-\/]+)/ui',$docsDeal->deal_number, $m)) {
                    if ($debtor->deals()->where('deal_number', '~*', $m[1])->whereKeyNot($docsDeal->id)->count())
                        $docsDeal->delete();
                }
            }
            $debtor->finish()->save();
            $this->lastId = $debtor->id;
            if (config('queue.default') !== 'sync')
                dispatch(new self($this->fileId, $this->limit, null, [$debtor->id], true))
                    ->delay(now()->addDays($deal->deal_end_date ? now()->daysInMonth : Carbon::DAYS_PER_WEEK));
        }
    }
}

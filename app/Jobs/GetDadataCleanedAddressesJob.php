<?php

namespace App\Jobs;

use App\Models\Catalog\Region;
use App\Models\Company\CompanyUploadedFile;
use App\Models\DadataAddressRecord;
use App\Models\DadataFioRecord;
use App\Models\Debtor;
use Carbon\Carbon;
use Facades\App\Utils\DadataApi;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;

class GetDadataCleanedAddressesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $fileModelId;
    protected $debtorId;

    /**
     * Create a new job instance.
     *
     * @param int $fileModelId
     * @param int $debtorId
     */
    public function __construct(int $fileModelId, ?int $debtorId =null)
    {
        $this->fileModelId = $fileModelId;
        $this->debtorId = $debtorId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->fileModelId && ($file = CompanyUploadedFile::query()->find($this->fileModelId))) {
            $debtors = $file->debtors()->where(function ($q) {
                return $q->whereHas('dadataFio')
                    ->whereHas('dadataAddress', Carbon::parse('2021-07-22')->gte(Carbon::now())
                        ? function($qA) {
                            return $qA->whereNotNull('result');
                        } : null
                    );
            })->get();
            /** @var Debtor $debt */
            foreach ($debtors?:[] as $debt) {
                $a = ($a = $debt->dadataAddress()->first()) ? $a->result : null;
                DadataAddressRecord::fillDebtorByResultArray($debt, $a);
                if ($n = ($n = $debt->dadataFio()->first()) ? $n->result : null)
                    DadataFioRecord::fillDebtorByResultArray($debt, $n);
                if ($debt->isDirty()) $debt->save();
            }
            $debtors = $file->debtors()->where(function ($q) {
                $q->orWhereDoesntHave('dadataFio')
                    ->orWhereDoesntHave('dadataAddress');
                if (Carbon::parse('2021-07-22')->gte(Carbon::now()))
                    $q->orWhereHas('dadataAddress', function($qA) {
                        return $qA->whereNull('result');
                    });
                return $q;
            })->whereNull('error')->get();
        } else if ($this->debtorId && $d = Debtor::query()->findOrFail($this->debtorId)) {
            if(($record = $d->dadataAddress()->first()) && $result = $record->result) {
                DadataAddressRecord::fillDebtorByResultArray($d, $result);
                $d->save();
                $debtors = [];
            } else $debtors = new Collection([$d]);
        } else return;
        $ids = $this->debtorId ? [$this->debtorId] : null;
        if (!$debtors || !$debtors->count()) {
            dispatch(new CheckDebtorCourtJob($this->fileModelId, 10, null, $ids));
            return;
        }
        $count = $debtors->count();
        for ($start = 0, $length = 50;$start < $count; $start+= $length) {
            $data = [];
            foreach ($debtors->slice($start, $count > $start+$length ? $length : $count - $start) as $debt) {
                $data[] = [$debt->fio, $debt->loaded_address, $debt->id];
            }
            if (!count($data)) continue;
            $response = DadataApi::cleanRecords($data);
            $regions = Region::query()->get()->keyBy('code');
            foreach ($response as $record) {
                $name = $record[0];
                $address = $record[1];
                $c = $debtors->find((int)$record[2]['source']);
                DadataFioRecord::fillDebtorByResultArray($c, $name);
                if ($name['qc'] < 2) {
                    DadataFioRecord::query()->firstOrCreate(['fio' => mb_strtolower(trim($c->fio))], ['result' => $name]);
                }
                DadataAddressRecord::fillDebtorByResultArray($c, $address);
                DadataAddressRecord::query()->firstOrCreate([
                        'address' => (mb_strtolower(trim($c->loaded_address)))], ['result' => $address])
                    ->save();
                if (Arr::get($address, 'result'))
                    DadataAddressRecord::query()->firstOrCreate([
                        'address' => (mb_strtolower(trim($address['result'])))], ['result' => $address])
                        ->save();
                $c->save();
                if ($address && ($r = $c->regionCode) && !$regions->offsetExists($r)) {
                    Region::query()->firstOrCreate(['code' => $r], ['name' => $address['region']]);
                }
            }
        }
        dispatch(new CheckDebtorCourtJob($this->fileModelId,10, null, $ids));
    }
}

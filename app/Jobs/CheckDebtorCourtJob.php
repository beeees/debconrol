<?php

namespace App\Jobs;

use App\Models\Catalog\CourtArea;
use App\Models\Catalog\Region;
use App\Models\Company\CompanyUploadedFile;
use App\Models\Debtor;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;

class CheckDebtorCourtJob extends ParseDebtorDealsJob
{
    /**
     * @param Builder|Debtor $query
     * @return Builder|Debtor
     */
    protected function prepareQuery($query)
    {
        return $query->readyForCheckCourt();
    }

    protected function handleBatch($debtors): void
    {
        foreach ($debtors as $debtor) {
            $debtor->load('region', 'dadataAddress');
            /** @var Region $r */
            if (!($r = $debtor->getRelation('region')) || !$debtor->getRelation('dadataAddress')) continue;
            /** @var Debtor $debtor */
            $courts = $r->courts()->magistrate()->whereHas('area', function ($q) use ($debtor) {
                /** @var CourtArea $q */
                $q->byAddress($debtor->getRelation('dadataAddress'));
            })->get(['courts.id'])->pluck('id')->toArray();
            $debtor->courts()->attach($courts);
            if ($this->reparse) {
                $deletedCourts = $debtor->courts()->whereNotNull('deleted_at')->get(['id'])->pluck('id')->toArray();
                if ($courts && $deletedCourts) {//если после обновления списка судов есть соответствующий адресу новый, старые - удаляем
                    $debtor->courts()->detach($deletedCourts);
                }
            }
            $debtor->update(['finished_at' => Carbon::now()]);
            $this->lastId = $debtor->id;
        }

    }
}

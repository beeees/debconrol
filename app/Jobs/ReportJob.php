<?php

namespace App\Jobs;

use App\Mail\TextMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Mail;

class ReportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $error;
    protected $msg;
    protected $onlyDev;

    /**
     * Create a new job instance.
     *
     * @param \Throwable|string $e
     * @param string $msg
     * @param bool $onlyDev
     */
    public function __construct(string $msg, bool $onlyDev = false)
    {
        $this->msg = $msg;
        $this->onlyDev = $onlyDev;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return '';
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return 'Отчет о работе сайте '. config('app.name');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $message = static::getMessage();
        $emails = config('mail.report_mails');
        if ($this->onlyDev && is_array($emails)) $emails = [Arr::last($emails)];
        Mail::to($emails)->send(new TextMail($this->msg .PHP_EOL. $message, static::getSubject()));
    }
}

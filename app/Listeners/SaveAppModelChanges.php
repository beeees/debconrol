<?php

namespace App\Listeners;

use App\Events\ModelUpdated;
use App\Models\ChangeHistory;
use App\Models\Users\UserHistory;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SaveAppModelChanges
{

    /**
     * Handle the event.
     *
     * @param  ModelUpdated  $event
     * @return void
     */
    public function handle(ModelUpdated $event)
    {
        if (!$event->changes) return;
        $h = $event->model->histories()->newModelInstance();
        /**@var \App\Models\ChangeHistory $h*/
        $h->loadChangesFromArray($event->changes);
        $h->model_id = $event->model->id;
        $h->save();
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: jura120596
 * Date: 3/11/21
 * Time: 10:18 AM
 */

namespace App\Utils;


use App\Models\Company\CompanyCase;
use App\Models\Debtor;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use function Symfony\Component\String\b;

/**
 * Class ParserApi
 * @package App\Utils
 */
class ParserApi
{
    private $client;
    public const CASE_CHECK_TYPE = CompanyCase::ASSESSED_CHECK_TYPE;
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param Collection $debtors
     * @param int $format
     * @param int $checkType
     * @return array
     * @throws GuzzleException
     */
    public function createTask(Collection $debtors, int $format, int $checkType = CompanyCase::ASSESSED_CHECK_TYPE) : array
    {
        $path = '/task/create/' . CompanyCaseDataUploadFormat::PARSER_API_PATH[$format];
        $body = [];
        $maxId = null;
        /** @var Debtor $debtor */
        foreach ($debtors as $debtor) {
            $data = $this->getDebtorBodyData($debtor, $checkType, $format);
            if ($data) $body[] = $data;
        }
        if (!$body) return [];
        $response = $this->client->post($path, [
            RequestOptions::JSON => $body
        ]);
        if ($response->getStatusCode() === 200) {
            return json_decode($response->getBody()->getContents(), true);
        }
    }

    /**
     * @param array $debtors
     * @param int $format
     * @return array
     * @throws GuzzleException
     */
    public function createFsspTask(array $debtors, int $format) : array
    {
        $path = '/task/create/' . CompanyCaseDataUploadFormat::PARSER_API_PATH[$format];
        if (!$debtors) return [];
        $response = $this->client->post($path, [
            RequestOptions::JSON => $debtors
        ]);
        if ($response->getStatusCode() === 200) {
            return json_decode($response->getBody()->getContents(), true);
        }
    }

    /**
     * @param Collection $debtors
     * @param string $taskCode
     * @param int $format
     * @param int $checkType
     * @return array
     * @throws GuzzleException
     */
    public function appendToTask(Collection $debtors, string $taskCode, int $format, int $checkType = CompanyCase::ASSESSED_CHECK_TYPE) : array
    {
        $path = '/task/' . $taskCode . '/add' ;
        $body = [];
        $maxId = null;
        foreach ($debtors as $debtor) {
            $data = $this->getDebtorBodyData($debtor, $checkType, $format);
            if ($data) $body[] = $data;
        }
        if (!$body) return [];
        $response = $this->client->post($path, [
            RequestOptions::JSON => $body
        ]);
        if ($response->getStatusCode() === 200) {
            return json_decode($response->getBody()->getContents(), true);
        }
    }

    /**
     * @param Debtor $debtor
     * @param int $checkType
     * @param int $format
     * @return array|null
     *
     */
    public function getDebtorBodyData(Debtor $debtor, int $checkType, int $format) : ?array
    {
        if ($checkType === CompanyCase::FSSP_CHECK_TYPE) {
            $deal = $debtor->fsspDeals()->first();
            if ($deal && $deal->deal_number) {
                return [
                    'number' => $debtor->fsspDeals()->first()->deal_number,
                ];
            }
        } else {
            $d = $debtor->only(CompanyCaseDataUploadFormat::DEBTOR_FIELDS[$checkType][$format]);
            if ($b = Arr::get($d, 'birthday')) $d['birthday'] = Carbon::parse($b)->format('d.m.Y');
            return $d ?: null;
        }
        return null;
    }

    /**
     * @param string $taskCode
     * @return array
     * @throws GuzzleException
     */
    public function getTaskData(string $taskCode) : array
    {
        $path = '/task/' . $taskCode . '/data' ;
        $response = $this->client->get($path);
        if ($response->getStatusCode() === 200) {
            return json_decode($response->getBody()->getContents(), true);
        }
    }

    /**
     * @param string $taskCode
     * @return array
     * @throws GuzzleException
     */
    public function getTaskStatus(string $taskCode) : array
    {
        $path = '/task/' . $taskCode . '/status' ;
        $response = $this->client->get($path);
        if ($response->getStatusCode() === 200) {
            return json_decode($response->getBody()->getContents(), true);
        }
    }

    /**
     * @param string $base64
     * @return array
     * @throws GuzzleException
     */
    public function captcha(string $base64) : array
    {
        $path = '/captcha/adler/' ;
        $response = $this->client->post($path, [
            RequestOptions::JSON => [
                'img' => $base64,
            ],
        ]);
        if ($response->getStatusCode() === 200) {
            return json_decode($response->getBody()->getContents(), true);
        }
    }
}
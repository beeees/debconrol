<?php


namespace App\Utils;


use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

/**
 * Class AntikapchaApi
 * @package App\Utils
 * @method static array createImageToTextTask(string $base64Image)
 * @method static array getTaskResult(string $base64Image)
 */
class AntikapchaApi
{
    private $client;
    private $clientKey;
    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->clientKey = config('services.antikapcha.key');
    }

    public function createImageToTextTask(string $base64Image) : array
    {
        $response = $this->client->post('/createTask', [
            RequestOptions::JSON => [
                'clientKey' => $this->clientKey,
                'task' => [
                    'type' => 'ImageToTextTask',
                    'body' => $base64Image,
                ],
                'languagePool' => 'rn',
            ]
        ]);
        return json_decode($response->getBody()->getContents(), true);
    }
    public function getTaskResult(string $taskId) : array
    {
        $response = $this->client->post('/getTaskResult', [
            RequestOptions::JSON => [
                'clientKey' => $this->clientKey,
                'taskId' => $taskId,
            ]
        ]);
        return json_decode($response->getBody()->getContents(), true);
    }


}

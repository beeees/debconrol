<?php


namespace App\Utils;
use App\Models\Company\CompanyCase;

/**
 * Class CompanyCaseFormat
 * @package App\Utils
 */
class CompanyCaseDataUploadFormat
{
    public const FORMAT_MIX = -1;
    public const FORMAT_FULL = 0;
    public const FORMAT_BIRTHDAY = 1;
    public const FORMAT_REGION = 2;
    public const FORMAT_FS_VS = 3;
    public const FORMAT_PROCEEDING  = 4;
    public const PARSER_API_PATH = [
        self::FORMAT_FULL => 'full',
        self::FORMAT_REGION => 'region',
        self::FORMAT_BIRTHDAY => 'birthday',
        self::FORMAT_FS_VS => 'document',
        self::FORMAT_PROCEEDING => 'proceeding',
    ];
    public const ALL = [
        CompanyCase::ASSESSED_CHECK_TYPE => [
            self::FORMAT_FULL => ['Метка', 'Фамилия', 'Имя', 'Отчество', 'Дата рождения', 'Код региона'],
            self::FORMAT_BIRTHDAY => ['Метка', 'Фамилия', 'Имя', 'Отчество', 'Дата рождения'],
            self::FORMAT_REGION => ['Метка', 'Фамилия', 'Имя', 'Отчество', 'Код региона'],
        ],
        CompanyCase::FULL_CHECK_TYPE => [
            self::FORMAT_FULL => ['Метка', 'Фамилия Имя Отчество', 'Адрес'],
        ],
        CompanyCase::DEAL_CHECK_TYPE => [
            self::FORMAT_FULL => ['Метка','Номер договора','ФИО','Дата рождения','Истец','Сайт суда','Дата подачи в суд','Номер дела',],
        ],
        CompanyCase::FSSP_CHECK_TYPE => [
            self::FORMAT_PROCEEDING => ['Метка', 'Номер исполнительного производства']
        ],
    ];
    public const EXCEL_HEADERS = [
        CompanyCase::ASSESSED_CHECK_TYPE => [
            self::FORMAT_FULL => ["metka", "familiya", "imya", "otcestvo", "data_rozdeniya", "kod_regiona"],
            self::FORMAT_BIRTHDAY => ["metka", "familiya", "imya", "otcestvo", "data_rozdeniya"],
            self::FORMAT_REGION => ["metka", "familiya", "imya", "otcestvo", "kod_regiona"],
        ],
        CompanyCase::FULL_CHECK_TYPE => [
            self::FORMAT_FULL => ["metka", "familiya_imya_otcestvo", "adres"],
        ],
        CompanyCase::DEAL_CHECK_TYPE => [
            self::FORMAT_FULL => ['metka','nomer_dogovora','fio','data_rozdeniya','istec', 'sait_suda','data_podaci_v_sud','nomer_dela',],
        ],
        CompanyCase::FSSP_CHECK_TYPE => [
            self::FORMAT_PROCEEDING => ['metka', 'nomer_ispolnitelnogo_proizvodstva'],
        ],
    ];
    public const DEBTOR_FIELDS = [
        CompanyCase::ASSESSED_CHECK_TYPE => [
            self::FORMAT_FULL => ['name', 'lastname', 'secondname', 'regionCode', 'birthday',],
            self::FORMAT_BIRTHDAY => ['name', 'lastname', 'secondname', 'birthday',],
            self::FORMAT_REGION => ['name', 'lastname', 'secondname', 'regionCode',],
        ],
        CompanyCase::FULL_CHECK_TYPE => [
            self::FORMAT_FULL => ['name', 'loaded_address'],
        ],
        CompanyCase::DEAL_CHECK_TYPE => [
            self::FORMAT_FULL => ['fio','birthday',],
        ],
        CompanyCase::FSSP_CHECK_TYPE => [
            self::FORMAT_PROCEEDING => ['deal_number'],
            self::FORMAT_MIX => ['name', 'lastname', 'secondname', 'regionCode', 'birthday',],
        ],
    ];
}
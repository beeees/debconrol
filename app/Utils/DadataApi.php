<?php

namespace App\Utils;


use App\Jobs\ErrorReportJob;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Storage;

class DadataApi
{

    public $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param $data [[FIO, real address]]
     * @param array $structure
     * @return array|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     * DOCUMENTATION: https://dadata.ru/api/clean/record/
     */
    public function cleanRecords($data, $structure = ['NAME', 'ADDRESS', 'AS_IS'])
    {
        try {
            $response = $this->client->post('clean', [
                RequestOptions::JSON => $d = compact('structure', 'data'),
            ]);
        } catch (ClientException $e) {
            dispatch_now(new ErrorReportJob($e, 'При обращение к сервису dadata произошла ошибка, пожалуйста проверьте баланс в сервисе'));
            throw $e;
        }
        Storage::disk('public')->put('dadata/'.Carbon::now()->format('Y/m/d/H_i_s').'.json',json_encode($d));
        return json_decode($response->getBody()->getContents(), true)['data'];

    }
}
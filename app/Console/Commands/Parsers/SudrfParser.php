<?php

namespace App\Console\Commands\Parsers;


use App\Console\Commands\AbstractParser;
use App\Console\Commands\Parsers\RegionParsers\RegionCourtParser;
use App\Console\Commands\RegionParsersInfo;
use App\Exceptions\LargeHtmlException;
use App\Models\Catalog\Court;
use App\Models\Catalog\CourtRequisite;
use App\Models\Catalog\CourtStructure;
use App\Models\Debtor;
use App\Models\DebtorDeal;
use App\Models\DebtorException;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;


/**
 * Class KomiParser
 * @package App\Console\Commands\Regions
 */
class SudrfParser extends AbstractParser
{
    public const ADMINISTARTE_DEAL_CATEGORY_PARAM = 1540005;
    public const SEARCH_NAME_OP_PARAM = 'r';
    public const PARSER_ROOT_PAGE = 'https://sudrf.ru';
    protected $mainPageUrl = '/index.php';
    protected $informationPageUrl = '/modules.php?name=information';
    protected $aboutCourtPageUrl = '/modules.php?name=info_court';
    protected $magistrateCourtsListUrl = '/ms_list.php?region_id=';
    protected $dealUrl = '/modules.php?name=sud_delo';

    public function __construct(?string $siteUrl)
    {
        parent::__construct($siteUrl);
        $this->setHtml($this->getHtml());
    }

    public function getUrl(): string
    {
        return $this->url . $this->mainPageUrl;
    }

    public function setHtml(string $str): void
    {
        $str = preg_replace('/(<\/(div|a|tr|p|span|table|td|li|ul)>)([^<>]*)(<(div|a|tr|p|span2|table|td|li|ul))/', '$1$3' . self::TAG_DELIMETER . '$4', $str);
        parent::setHtml(mb_convert_encoding($str, "UTF-8", 'windows-1251'));
        $this->domHtml = str_get_html($this->getHtml());
    }


    /**
     * parse li items from https://sudrf.ru/index.php?id=300&act=go_search&searchtype=fs&court_subj=11&court_type=0
     * @param \simple_html_dom_node $ul
     * @return Court
     */
    public function makeCourtByIndexPageSimpleHtmlNode(\simple_html_dom_node $node): Court
    {
        $court = new Court();
        $div = self::clear($node->find('div', 0)->innertext());
        if (preg_match('/mailto:([^\'"]+)/ui', $div, $m)) {
            $court->email = $m[1];
        }
        if (preg_match('/телефон:\s?<\/b>([^<]+)</ui', $div, $m2)) {
            $court->phone = trim($m2[1]);
        }
        if (preg_match('/адрес:\s?<\/b>([^<]+)</ui', $div, $m3)) {
            $court->address = trim($m3[1]);
        }
        $court->region_code = $this->getRegionCode();
        $court->url =  $node->find('a[target]', 0)->innertext();
        $court->name = $node->find('a', 0)->innertext();
        return $court;
    }
    /**
     * @return array|Court[]
     */
    public function loadSupreme(): array
    {
        $this->changeUrl(self::PARSER_ROOT_PAGE . $this->mainPageUrl
            . '?id=300&act=go_search&searchtype=fs&court_subj='
            . str_pad($this->regionCode, 2, '0', STR_PAD_LEFT) . '&court_type=0');
        $ul = $this->domHtml->find('.search-results')[0];
        $court = $this->makeCourtByIndexPageSimpleHtmlNode($this->firstLinkNodeFromContainerNode($ul, 'li', ['областн|верховн|суд.*округа|краевой']));
        $court->court_type = Court::SUPREME_TYPE;
        $court->url = ($s = Arr::get(RegionParsersInfo::REGIONS[$this->regionCode], 'supreme', []))
            ? $s['url'] : $court->url;
        $court->name = $s ? $s['name'] :$court->name;
        $court->save();
        if ($this->needDebugCourt($court)) {
            if ($parser = RegionCourtParser::getParserByCourtUrl($court)) {
                try {
                    $parser->loadCourtRequisites($court);
                    $this->success('requisite', $court);
                } catch (\Throwable $e) {
                    $this->error('requisite', $court, $e);
                }
                try {
                    $parser->loadCourtStructure($court);
                    $this->success('structure', $court);
                } catch (\Throwable $e) {
                    $this->error('structure', $court, $e);
                }
            } else {
                $this->loadCourtRequisites($court);
                $this->loadCourtStructure($court);
            }
        }
        $this->supreme = [$court];
        return $this->supreme;
    }
    /**
     * @param Court $court
     */
    public function loadCourtRequisites(Court $court): void
    {
        try {
            $this->changeUrl($court->url . $this->informationPageUrl);
            foreach (CourtRequisite::STEP_MENU_KEYS as $keys){
                $link = $this->firstLinkNodeFromContainerNode($this->domHtml->root, 'div#divINFOMenu0 a', $keys);
                if ($link) break;
            }
            if (!$link) foreach (CourtRequisite::STEP_MENU_KEYS as $keys){
                $link = $this->firstLinkNodeFromContainerNode($this->domHtml->root, 'div#divINFODocsList a', $keys);
                if ($link) break;
            }
            if (!$link) {
                $this->error('requisite', $court, new \Exception('No menu link'));
                return;
            }
            $this->changeUrl($court->url . $link->getAttribute('href'));
            $menus  = ['divINFOMenu0'];
            $requisite = null;
            $text = null;
            $checkOnDomPage = function() use(&$requisite, &$text, $court) {
                $textNode = $this->firstElementByIdPrefix('divINFODocText');
                if ($textNode) {
                    $text = trim($textNode->text());
                    $requisite = $this->getRequisiteByText($text);
                }
                if (!$requisite && ($file = $this->firstElementByIdPrefix('divINFODocFile'))
                    &&$link = $this->firstLinkNodeFromContainerNode($file, 'a', ['скачать', 'документ'])){
                    $requisite = $this->saveRequisiteWithFile($link->getAttribute('href'),$court);
                } else if (!$requisite && $textNode && $img = $textNode->find('img', 0)) {
                    $src = $img->getAttribute('src');
                    if (preg_match('/data:image\/(png|img|gif|jpg|jpeg);base64,(.*)/', $src,  $m)
                        && Storage::disk('public')->put($path = (new CourtRequisite())->getStoreDirectory() .'/'.Str::random(50) .'.'. $m[1], base64_decode($m[2]))) {
                        $requisite = $this->saveRequisiteWithFile(Storage::url($path),$court);
                    }
                }
            };
            $checkOnDomPage();
            if ($requisite) goto save;
            $lastMenu = null;
            /** @var \simple_html_dom_node[] $linkContainers */
            $linkContainers = [];
            try {
                check_table:
                $table = function() {return $this->domHtml->find('#tblINFODocsList', 0);};
                if (!$table() & ($canCheckMenu = ($menu = $this->firstElementByIdPrefix('divINFOMenu', null, true))
                    && array_search($mid = $menu->getAttribute('id'), $menus) === false)){
                    $lastMenu = $menu;
                    $menu->setAttribute('id', 'checked');
                    $menus[] = $mid;
                    $this->changeUrl($court->url . ($link = $this->firstLinkNodeFromContainerNode($menu, 'a',
                            ['рекизиты|квитанци|пошлин', ['для уплаты', 'государственн', 'перечисления', 'банк']]))->getAttribute('href'));
                    goto check_table;
                }
                //название города из названия городского суда без окончания
                $keys = ['реквизит|квитанци|бланк', [mb_strtolower(mb_substr($court->name, 0, mb_strlen(explode(' ', $court->name)[0]) - 3)),
                    'для уплаты'], CourtRequisite::EXCEPT_MENU_KEYS];
                $link = $this->firstLinkNodeFromContainerNode($t = $table(), 'a', $keys);
                if ($t) $linkContainers[] = $t;
                if (!$link && $canCheckMenu) $link = $this->firstLinkNodeFromContainerNode($menu, 'a', $keys);
                $this->changeUrl($court->url . $link->getAttribute('href'));
            } catch (\Throwable $e) {
//                dump($e->getLine(). $e->getMessage());
            } finally {
                $checkOnDomPage();
            }
            $linkContainers = array_values(array_filter(
                array_merge($linkContainers, [$lastMenu ?? $menu, $table()])
            ));
            if (!$requisite && count($linkContainers)) {
                $excluded = [];
                $soft = false;
                check_all:
                for($i= 0; $i < count($linkContainers) && $i < ($maxLoop = 10) && !$requisite; $i++) {
                    $links = $linkContainers[$i]->find('a');
                    foreach ($links as $li=> $link) {
                        if (!$soft && mb_eregi('('.CourtRequisite::EXCEPT_MENU_KEYS .')', $link->text())) continue;
                        $this->changeUrl($court->url . $link->href);
                        $link->remove();
                        $checkOnDomPage();
                        if($requisite) break;
                        else if ($t = $table()) $linkContainers[]= $t;
                        unset($links[$li]);
                    }
                    if ($links) {
                        $excluded[] = $linkContainers[$i];
                    }
                }
                if (!$soft && count($excluded)) {
                    $soft = true;
                    goto check_all;
                }
            }
            save:
            if ($requisite) {
                $court->requisite()->save($requisite);
//                dump($requisite->toArray());
                $this->success('requisite', $court);
                return;
            }
            $this->error('requisite', $court);
        } catch (\Throwable $e) {
            $this->error('requisite', $court, $e);
        }

    }
    public function loadCourtStructure(Court $court): void
    {
        $structures = [];
        $link = null;
        $firstLink = null;
        $menus = ['divCIMenu0'];
        try {
            $parsingTry =0;
            search_link:
            if ($parsingTry) dump('Try ' . ($parsingTry+1));
            if (!isset(CourtStructure::STEP_MENU_KEYS[0][$parsingTry])) {
                // если мы не нашли данных, то переходим по первой подходящей ссылке из основоного меню и парсим динамичное меню из этой страницы
                //TODO: если нет такого меню парсить саму страницу
                if ($firstLink) {
                    $this->changeUrl($firstLink);
                    $this->parseLastDynamicMenuLinks($court, $menus, $structures, '('.self::SIMPLE_WORK_POSITION_PATTERN.')');
                    $firstLink = null;
                    if ($structures) goto save_structure;
                }
                $this->error('structure', $court, new \Exception('No link with structure data'));
                return;
            }
            $this->changeUrl($aboutUrl = $court->url . $this->aboutCourtPageUrl); //переходим на старницу о суде и ищем ссылку с нужным текстом, в зависимости о  попытки парсинга
            $link = $this->firstLinkNodeFromContainerNode($this->domHtml->root, 'div#divCIMenu0 a', CourtStructure::STEP_MENU_KEYS[0][$parsingTry]);
            if (!$link) {
                $link = $this->firstLinkNodeFromContainerNode($this->domHtml->root, 'div#divCIMenu0 a', CourtStructure::STEP_MENU_KEYS[1][$parsingTry]);
            }
            if (!$link) {$parsingTry++; goto search_link;} //если нет нужной ссылки - пробуем найти другой текст
            elseif (!$firstLink) $firstLink = $court->url . $link->getAttribute('href');
            $subMenuKeys = Arr::get(CourtStructure::STEP_MENU_KEYS[2], $parsingTry);
            try {
                check_text:
                $block = null;
                if ($link) $this->changeUrl($court->url . $link->getAttribute('href'));
                //пытаемся найти блок с динамичным меню, которого может не быть, прио отсутсвии подкатегорий
                $ciDocText = $this->firstElementByIdPrefix('divCIDocText');
                $ciMenu = $this->firstElementByIdPrefix('divCIMenu', null, true);
                if (!$ciMenu || array_search($mid = $ciMenu->getAttribute('id'), $menus) !== false) {
                    $block = $this->domHtml->find('#tblCIDocsList', 0)
                        ?? $ciDocText
                        ?? $ciMenu;
                    $link = $this->firstLinkNodeFromContainerNode($block, 'a', $subMenuKeys);
                    $this->changeUrl($court->url . $link->getAttribute('href'));
                } else {
                    $ciMenu->setAttribute('id', 'checked');
                    $menus[] = $mid;
                    $link = $this->firstLinkNodeFromContainerNode($ciMenu, 'a', ['суда', ['организационная структура', 'аппарат', 'состав'], CourtStructure::EXCEPT_MENU_KEYS])
                        ?? $this->firstLinkNodeFromContainerNode($ciMenu, 'a', $subMenuKeys);
                    goto  check_text;
                }
            }catch (\ErrorException $e){
                //если по ссылке открывается файл, то сохраняем информацию об этом файле
                if ($link && strpos($e->getMessage(), 'file_get_contents') !== false) {
                    $s = $this->saveStructureWithFile($link->getAttribute('href'), $court);
                    $court->structure()->save($s);
                    $this->success('structure', $court);
                    return;
                }
//                dump($e->getLine() . $e->getMessage());

            }catch (\Throwable $e) {
//                dump($e->getLine() . $e->getMessage());
            } finally {
                $text = '';
                $ciDocText = null;
                //ищем таблицу с данными, если нет таблицы, то сохраняем текст с этой страницы для парсинга
                $table = $this->findCiDocTable($court, $ciDocText, $text, $block, $menus);

            }
            if (!$table && !$text && $block && $block->getAttribute('id') === 'tblCIDocsList') {
                $this->parseLastDynamicMenuLinks($court, $menus, $structures, '('.self::SIMPLE_WORK_POSITION_PATTERN.')');
            }
            if ($table) {
                $this->parseStructureFromTable($court, $table, $structures);
                //если из таблицы ничего не распарсилось, парсим как текст
                if (count($structures) === 0) {
                    $text = $table->text();
                }
            }
            if (!$court->structure()->count() && $this->readStructuresByText($text, $structures)) {
                $court->structure()->saveMany($structures);
            }
            save_structure:
            //если не удлаось распарсить парсим в других разделах меню
            if (($c= count($structures)) < self::MIN_COURT_STRUCTURE_SIZE) {
                $court->structure()->delete();
                $parsingTry++;
                goto search_link;
            } else
            {
                $this->success('structure', $court);
            }

        } catch (\Throwable $e) {
            $this->error('structure', $court, $e);
        }
    }
    public function findCiDocTable(Court $court, ?\simple_html_dom_node &$ciDocText,
                                   ?string &$text, ?\simple_html_dom_node &$block, array &$menus)
    {
        $table = null;
        $emptyDataTextLength = 50;
        $findCIDocTable = function() use (&$ciDocText) {
            start_CIDOC:
            if ($node = $this->firstElementByIdPrefix('divCIDocText')){
                $ciDocText = $node;
                $t = $ciDocText->find('table table') ?: $ciDocText->find('table');
                /** @var \simple_html_dom_node $table */
                //  удаляем пустые таблицы и отдаем первую не пустую
                foreach ($t ?:[] as $table) {
                    if((mb_strlen($this->clear($table->text(), '|[^\wа-яА-Я\d]+'))<100)
                        || mb_ereg('(перерыв|рабочего дня|обед)', $table->innertext())){
                        $table->parent()->removeChild($table);
                        goto start_CIDOC;
                    }
                }
                return $t;
            }
            return null;
        };
        foreach(CourtStructure::STEP_MENU_KEYS[3] as $menuKeys) {
            $gotoStepKeysNum = 0;
            search_table :
            //ищем таблицу, и если  ее нет на странице, то переходим по следующим ссылкам из оснвоного или динамичного меню
            $table = $findCIDocTable();
            if (!$table && ($keys = Arr::get($menuKeys,$gotoStepKeysNum)) &&
                $link = $this->firstLinkNodeFromContainerNode($block ?: $this->domHtml->root, $block ? 'a' :'.box .menuArea a', $keys)) {
                $url = $court->url . $link->getAttribute('href');
                if ($this->checkHistory($url)) { // если мы переходим по ссылке из истории то парсим все подпункты меню (см ижемский суд)
                    $this->changeUrl(Arr::last($this->history));
                    $structures = isset($structures) ? $structures : [];
                    $this->parseLastDynamicMenuLinks($court, $menus, $structures);
                    if ($text) break;
                }
                $this->changeUrl($url);
                $gotoStepKeysNum++;
                goto search_table;
            } else if ($table) {
                $tableText = '';
                foreach ((array) $table as $t) if ($t) $tableText .= $t->text();
                if (mb_strlen($this->clear($tableText)) < $emptyDataTextLength){
                    foreach ((array) $table as $t) $t->parent()->removeChild($t);
                    $table = null;
                    $gotoStepKeysNum++;
                    goto search_table;
                }
                break;
            }
            //если мы не нашли таблицу то нам нужно запомнить данные со страницы, чтобы парсить из текста
            if ($ciDocText && (mb_strlen($this->clear($tmp = $ciDocText->text()))>$emptyDataTextLength)) {
                $text = $tmp;
                break;
            }
            $ciDocText = null;
        }
        return $table;
    }

    public function parseStructureFromTable(Court $court, &$table, &$structures) : void
    {
        $tables = (array)$table;
        $replace = function ($v) {
            return $this->firstTagValue(trim(preg_replace('/([\n"\r\']|(&nbsp;\s?)+)/s', ' ', $v)));
        };
        $columns = [];
        $defaultColumns = [
            'fio' => 0,
            'work_position' => 1,
            'phone' => 2,
            'email' => 3,
        ];
        $headers = [];
        $headRowIndex= 0;
        foreach ($tables as $table) {
            $department = null;
            // определяем порядок колонок
            if (!$columns) {
                $head = null;
                for($sch = 0; $sch < 1000 && ((!$head = $table->find('tr', $headRowIndex++)) || !$hText = trim($this->clear($head->text(), '|\s\s+'))); $sch++);
                if ($head) foreach ($head->find('td') as $i => $column) {
                    foreach (CourtStructure::HOT_KEYS as $field => $keywords) foreach ($keywords as $keyword) {
                        if (mb_strpos(mb_strtolower($column->text()), $keyword) !== false) {
                            $columns[$field] = $i;
                            $defaultColumns = null; //если есть хедер таблицы, удаляем порядок по умолчанию
                            break;
                        }
                        $headers[$i] = $this->clear($column->text(), '|\s\s+');
                    }
                    if (array_search($i, $columns) === false) $columns["column$i"] = $i;
                }
            }
            //если заголовков не было используем дефолтный порядок столбцов
            if ($defaultColumns) {
                $headRowIndex--;
                $columns = $defaultColumns;
            }
            if (!isset($columns['fio'])) continue;
            $departments = [];
            $structureData= [];
            $cellFioPhoneData = 0;
            $twoFio = 0;
            if (count($tables) == 1 && count($table->childNodes()) == 1 && $defaultColumns) {
                $headRowIndex = 0;
                $headers = [];
            }
            //заполняем данные в двумерный массив в соответствии  с таблицей
            foreach (array_slice($table->find('tr'), $headRowIndex) as $tri => $row) {
                foreach ($columns as $column) $structureData[$tri][$column] = $structureData[$tri][$column]??null;
                if (count($childs = $row->childNodes()) > 1) {
                    for ($ci = 0; $ci < count($structureData[$tri]); $ci++) if($structureData[$tri][$ci] == null) break;
                    foreach($childs as $tdi => $child) {

                        $span = $child->getAttribute('rowspan') ?: 1;
                        // если колонка растянута на несколько строк, то заполняем следующие строки
                        for($ri = 0; $ri < $span; $ri++) {
                            $text = trim($this->clear($child->text(), '|t\t+|\s\s+|&nbsp;'));
                            $cellFioPhoneData += (preg_match('/'.self::SIMPLE_PHONE_PATTERN.'/', $text)
                                && mb_ereg('.*'.self::FIO_PATTERN.'.*', $text)
                                && !preg_match('/'.self::FIO_EXCLUDE_PATTERN.'/', $text) ) ? 1 : 0;
                            if(mb_ereg(self::FIO_PATTERN, $text, $fiom) && mb_ereg(self::FIO_PATTERN, mb_substr($text, mb_strpos($text, $fiom[1]) + mb_strlen($fiom[1])))) $twoFio+=2;
                            $structureData[$tri + $ri][$ci + $tdi] = $text;
                        }
                        if ($department) $departments[$tri] =$department;
                    }
                } else {
                    //если в строке только одна колонка или 0 - очищаем данные
                    unset($structureData[$tri]);
                }
                //если одна колонка, то возможно это отдел сотрудника
                if (count($childs) == 1 && !isset($columns['department'])
                    && preg_match('/[А-Яа-я]+/', $t = $row->text()) && !mb_eregi(self::DEPARTMENT_EXCLUDE_PATTERN, $t)) {
                    $department = $replace($t);
                }
            }
            //сохраняем данные
            if ($cellFioPhoneData < self::MIN_COURT_STRUCTURE_SIZE && $headRowIndex>0
                && $twoFio < self::MIN_COURT_STRUCTURE_SIZE) {
                foreach ($structureData as $tri => $row) {
                    $struc = new CourtStructure();
                    $struc->fio = $replace($row[$columns['fio']]);
                    if (!$struc->fio || mb_strpos(('ФИО'), ($struc->fio)) !== false
                        || !$defaultColumns && mb_eregi(self::FIO_EXCLUDE_PATTERN, $struc->fio)) continue;
                    foreach (CourtStructure::HOT_KEYS as $field => $v)
                        $struc->$field = isset($columns[$field], $row[$columns[$field]]) ? $this->clear($row[$columns[$field]]) : null;
                    if (array_filter(array_values($struc->only($struc->getFillable())))) {
                        $struc->source_url = $this->getDomUrl();
                        if ($department && !$struc->department) $struc->department = Arr::get($departments, $tri);
                        $court->structure()->save($struc);
                    }
                    $structures[] = $struc;
                }
            } else {
                $tmp = [];
                foreach ($structureData as $tri => $row) {
                    foreach ($row as $i => $td) {
                        $work_position = Arr::get($headers, $i);
                        if (mb_ereg(self::FIO_PATTERN, $work_position)) $work_position = null;
                        if (count($row) == 1
                            || ($r =$table->find('tr', $tri+1)) && !preg_match('/\w+/', $r->text())) continue;
                        if ($td && $this->readStructuresByText($td, $tmp, $work_position)) {
                            $court->structure()->saveMany($tmp);
                            $structures = array_merge($structures, $tmp);
                            $tmp = [];
                        }

                    }
                }
            }
        }
    }

    public function parseLastDynamicMenuLinks(Court $court, array $menus, array &$structures, ?string $linkRegex = null) : void {
        foreach ($menus as $menu) {
            $e = $this->domHtml->find('#'. $menu,0);
            if ($e) $e->parentNode()->removeChild($e);
        }
        $box = $this->domHtml->find('.box', 0);
        if (!$box) return;
        $byRegex = false;
        $links = $box->find('a.mItem')?: $box->find('a');
        check_all_links:
        foreach ($links as $link) {
            if (!($t = $link->text()) || $linkRegex && !mb_eregi($linkRegex, $t)) continue;
            $byRegex = true;
            try {
                $parser = new SudrfParser($court->url . $link->getAttribute('href'));
                $body = $parser->domHtml->find('.box .printVersionBody.outputArea', 0);
                $block = $text = $ciDocText = null;
                if ($table = $parser->findCiDocTable($court, $ciDocText, $text, $block, $menus))
                    $this->parseStructureFromTable($court, $table, $structures);
                if (!$court->structure()->count())
                    $this->readStructuresByText($body->text(), $structures, $linkRegex ? $t : null);
            }catch (\Throwable $e) {
                dump($e->getLine() . $e->getMessage());
            }
        }
        if (!$byRegex) {
            $links = [];
            $linkRegex = null;
            foreach (CourtStructure::STEP_MENU_KEYS[0] as $keys) {
                if ($link = $this->firstLinkNodeFromContainerNode($box, 'a', $keys)) {
                    $links[] = clone $link;
                    $link->parent()->removeChild($link);
                }
            }
            $byRegex = true;
            goto check_all_links;
        }
    }
    /**
     * @return array|Court[]
     */
    public function loadCity(): array
    {
        $this->changeUrl(self::PARSER_ROOT_PAGE . $this->mainPageUrl
            . '?id=300&act=go_search&searchtype=fs&court_subj='
            . str_pad($this->regionCode, 2, '0', STR_PAD_LEFT) . '&court_type=0');
        $childs = $this->domHtml->find('.search-results')[0]->childNodes();
        $a = false;
        foreach ($childs as $child) {
            if (!preg_match('/(районный|городской)/ui',$child->innertext(), $m)) continue;
            $court = $this->makeCourtByIndexPageSimpleHtmlNode($child);
            $court->court_type = Court::CITY_TYPE;
            $court->save();
            if (($a |= $this->needDebugCourt($court)) || $a) {
                $this->loadCourtStructure($court);
                $this->loadCourtRequisites($court);
            }
            $this->city[] = $court;
        }
        return $this->city;
    }

    /**
     * @return array|Court[]
     */
    public function loadMagistrate(): array
    {
        if (!$this->magistrateParser) {
            $this->changeUrl(self::PARSER_ROOT_PAGE . $this->mainPageUrl
                . '?id=300&act=go_ms_search&searchtype=ms&var=true&ms_type=ms&court_subj='
                . str_pad($this->regionCode, 2, '0', STR_PAD_LEFT));
            $links = $this->domHtml->find('.msSearchResultTbl', 0)->find('div > a');
            foreach ($links as $i => $link) {
                $links[$i] = [
                    'name' => $link->text(),
                    'url' => $link->getAttribute('href'),
                ];
            }
        } else {
            $links = $this->magistrateParser->getMagistrateLinks();
        }
        $parsers = [];
        foreach ($links ?:[] as $link) {
            try {
                $this->magistrate[] = $court = new Court();
                $court->region_code = $this->getRegionCode();
                $court->court_type = Court::MAGISTRATE_TYPE;
                $court->name = $link['name'];
                if (!$court->name) continue;
                $href = $link['url'];
                if (($pos = strpos((substr($href, 0, 15)), 'http', 4)) !== false) { //search second protocol part "http"
                    $href = substr($href, $pos);
                }
                $court->url = $href;
                if (!$this->needDebugCourt($court) && !isset($force)) continue;
                $force = true;
                if ($parser = RegionCourtParser::getParserByCourtUrl($court)) {
                    $parsers[class_basename($parser)] = $parser;
                    $parser->loadCourtData($court);;
                    continue;
                }
                dump('Unknown magistrate page for ' . $court->url);
                $court->save();
            } catch (LargeHtmlException $e) {}
        }
        foreach ($parsers as $parser) $parser->afterParse();
        return $this->magistrate;
    }

    public function checkAndFillDeal(DebtorDeal $deal, Debtor $debtor): void
    {
        $clone = (new DebtorDeal())->forceFill(Arr::except($deal->getAttributes(), 'id'));
        try {
            $fios = $debtor->getFioSearchParams();
            $fios = [Arr::last($fios)]; //только по фамилии
            $deals = $deal->getDealNumberSearchParams();
            $results = [];
            foreach ($deals as $num) {
                foreach ($fios as $try => $fio) {
                    $searchParams = [
                        'delo_id=' . self::ADMINISTARTE_DEAL_CATEGORY_PARAM,
                        'name_op=' . self::SEARCH_NAME_OP_PARAM,
                        'srv_num=' . 1,
                        'case_type=0',
                        'new=0',
                        'delo_table=g1_case'
                    ];
                    if ($num) {
                        $searchParams[] = 'g1_case__CASE_NUMBERSS=' . $num;
                    } else {
                        $searchParams[] =  http_build_query(['G1_PARTS__NAMESS' => mb_convert_encoding($fio,'cp1251')]);
                    }
                    $this->changeUrl($deal->getParseDomain() . $this->dealUrl . '&' . implode('&', $searchParams), false);
                    if (count($results = $this->domHtml->find('.box #tablcont tr'))
                        && $results = array_slice($results, 1)) break;
                    else sleep(1);
                }
                if ($results) break;
            }
            if ($results) {
                $indexes = $this->getDealIndexes($this->domHtml->find('.box #tablcont tr', 0));
                $count = 0;
                foreach ($results as $i => $result) {
                    if ($count > 0) $deal = clone $clone;
                    if (($i = Arr::get($indexes, 'deal_number', false)) !== false) {
                        $deal->deal_number = trim($this->clear($result->childNodes($i)->text(), '|&nbsp;'))?: $deal->deal_number;
                        $deal->deal_link = $deal->getParseDomain() . '/'
                            . ($result->childNodes($i))->find('a', $i)->getAttribute('href');
                    }
                    if (($i = Arr::get($indexes, 'deal_verdict', false)) !== false)
                        $deal->deal_verdict = trim($this->clear($result->childNodes($i)->text(), '|&nbsp;')) . ';';
                    $m = [];
                    $deal->deal_end_date =
                        (($i = Arr::get($indexes, 'deal_end_date', false)) !== false)
                        && ($date = $result->childNodes($i)->text())
                        && (preg_match('/(\d\d?\.\d\d?\.\d+)/', $date, $m))
                        && ($date = Arr::get($m, 1))
                            ? Carbon::parse($date)->setHour(12) : null;
                    $m = [];
                    $deal->deal_start_date =
                        (($i = Arr::get($indexes, 'deal_start_date', false)) !== false)
                        && ($date = $result->childNodes($i)->text())
                        && preg_match('/(\d\d?\.\d\d?\.\d+)/', $date, $m)
                        && ($date = Arr::get($m, 1))
                            ? Carbon::parse($date)->setHour(12) : null;
                    if ($deal->deal_link) try {
                        $dom = str_get_html(mb_convert_encoding(file_get_contents($deal->deal_link), 'utf-8', 'windows-1251'));
                        $li = $this->firstLinkNodeFromContainerNode($dom->find('ul#tabs', 0), 'li',
                            ['стороны', ['по делу']]);
                        $id = str_replace('tab', '', $li->getAttribute('id'));
                        $table = $dom->find('#contentt #cont'.$id . ' table tr');
                        foreach ($table as $row) {
                            if (mb_eregi('истец', $row->outertext())) {
                                $deal->deal_initiator .= $row->childNodes()[1]->text() .',';
                            }
                        }
                        $li = $this->firstLinkNodeFromContainerNode($dom->find('ul#tabs', 0), 'li',
                            ['исполнительные', ['документы', "листы"]]);
                        if ($li) {
                            $id = str_replace('tab', '', $li->getAttribute('id'));
                            $table = $dom->find('#contentt #cont' . $id . ' table tr');
                            foreach ($table as $row) {
                                if (!$vNode = ($row->childNodes(1))) continue;
                                if (mb_eregi('((ФС|ВС)\s*№?\s*\d{3,})', $vNode->outertext())
                                    && (mb_strpos($deal->fs_number, $fs = $vNode->text()) === false)) {
                                    $deal->fs_number .= $fs.';';
                                }else if (mb_eregi('(№|\d{3,})', $vNode->outertext())
                                    && (mb_strpos($deal->deal_verdict, $verd = $vNode->text()) === false)) {
                                    $deal->deal_verdict .= $verd.';';
                                }
                            }
                        } else if ($li = $this->firstLinkNodeFromContainerNode(
                            $dom->find('ul#tabs', 0),
                            'li',
                            ['дело'])
                        ){
                            $id = str_replace('tab', '', $li->getAttribute('id'));
                            $table = $dom->find('#contentt #cont' . $id . ' table tr');
                            foreach ($table as $row) {
                                if (mb_eregi('результат рассмотрения', $row->outertext())) {
                                    $deal->deal_verdict .= $row->childNodes()[1]->text() .';';
                                }
                            }
                        }
                    } catch (\Throwable $e) {
//                        throw $e;
                        DebtorException::report($debtor, $e);
                        $debtor->setError(Debtor::ERROR_NO_DEAL_DETAILS_LOAD);
                    }
                    if ($deal->checkSameInitiator()) {
                        $deal->save();
                        $count++;
                    }
                    sleep(1);
                }
            } else {
                $debtor->setError(Debtor::ERROR_NO_DEAL_DATA);
                $debtor->save();
            }
            return;
        } catch (\Throwable $e) {
            DebtorException::report($debtor, $e);
//            throw $e;
        }
        $debtor->setError(Debtor::ERROR_PARSE_FAIL);
        $debtor->save();
    }


}
<?php


namespace App\Console\Commands\Parsers\RegionParsers;

use App\Console\Commands\AbstractParser;
use App\Models\Catalog\Court;

/**
 * Class RegionCourtParser
 * @package App\Console\Commands\Parsers\RegionParsers
 */
abstract class RegionCourtParser extends AbstractParser
{
    public const PARSERS = [
        'cap.ru' => CapruParser::class,
        'mirsud24.ru' => KRKParser::class,
        'msudrf.ru' => MsudrfParser::class,
        'mirsud.yanao.ru' => YanaoMirsudParser::class,
        'mirsud86.ru' => HmaoMirsudParser::class,
        'mirsud.spb.ru' => MirsudSPBParser::class,
    ];
    protected $magistrateAreaUrl = '';

    /**
     * @param Court|string $court
     * @return RegionCourtParser|null
     */
    public static function getParserByCourtUrl($courtUrl): ?self
    {
        if ($courtUrl instanceof Court) $courtUrl = $courtUrl->url;
        foreach (self::PARSERS as $part => $claz) {
            if (strpos($courtUrl, $part) !== false) return new $claz($courtUrl);
        }
        return null;
    }

    public function getBaseUrl() : string
    {
        $domain = array_search(static::class, self::PARSERS);
        return mb_substr($this->url, 0, strpos($this->url, $domain)+strlen($domain));
    }
    public abstract function loadCourtRequisites(Court $court) : void;
    public abstract function loadCourtStructure(Court $court) : void;
    public abstract function loadCourtArea(Court $court) : void;

    /**
     * @param Court $court
     * @return void
     */
    public function loadCourtData(Court $court):void
    {
        $this->loadCourtRequisites($court);
        $this->loadCourtStructure($court);
        $this->loadCourtArea($court);
    }
     /**
      * @return array|Court[]
      */
     public function loadSupreme(): array {return [];}

     /**
      * @return array|Court[]
      */
     public function loadCity(): array {return [];}

     /**
      * @return array|Court[]
      */
     public function loadMagistrate(): array {return [];}

     public function getMagistrateLinks() : array {return [];}

     public function afterParse() : void {}
 }
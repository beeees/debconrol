<?php


namespace App\Console\Commands\Parsers\RegionParsers;


use App\Models\Catalog\Court;
use App\Models\Catalog\CourtArea;
use App\Models\Catalog\CourtRequisite;
use App\Models\Catalog\CourtStructure;
use App\Models\Catalog\Region;
use GuzzleHttp\RequestOptions;

/**
 * Class YanaoMirsudParser
 * @package App\Console\Commands\Parsers\RegionParsers
 */
class YanaoMirsudParser extends RegionCourtParser
{
    protected $mainPageUrl = '/ms';
    protected $areaUrl = '/js/showNextMU.php';
    protected $regionCode = 89;
    public function loadCourtData(Court $court): void
    {
        $this->changeUrl($court->url);
        $court->save();
        try{
            $table = $this->domHtml->find('.main-sidebar table table',0)
                ?:$this->domHtml->find('.main-sidebar table',0);
            foreach ($table->find('tr') as $row) {
                if (mb_stripos($row->text(), 'адрес') !== false&& !$court->address) {
                    $court->address = $row->childNodes()[1]->text();
                } elseif(mb_stripos($row->text(), 'телефон') !== false) {
                    $court->phone =($court->phone ?? ' ') . $row->childNodes()[1]->text();
                } elseif (mb_stripos($row->text(), 'e-mail') !== false) {
                    $court->email = $row->childNodes()[1]->text();
                } elseif (mb_stripos($row->text(), 'ФИО Мирового судьи') !== false) {
                    $court->structure()->save(new CourtStructure([
                        'fio' => $row->childNodes()[1]->text(),
                        'work_position' => $row->childNodes(0)->text(),
                    ]));
                } elseif (mb_stripos($row->text(), 'вышестоящий суд') !== false
                    && ($name = $row->childNodes()[1]->text())
                    && $city = Court::query()->byName($name)->first()) {
                    $city->magistrates()->save($court);
                }
            }
            $court->save();
            $this->success('structure', $court);
        } catch (\Throwable $e) {
            $this->error('structure', $court, $e);
        }
        $this->loadCourtRequisites($court);
    }

    public function loadCourtRequisites(Court $court): void
    {
        $menu = $this->domHtml->find('.main-sidebar .menu', 0);
        try {
            $link = null;
            foreach(CourtRequisite::STEP_MENU_KEYS as $keys)  {
                if ($link) break;
                $link = $this->firstLinkNodeFromContainerNode($menu, 'a', $keys);
            }
            $this->changeUrl($this->getBaseUrl() . $link->getAttribute('href'));
            $court->requisite()->save($this->getRequisiteByText($this->domHtml->find('.main-sidebar table',0)->text()));
            $this->success('requisite', $court);
        } catch (\Throwable $e) {
            $this->error('requisite', $court, $e);
        }
    }

    public function loadCourtStructure(Court $court): void
    {
    }

    public function loadCourtArea(Court $court): void
    {
    }
    public function getMagistrateLinks(): array
    {

        $this->changeUrl($this->url . $this->mainPageUrl);
        $links = $this->domHtml->find('select[name=subcat] option');
            foreach ($links as $i => $link) {
                $links[$i] = [
                    'name' => $link->text(),
                    'url' => $this->url . $link->getAttribute('value'),
                ];
            }
        return $links;
    }
    public function afterParse(): void
    {
        $convert = function($text) {
            return mb_convert_encoding($text, "UTF-8", 'windows-1251');
        };
        $html = $convert(file_get_contents($this->getBaseUrl() . $this->areaUrl));
        $addresses = json_decode($html, true);
        $count = count($addresses);
        $i = 0;
        foreach ($addresses as $address) {
            echo ++$i . "/" . $count;
            $client = new \GuzzleHttp\Client();
            $r = $client->post($this->getBaseUrl() . $this->areaUrl, [
                    RequestOptions::FORM_PARAMS => [
                        'mode' => 'house',
                        'val' => $address,
                    ],
                    RequestOptions::HEADERS => [
                        'X-Requested-With' => 'XMLHttpRequest',
                    ]
                ]
            );
            $dom = str_get_html($convert($r->getBody()->getContents()));
            $regionQuery = Region::query()->find($this->regionCode)->courts()->magistrate();
            foreach ($dom->find('option') as $item) {
                if (mb_stripos($item->text(), 'все') !== false) {
                    $house = $item->getAttribute('value');
                    $r = $client->post($this->getBaseUrl() . $this->areaUrl, [
                            RequestOptions::FORM_PARAMS => [
                                'mode' => 'mu',
                                'val' => $house,
                            ],
                            RequestOptions::HEADERS => [
                                'X-Requested-With' => 'XMLHttpRequest',
                            ]
                        ]
                    );
                    $id = explode(';', $convert($r->getBody()->getContents()))[0];
                    $id = (int)explode('MS', $id)[1];
                    if ($c = $regionQuery->where('url', 'like', ('%/ms/' .$id))->first()) {
                        $c->area()->save(new CourtArea([
                            'locality' => '',
                            'address' => $address
                        ]));
                        $this->success('area', $c);
                    }
                }
            }
        }
    }
}
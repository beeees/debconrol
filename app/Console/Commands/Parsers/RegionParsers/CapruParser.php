<?php

namespace App\Console\Commands\Parsers\RegionParsers;
use App\Models\Catalog\Court;
use App\Models\Catalog\CourtRequisite;
use App\Models\Catalog\CourtStructure;


/**
 * Class CapruParser
 * @package App\Console\Commands\Parsers\RegionParsers
 */
class CapruParser extends RegionCourtParser
{
    protected $regionCode = 21;
    public function __construct(?string $siteUrl = null)
    {
        parent::__construct($siteUrl);
        $this->domHtml = str_get_html($this->getHtml());
    }

    public function loadCourtRequisites(Court $court): void
    {
        $link = $this->firstLinkNodeFromContainerNode($this->domHtml->root, '.SiteMapBlock a', ['госпошлина']);
        $this->changeUrl(str_replace('&amp;', '&', $link->getAttribute('href')));
        $link = $this->firstLinkNodeFromContainerNode($this->domHtml->root, '#ContentBody_PageChilds a', ['госпошлин', 'блок']);
        $file = $this->getDomainByUri($this->url) . preg_replace('/(^\.\.+\/|^\/)/','/',$link->getAttribute('href'));
        $requisite = new CourtRequisite(compact('file'));
        $requisite->source_url = $this->getDomUrl();
        $court->requisite()->save($requisite);
    }

    public function loadCourtStructure(Court $court): void
    {
        $link = $this->firstLinkNodeFromContainerNode($this->domHtml->root, '#MainMenu a', ['структура']);
        $this->changeUrl($this->getDomainByUri($this->url).$link->getAttribute('href'));
        $childs = $this->domHtml->find('.CntentBlock .PersonLI', 0)->parent()->childNodes() ?:[];
        $structures = [];
        $department = '';
        foreach ($childs as $child) {
            if (strpos($child->getAttribute('class')?:'', 'StructureName') !== false) {
                $department = $this->clear($child->text());
                continue;
            } else if (strpos($child->getAttribute('class')?:'', 'PersonLI') !== false) {
                $work_position = $this->clear(($c = $child->find('.PersonPost', 0)) ? $c->text() : '');
                $fio = $this->clear(($c = $child->find('.PersonName', 0)) ? $c->text() : '');
                $phone =$this->clear(($c = $child->find('.PersonPhone', 0)) ? $c->text() : '');
                $email =$this->clear(($c = $child->find('.PersonMail', 0))?$c->text() : '');
                if (!$fio) continue;
                $data = compact('department', 'fio', 'phone', 'email', 'work_position');
                $structures[] = (new CourtStructure($data));
            }
        }
        $court->structure()->saveMany($structures);
    }

    public function loadCourtArea(Court $court): void
    {
        // TODO: Implement loadCourtArea() method.
    }
}
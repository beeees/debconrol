<?php

namespace App\Console\Commands\Parsers\RegionParsers;


use App\Console\Commands\Parsers\SudrfParser;
use App\Models\Catalog\Court;
use App\Models\Catalog\CourtArea;
use App\Models\Catalog\CourtRequisite;
use App\Models\Catalog\CourtStructure;
use App\Models\Debtor;
use App\Models\DebtorDeal;
use App\Models\DebtorException;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class MsudrfParser extends RegionCourtParser
{
    protected $magistrateAreaUrl= '/modules.php?name=terr';
    protected $dealUrl = '/modules.php?name=sud_delo';
    public const ADMINISTARTE_DEAL_CATEGORY_PARAM = SudrfParser::ADMINISTARTE_DEAL_CATEGORY_PARAM;
    public const SEARCH_OP_PARAM = 'sf';
    public function loadCourtData(Court $court): void
    {
        if ($this->domUrl != $court->url) $this->changeUrl($court->url);
        $court->address = ($e = $this->domHtml->find('.content p#court_address', 0)) ?$e->text() : null;
        $court->email = ($e = $this->domHtml->find('.content p#court_email',0)) ? $e->text() : null;
        $court->name = $this->domHtml->find('.site #court_name', 0)->text();
        foreach (($p = $this->domHtml->find('.content .person-phone .right') ?: [])  as $i => $e) {
            if (preg_match('/'. self::SIMPLE_PHONE_PATTERN .'/', $text = $this->clear($e->text(), '|\t')))
                $p[$i]= trim($text);
            else unset($p[$i]);
        }
        if ($p) $court->phone = implode(',', $p);
        $court->save();
        // парсим ссылку на районный суд, ищем ссылку по домену, а потом проверяем на наличие в бд такой же для городских судов
        $cityCourt = $this->domHtml->find('.content .last a') ?: [];
        foreach ($cityCourt as $linkCity) {
            if ($pos = strpos($href = $linkCity->getAttribute('href'), 'sudrf.ru')) {
                $cityCourt = Court::byUrl($href)->first();
                break;
            }
        }
        if ($cityCourt instanceof Court) $cityCourt->magistrates()->save($court);
        parent::loadCourtData($court);
    }
    public function loadCourtRequisites(Court $court): void
    {
        $cols = $this->domHtml->find('.content>div')?: [];
        $minFieldPosDiff = 100;
        $requisite = null;
        foreach ($cols as $col) {
            $text = $col->text();
            if (($posinn = mb_strpos(mb_strtolower($text), 'инн'))
                && ($poskpp = mb_strpos(mb_strtolower($text), 'кпп'))
                && abs($posinn-$poskpp) < $minFieldPosDiff) {
                $parsed = $this->getRequisiteByText($text);
                if ($parsed && array_filter($parsed->toArray())) {
                    $requisite = $parsed;
                    break;
                }
            }
        }
        //если не найдем ключевых слов переходим к поиску ссылки для рекизитов
        if (!$requisite && $block= $this->findMsudrfBlockByFullTitles(['вы можете', 'информация', 'справочная информация'])){
            $link = $this->firstLinkNodeFromContainerNode($block, 'a', ['пошлин', ['мировым', 'государств']]);
            if ($link) $requisite = static::parseRequisiteLink($link, $court);
        }
        if ($requisite) {
            $court->requisite()->save($requisite);
            $this->success('requisite', $court);
        }
        else $this->error('requisite', $court);
    }

    private function parseRequisiteLink(\simple_html_dom_node $link, Court $court) : ?CourtRequisite
    {
        $href = $link->getAttribute('href');
        next_link:
        if (preg_match('/((https?:\/\/)?[^\/]+)?(\/?.*)/', $href, $m)) {
            $href = str_replace(':/', '://', str_replace('//', '/', ($m[1] ?: $court->url) . $m[3]));
        }
//        dump($href);
        /** @var \simple_html_dom_node $dom */
        $dom = str_get_html(file_get_contents($href));
        $normalizeLink = function($l) use (&$href, &$link) {
            if ((strpos($l->getAttribute('href'), '/') === 0) && preg_match('/(https?:\/\/[^\/]+)\/?.*/', $href, $m)) {
                $href = $m[1] . $l->getAttribute('href');
            } else {
                $href= $l->getAttribute('href');
            }
            $link = $l;
        };
        if (strpos($href, 'minjust')) { //minjust.rkomi.ru
            $l = $this->firstLinkNodeFromContainerNode($dom->root, '.layout__main .container a', ['реквизиты', ['уплаты']]);
            if (!$l) {
                $this->error('requisite', $court, new \Exception('not parsable minjust link '.$href.')'));
                return null;
            }
            $normalizeLink($l);
            if (preg_match('/\.(docx?|xlsx?|pdf)$/', $href)) return new CourtRequisite(['file' => $href]);
            else goto next_link;
        } else if (strpos($href, 'msudrf')) {
            if ($docText= $this->firstElementByIdPrefix('divINFODocText', $dom->root)) {
                return $this->getRequisiteByText($docText->text());
            }
            $l = $this->firstLinkNodeFromContainerNode($dom->root, '.content .links-list a', ['реквизит', ['уплат', 'оплат']]);
            if (!$l) {
                $this->error('requisite', $court, new \Exception('not parsable msudrf link ('.$href.')'));
                return null;
            }
            $normalizeLink($l);
            goto next_link;
        }
        dump('Unknown court requisites link . ' . $court->url . '('.$href.')');
        return null;
    }


    public function loadCourtStructure(Court $court): void
    {
        if ((!$block = $this->findMsudrfBlockByFullTitles(['аппарат мирового судьи']))
            || !$block = $block->find('.info-block', 0)) {
            $this->error('structure', $court);
            return;
        }
        $ss = [];
        for ($i = 0; $i < count($childs = $block->childNodes()); $i++){
            if (($c =$childs[$i])->tag === 'h2' && (!$c->getAttribute('class'))) {
                $work_position = $c->text();
                $i++;
                if (($c = $childs[$i]) && !$c->getAttribute('class') && $c->tag === 'p'
                    && mb_ereg(self::FIO_PATTERN, $fio = $c->text())){
                    $s = new CourtStructure(compact('fio', 'work_position'));
                    $s->source_url = $this->getDomUrl();
                    $ss[] = $court->structure()->save($s);
                }

            }
        }
        if (!count($ss)) $this->error('structure', $court, new \Exception('No find magistrate structure in block'));
        else $this->success('structure', $court);
    }

    private function findMsudrfBlockByFullTitles(array $titles):?\simple_html_dom_node
    {
        $cols = $this->domHtml->find('.content>div')?: [];
        foreach ($titles as $title) {
            foreach ($cols as $col) {
                $heads = $col->find('.block-header') ?: [];
                foreach ($heads as $head) {
                    if ($find = (mb_strpos($text = mb_strtolower($head->text()), $title) === 0))
                        return $head->parent();
                }
            }
        }
        return null;
    }


    public function loadCourtArea(Court $court): void
    {
        try {
            $this->changeUrl($court->url . $this->magistrateAreaUrl);
            $areas = $this->domHtml->find('.content .terr-item') ?: [];
            $aa = [];
            $a = null;
            foreach ($areas as $area) {
                $a = new CourtArea();
                /** @var \simple_html_dom_node $area */
                if (strpos($area->getAttribute('class'), 'header')) continue;
                $left = $area->childNodes(0);
                $rigth = $area->childNodes(1);
                $note = $rigth->childNodes(0);
                $d = '';
                if ($note) {
                    $d = $this->clear($note->text(), '|\s\s+');
                    $rigth->removeChild($note);
                }
                $a->locality = $this->clear($left->text(), '|\s\s+');
                $ad = $this->clear($rigth->text(), '|\s\s+');
                $reverse = $d && mb_strlen($d) < mb_strlen($ad);
                $a->address = $reverse ? $d : $ad;
                $a->details = $reverse ? $ad : $d;
                $aa[] = $a;
            }
            $court->area()->saveMany($aa);
            if (!$this->domHtml->find('.content .search-error')) {
                if (!$areas || !count($aa)) throw new \Exception('No area information');
                else {
                    $this->success('area', $court);
                    return;
                }

            } else{
                dump("Empty area for ". $this->domUrl);
            }
        } catch (\Throwable $e) {
            $this->error('area',$court, $e);
        }
    }

    /**
     * @param DebtorDeal $deal
     * @param Debtor $debtor
     */
    public function checkAndFillDeal(DebtorDeal $deal, Debtor $debtor): void
    {
        $clone = (new DebtorDeal())->forceFill(Arr::except($deal->getAttributes(), 'id'));
        try {
            $fios = $debtor->getFioSearchParams();
            $deals = $deal->getDealNumberSearchParams();
            $results = [];
            foreach ($deals as $num) {
                foreach ($fios as $try => $fio) {
                    $searchParams = [
                        'G1_PARTS__PARTS_TYPE='.'Должник',
                        'G1_PARTS__NAMESS='.$fio,
                        'delo_id='.self::ADMINISTARTE_DEAL_CATEGORY_PARAM,
                        'op='.self::SEARCH_OP_PARAM,
                    ];
                    if ($num) $searchParams[] = 'g1_case__CASE_NUMBERSS='.$num;
                    $this->changeUrl($deal->getParseDomain() . $this->dealUrl . '&' . implode('&',$searchParams), false);
                    if (count($results = $this->domHtml->find('#search_results tr'))
                        && $results = array_slice($results, 1)) break;
                    else sleep(1);
                }
                if ($results) break;
            }
            if ($results) {
                $indexes = $this->getDealIndexes($this->domHtml->find('#search_results tr', 0));
                $count = 0;
                foreach ($results as $i => $result) {
                    if ($count > 0) $deal = clone $clone;
                    if (($i = Arr::get($indexes, 'deal_number', false)) !== false) {
                        $deal->deal_number = trim($this->clear($result->childNodes($i)->text(), '|&nbsp;'))?: $deal->deal_number;
                        $deal->deal_link = $deal->getParseDomain() . '/'
                            . ($result->childNodes($i))->find('a', 0)->getAttribute('href');
                    }
                    if (($i = Arr::get($indexes, 'deal_verdict', false)) !== false)
                        $deal->deal_verdict = trim($this->clear($result->childNodes($i)->text(), '|&nbsp;'));
                    $m = [];
                    $deal->deal_end_date =
                        (($i = Arr::get($indexes, 'deal_end_date', false)) !== false)
                        && ($date = $result->childNodes($i)->text())
                        && (preg_match('/(\d\d?\.\d\d?\.\d+)/', $date, $m))
                        && ($date = Arr::get($m, 1))
                            ? Carbon::parse($date)->setHour(12) : null;
                    $m = [];
                    $deal->deal_start_date =
                        (($i = Arr::get($indexes, 'deal_start_date', false)) !== false)
                        && ($date = $result->childNodes($i)->text())
                        && preg_match('/(\d\d?\.\d\d?\.\d+)/', $date, $m)
                        && ($date = Arr::get($m, 1))
                            ? Carbon::parse($date)->setHour(12) : null;
                    $m = [];
                    if ((($i = Arr::get($indexes, 'deal_initiator', false)) !== false)
                        && mb_eregi('истец:?([^<>]+)\s*(<[^>]+>)+?\s*ответчик', $result->childNodes($i)->outertext(), $m)) {
                        $deal->deal_initiator = trim($m[1]);
                    }
                    if ($deal->checkSameInitiator()) {
                        $deal->save();
                        $count++;
                    }
                    sleep(1);
                }
            } else {
                $debtor->setError(Debtor::ERROR_NO_DEAL_DATA);
                $debtor->save();
            }
            return;
        } catch (\Throwable $e) {
            DebtorException::report($debtor, $e);
//            throw $e;
        }
        $debtor->setError(Debtor::ERROR_PARSE_FAIL);
        $debtor->save();
    }
}
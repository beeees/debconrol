<?php

namespace App\Console\Commands\Parsers\RegionParsers;


use App\Models\Catalog\Court;
use App\Models\Catalog\CourtArea;
use App\Models\Catalog\CourtStructure;
use App\Models\Company\CompanyCase;
use App\Models\Debtor;
use App\Models\DebtorDeal;
use App\Models\DebtorException;
use Carbon\Carbon;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Arr;

class KRKParser extends RegionCourtParser
{
    public $mainPageUrl = '/sudebnye-uchastki/spisok-sudebnykh-uchastkov/';
    public $infoUrl = '/ms/o-sude/index.php';
    public $structureUrl = '/ms/o-sude/getinfo.php';
    public $requisiteUrl = '/ms/regulatory/kalkulyator-gosposhliny/';
    public $areaUrl = '/ms/o-sude/territorialnaya-yurisdiktsiya/';
    protected $dealUrl = '/start/informatsiya-po-sudebnym-delam/';
    protected $regionCode = 24;
    public static function clear(string $string, string $addRegexp = ''): string
    {
        $addRegexp = '|\t\t*|\s\s+';
        $result = parent::clear($string, $addRegexp);
        return mb_eregi_replace('&?nbsp;|</?(br?|strong)>' . $addRegexp, '', $result);
    }

    public function loadCourtData(Court $court): void
    {
        $this->changeUrl($court->url);
        $court->url = preg_replace('/(https?:\/\/)?([^\/:])(:\d+)?(\/.+)?/', '$1$2', $this->domUrl);
        $headerChilds = $this->domHtml->find('header .container.container-white .col');
        foreach ($headerChilds as $child) {
            if (mb_stripos($child->text(), 'адрес ') !== false) {
                foreach($child->childNodes() as $c) $c->remove();
                $court->address = $this->clear($child->text());
            } elseif (mb_ereg(self::SIMPLE_PHONE_PATTERN, $child->text())) {
                foreach($child->childNodes() as $c) $c->remove();
                $court->phone = $this->clear($child->text());
            }
            $child->remove();
        }
        $court->save();
        parent::loadCourtData($court);
    }

    public function getMagistrateLinks(): array
    {

        $this->changeUrl($this->url . $this->mainPageUrl);
        $links = $this->domHtml->find('.container.container-main .content table#info-table a');
        foreach ($links as $i => $link) {
            if (!mb_eregi('(упразднён)', $link->text()))
                $links[$i] = [
                    'name' => $link->text(),
                    'url' => $link->getAttribute('href'),
                ];
            else unset($links[$i]);
        }
        return $links;
    }

    public function loadCourtRequisites(Court $court): void
    {
        $this->changeUrl($court->url . $this->requisiteUrl);
        $block = $this->domHtml->find('.container.container-main .content .tbl-data tbody', 0);
        try {
            if ($r = $this->getRequisiteByText($block->text())) {
                $court->requisite()->save($r);
                $this->success('requisite', $court);
                return;
            }
        } catch (\Throwable $e) {
            $this->error('requisite', $court, $e);
        }
        $this->error('requisite', $court);
    }

    public function loadCourtStructure(Court $court): void
    {
        $this->changeUrl($court->url . $this->infoUrl);
        $block = $this->domHtml->find('.container.container-main .content .col table tbody');
        try {
            $sBlock = null;
            foreach ($block as $b) {
                if (mb_eregi('мировой судья', $b->text())) {
                    $sBlock = $b;
                    break;
                }
            }
            $fio = $this->clear($sBlock->find('tr', 1)->childNodes(1)->innertext());
            $work_position = $this->clear($sBlock->find('tr', 1)->childNodes(0)->innertext());
            $court->structure()->save(new CourtStructure(compact('fio', 'work_position')));
            if ((mb_stripos($block[count($block) - 1]->text(), 'инстанция')) !== false
                && $link = $block[count($block) - 1]->find('a', 0)) {
                $cc = Court::byUrl($link->getAttribute('href'))->first();
                if ($cc) $cc->magistrates()->save($court);
            };
            preg_match('/ms(\d+)\./', $court->url, $m);
            if ($courtMirsudId = Arr::get($m, 1)) {
                $client = new \GuzzleHttp\Client();
                $r = $client->post($court->url . $this->structureUrl, [
                        RequestOptions::FORM_PARAMS => [
                            'mss' => $courtMirsudId,
                        ],
                        RequestOptions::HEADERS => [
                            'X-Requested-With' => 'XMLHttpRequest',
                        ]
                    ]
                );
                $dom = str_get_html($r->getBody()->getContents());
                $rows = $dom->find('table tbody tr');
                foreach ($rows as $row) {
                    $c = count($row->find('td'));
                    $fio = $row->find('td', $c - 1);
                    $wp = $row->find('td', $c - 2);
                    if ($fio && $wp) {
                        $this->saveStructureByFioAndWP($court, $fio->text(), $wp->text());
                    } else {
                        dump('Strange behavior for KRK structure');
                    }
                }
            } else {
                dump('Bad court mss id from url ' . $court->url);
            }
            if (!$court->structure()->count()) {
                $this->error('structure', $court, new \Exception('No find magistrate structure in block'));
            }
            else {
                $this->success('structure', $court);
                return;
            }
        } catch (\Throwable $e) {
            $this->error('structure', $court, $e);
            return;
        }

    }

    public function saveStructureByFioAndWP(Court $court, string $fio, string $work_position): void
    {
        $court->structure()->save(new CourtStructure((compact('fio', 'work_position'))));
    }

    public function loadCourtArea(Court $court): void
    {
        $this->changeUrl($court->url . $this->areaUrl);
        $block = $this->domHtml->find('.container.container-main .content table tbody', 0);
        try {
            $text = $block->text();
            $text = preg_replace('/(:\r?\n|:)/', ': ', $text);
            if (preg_match_all(self::AREA_KEYS_PATTERN, $text, $m) || count($m[2])) {
//                dump($m);
                for ($i = 0; $i < count($m[2]); $i++) {
                    $key = $m[2][$i];
                    start:
                    $newLinePos = function() use ($text){
                        $m = [];
                        preg_match('/(^[^\n]+)\n/', $text, $m);
                        return mb_strlen(Arr::get($m, 1, ''));
                    };
                    $keyPos = mb_strpos($text, $key);
                    $min = min($newLinePos(), $keyPos);
                    if ($min >= 0 && $keyPos !== 0) {
//                        dump($key, $keyPos, $newLinePos());
                        if (($keyPos > 254) & mb_ereg('([^\.\s ]{4,}\.)', $text,$m2)){
                            $keyPos = mb_strpos($m2[1], $text) + mb_strlen($m2[1]);
                        }
                        $locality = mb_substr($text, 0, $keyPos);
                        if ($locality && mb_eregi('([а-я])', $locality)) $court->area()->save(new CourtArea(compact('locality')));
                        $text = mb_substr($text, mb_strlen($locality));
                        goto start;
                    } else if ($min == 0 && $keyPos == 0) {
                        $locality = '';
                        $next = Arr::get($m[2], $i + 1);
                        $details = mb_substr($text, $len = mb_strlen($key),
                            $next && ($pos = mb_strpos($text, $next)) ? $pos - $len : null);
                        if (preg_match('/^([^\d\w]+)[\d\w]/u', $details,$m3))
                            $details = str_replace($m3[1], '', $details);
                        $address = $key;
                        $court->area()->save(new CourtArea(compact('locality', 'address', 'details')));
                        $text = mb_substr($text, mb_strlen($details . $address));
                    }
                }
            } else {
                if (($locality = $block->text()) && mb_eregi('([а-я])', $locality))
                    $court->area()->save(new CourtArea(compact('locality')));
                else dump('Not parsable area text ' . $text);
            }
            if ($court->area()->count()) {
//                foreach ($court->area()->get() as $a) dump($a->toArray());
                $this->success('area', $court);
                return;
            }
        } catch (\Throwable $e) {
            $this->error('area', $court, $e);
            return;
        }
        $this->error('area', $court);

    }

    /**
     * @param DebtorDeal $deal
     * @param Debtor $debtor
     */
    public function checkAndFillDeal(DebtorDeal $deal, Debtor $debtor): void
    {
        $clone = (new DebtorDeal())->forceFill(Arr::except($deal->getAttributes(), 'id'));
        $company = CompanyCase::query()->whereHas('debtors', function($q)  use ($debtor){
            $q->whereKey($debtor->id);
        })->first()->company;
        try {
            $fios = $debtor->getFioSearchParams();
            $deals = $deal->getDealNumberSearchParams();
            $results = [];
            foreach ($deals as $num) {
                foreach ($fios as $try => $fio) {
                    $searchParams = [
                        'numberingyear' => $deal->deal_init_date->addMonth()->year, //Дела за декабрь нужно искать со значением следущего года
                        'sprs'=>$fio,
                        'sprsType' => 'np',
                        'spt' => 'CS',
                        'uid' => 'NO',
                    ];
                    if ($num) $searchParams['val'] = $num;
                    $client = new \GuzzleHttp\Client();
                    $r = $client->post(($deal->getParseDomain() . $this->dealUrl. 'search.php'), [
                            RequestOptions::FORM_PARAMS => ($searchParams),
                            RequestOptions::HEADERS => [
                                'X-Requested-With' => 'XMLHttpRequest',
                            ]
                        ]
                    );
                    $dom = str_get_html(($r->getBody()->getContents()));
                    if (count($results = $dom->find('table tr'))
                        && $results = array_slice($results, 1)) break;
                    else sleep(1);
                }
                if ($results) break;
            }

            if ($results) {
                $indexes = $this->getDealIndexes($dom->find('table tr', 0));
                $count = 0;
                foreach ($results as $i => $result) {
                    if ($count > 0) $deal = clone $clone;
                    if (($i = Arr::get($indexes, 'deal_number', false)) !== false) {
                        $e = $result->find('td>nobr', 0);
                        $deal->deal_number = trim($this->clear($e->text(), '|&nbsp;'))?: $deal->deal_number;
                        $href = $e->find('a', 0)->getAttribute('href') ?: '';
                        if (strlen($href) > 0) {
                            $deal->deal_link = $deal->getParseDomain() . $this->dealUrl . $href;
                        }
                    }
                    $m = [];
                    $deal->deal_start_date =
                        (($i = Arr::get($indexes, 'deal_start_date', false)) !== false)
                        && ($date = $result->childNodes($i)->text())
                        && (preg_match('/(\d\d?.\d\d?.\d+)/', $date, $m))
                        && ($date = Arr::get($m, 1))
                            ? Carbon::parse(str_replace('-', '.',$date))->setHour(12) : null;
                    if ((($i = Arr::get($indexes, 'deal_initiator', false)) !== false)){
                        $initiator = Arr::last($result->childNodes())->text();
                        if ($deal->deal_initiator
                            && stripos($initiator, $deal->loaded_initiator ?: $company->name) === false)
                            continue;
                    }
                    if ($deal->deal_link) try {
                        $dom = str_get_html(file_get_contents($deal->deal_link));
                        $info = $dom->find('#part1 table tr');
                        foreach ($info as $r1) {
                            $text = $r1->text();
                            $m  = [];
                            if (mb_eregi('текущее состояние.+?заверш.+?(\d\d\.\d\d.\d+)', $text, $m)){
                                $deal->deal_end_date = Carbon::parse($m[1])->setHour(12);
                                break;
                            }
                        }
                        if ($info = $dom->find('#part5 table tr')) {
                            $lastRow = $info[count($info)-1];
                            $deal->deal_verdict = $lastRow->lastChild()->text();
                        }
                        $info = $dom->find('#part2 table tr');
                        foreach ($info as $r1) {
                            $text = $r1->text();
                            $m  = [];
                            if (mb_eregi('(взыскатель|истец)(.*)', $text, $m)){
                                $deal->deal_initiator = str_replace('&quot;', '"', $this->clear($m[2]));
                                break;
                            }
                        }
                    } catch (\Throwable $e) {
                        DebtorException::report($debtor, $e);
//                        throw $e;
                        $debtor->setError(Debtor::ERROR_NO_DEAL_DETAILS_LOAD);
                    }

                    if ($deal->checkSameInitiator()) {
                        $deal->save();
                        $count++;
                    }
                    sleep(1);
                }
            } else {
                $debtor->setError(Debtor::ERROR_NO_DEAL_DATA);
                $debtor->save();
            }
            return;
        } catch (\Throwable $e) {
            DebtorException::report($debtor, $e);
//            throw $e;
        }
        $debtor->setError(Debtor::ERROR_PARSE_FAIL);
        $debtor->save();
    }

    protected function getDealIndexes(\simple_html_dom_node $tableHeadRow, array $result  = []) : array
    {
        $result = [
            'deal_number' => 'номер',
            'deal_start_date' => 'дата',
            'deal_initiator' => 'участники',
        ];
        return parent::getDealIndexes($tableHeadRow, $result);
    }

}

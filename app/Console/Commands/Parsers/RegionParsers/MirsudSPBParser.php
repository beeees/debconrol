<?php
/**
 * Created by PhpStorm.
 * User: jura120596
 * Date: 12/2/21
 * Time: 9:22 AM
 */

namespace App\Console\Commands\Parsers\RegionParsers;


use App\Models\Catalog\Court;
use App\Models\Catalog\CourtArea;
use App\Models\Catalog\CourtRequisite;

class MirsudSPBParser extends RegionCourtParser
{

    public $mainPageUrl = '';
    public $infoUrl = '/ms/o-sude/index.php';
    protected $dealUrl = '/start/informatsiya-po-sudebnym-delam/';
    protected $regionCode = 78;

    public static function clear(string $string, string $addRegexp = '|(<?\/?(\w)>)'): string
    {
        return parent::clear($string, $addRegexp);
    }

    public function loadCourtRequisites(Court $court): void
    {
        try {
            $req = new CourtRequisite();
            $childs = $this->domHtml->find('.bank-details', 0);
            $reqFull = $this->getRequisiteByText($this->clear($childs->innertext()));
            $req->fill($reqFull->only(['inn', 'oktmo']));
            $req->court()->associate($court);
            $req->save();
            $this->success('requisite', $court);
            return;
        } catch (\Throwable $e) {
            $this->error('requisite', $court, $e);
        }

    }

    public function loadCourtStructure(Court $court): void
    {
        try {
            $c= $this->domHtml->find('.about-sector .col-lg-6', 0);
            $s = [];
            $this->readStructuresByText($c->innertext(), $s);
            foreach ($s as $child) {
                $child->work_position = self::clear($child->work_position);
                $court->structure()->save($child);
            }
            $this->success('structure', $court);
        } catch (\Throwable $e) {
            $this->error('structure', $court, $e);
        }
    }

    public function loadCourtArea(Court $court): void
    {
        try {
            $c = $this->domHtml->find('.territorial table tr');
            $c = array_slice($c, 1);
            foreach ($c as $area) {
                $ca = new CourtArea();
                $ca->locality = $area->childNodes(0)->innertext();
                $ca->address = $area->childNodes(1)->innertext() . ';'. $area->childNodes(2)->innertext();
                $court->area()->save($ca);
            }
            $this->success('area', $court);
            return;
        } catch (\Throwable $e) {
            $this->error('area', $court, $e);
        }
    }

    public function loadCourtData(Court $court): void
    {
        $this->changeUrl($court->url);
        $court->name = $this->clear($this->domHtml->find('.bredcrumbs-and-title h1.title', 0)->innertext());
        $court->address = $this->clear($this->domHtml->find('.sector-info .adress-fact p',0)->innertext());
        $court->phone = $this->clear($this->domHtml->find('.sector-info .telfax p',0)->innertext());
        $court->email = $this->clear($this->domHtml->find('.sector-info .reseption .link__mail',0)->innertext());
        $court->save();
        parent::loadCourtData($court);
    }
}
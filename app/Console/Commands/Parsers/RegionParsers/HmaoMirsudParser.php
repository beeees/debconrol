<?php

namespace App\Console\Commands\Parsers\RegionParsers;


use App\Models\Catalog\Court;
use App\Models\Catalog\CourtArea;
use App\Models\Catalog\CourtStructure;
use App\Models\Company\Company;
use App\Models\Company\CompanyCase;
use App\Models\Debtor;
use App\Models\DebtorDeal;
use App\Models\DebtorException;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class HmaoMirsudParser extends RegionCourtParser
{
    protected $dealUrl = 'http://mirsud86.ru/activity/regions/';
    protected $regionCode = 86;
    public function loadCourtData(Court $court): void
    {
        $this->changeUrl($court->url);
        $table = $this->domHtml->find('.RightContentContainer table table',0) ?:
            $this->domHtml->find('.RightContentContainer table',0);
        $structures = [];
        $areas = [];
        $requisite = null;
        foreach ($table->find('tr') as $row) {
            try {
                $mode = '';
                $childs = $row->childNodes();
                if (mb_eregi(self::WORK_POSITION_PATTERN, $text = $childs[0]->text())) {
                    $mode = 'strucutre';
                    if (preg_match('/' . self::PHONE_PATTERN . '/', $childs[1]->text(), $m)) {
                        mb_ereg(self::FIO_PATTERN, $childs[1]->text(), $m2);
                        $structures[] = new CourtStructure([
                            'fio' => $m2[0] ?: $m[1],
                            'work_position' => $text,
                            'phone' => $m[3],
                        ]);
                    } else {
                        $structures[] = new CourtStructure([
                            'fio' => $childs[1]->text(),
                            'work_position' => $text,
                        ]);
                    }
                } else if (mb_stripos($text, 'адрес') !== false && !$court->address) {
                    $court->address = $row->childNodes()[1]->text();
                } elseif (mb_stripos($text, 'телефон') !== false) {
                    $court->phone = ($court->phone ?? ' ') . $row->childNodes()[1]->text();
                } elseif (mb_stripos($text, 'e-mail') !== false) {
                    $court->email = $row->childNodes()[1]->text();
                } elseif (mb_stripos($text, 'обжалование') !== false
                    && ($nameLink = $childs[1]->find('a', 0))
                    && $city = (Court::query()->byName($nameLink->text())->first()
                        ?? Court::query()->byUrl($nameLink->getAttribute('href'))->first())) {
                    $city->magistrates()->save($court);
                } elseif (mb_stripos($text, 'территориальная подсудность') !== false) {
                    $mode = 'area';
                    if (preg_match_all(self::AREA_KEYS_PATTERN, $text = $childs[1]->text(), $m) || count($m[2])) {
                        for ($i = 0; $i < count($m[2]); $i++) {
                            $locality = $m[2][$i];
                            $details = mb_substr($text, mb_strpos($text, $locality), isset($m[2][$i + 1]) ? mb_strpos($text, $m[2][$i + 1]) : null);
                            $details = trim(mb_substr($details, mb_strlen($locality) + 1));
                            $areas[] = new CourtArea(compact('locality', 'details'));
                        }
                    }
                } elseif (!$requisite && mb_stripos($text, 'государственная пошлина') !== false
                    && $link = $childs[1]->find('a', 0)) {
                    $court->save();
                    $requisite = $this->saveRequisiteWithFile($this->getBaseUrl() . $link->getAttribute('href'), $court);
                }
                $m = [];
            } catch (\Throwable $e) {
                if($mode) {
                    $this->error($mode, $court, $e);
                    continue;
                }
                dump("HMAO parse error \n" . $e->getLine() . $e->getMessage());
            }
        }
        $court->save();
        if ($structures) {
            $court->structure()->saveMany($structures);
            $this->success('structure', $court);
        }
        if ($requisite) {
            $this->success('requisite', $court);
        }
        if ($areas) {
            $court->area()->saveMany($areas);
            $this->success('area', $court);
        }
    }

    public function loadCourtRequisites(Court $court): void
    {

    }

    public function loadCourtStructure(Court $court): void
    {

    }

    public function loadCourtArea(Court $court): void
    {

    }

    /**
     * @param DebtorDeal $deal
     * @param Debtor $debtor
     */
    public function checkAndFillDeal(DebtorDeal $deal, Debtor $debtor): void
    {
        $clone = (new DebtorDeal())->forceFill(Arr::except($deal->getAttributes(), 'id'));
        $company = CompanyCase::query()->whereHas('debtors', function($q)  use ($debtor){
            $q->whereKey($debtor->id);
        })->first()->company;
        try {
            $fios = $debtor->getFioSearchParams();
            $deals = $deal->getDealNumberSearchParams();
            $results = [];
            foreach ($deals as $num) {
                foreach ($fios as $try => $fio) {
                    $searchParams = [
                        'year' => $deal->deal_init_date->addMonth()->year, //Дела за декабрь нужно искать со значением следущего года
                        'sf4'=>mb_convert_encoding($fio,'cp1251'),
//                        'sf3' =>mb_convert_encoding($deal->loaded_initiator ?: ($company ? $company->name : ''), 'cp1251'),
                    ];
                    if ($num) $searchParams['sf1'] = $num;
                    $this->changeUrl($this->dealUrl . '?' . http_build_query($searchParams), false);
                    if (count($results = $this->domHtml->find('.decision_table tr'))
                        && $results = array_slice($results, 1)) break;
                    else sleep(1);
                }
                if ($results) break;
            }
            if ($results) {
                $indexes = $this->getDealIndexes($this->domHtml->find('.decision_table tr', 0));
                foreach ($results as $i => $result) {
                    if ($i > 0) $deal = clone $clone;
                    if (($i = Arr::get($indexes, 'deal_number', false)) !== false) {
                        $deal->deal_number = trim($this->clear($result->childNodes($i)->text(), '|&nbsp;'))?: $deal->deal_number;
                        if (($i = Arr::get($indexes, 'deal_link', false)) !== false)
                            $href = ($result->childNodes($i))->find('a', 0)->getAttribute('href') ?: '';
                            if (strlen($href) > 0) {
                                $deal->deal_link = $deal->getParseDomain() . '/' . substr($href,1);
                            }
                    }
                    $m = [];
                    $deal->deal_start_date =
                        (($i = Arr::get($indexes, 'deal_start_date', false)) !== false)
                        && ($date = $result->childNodes($i)->text())
                        && (preg_match('/(\d\d?\-\d\d?\-\d+)/', $date, $m))
                        && ($date = Arr::get($m, 1))
                            ? Carbon::parse(str_replace('-', '.',$date))->setHour(12) : null;
                    if ((($i = Arr::get($indexes, 'deal_initiator', false)) !== false)){
                        $deal->deal_initiator = $result->childNodes($i)->text();
                    }
                    if ($deal->deal_link) try {
                        $dom = str_get_html(mb_convert_encoding(file_get_contents($deal->deal_link), 'utf-8', 'windows-1251'));
                        $info = $dom->find('body .MM > table > tr');
                        foreach ($info as $r1) {
                            $text = $r1->text();
                            $m = $m2 = [];
                            if (mb_eregi('Истец', $text, $m)){
                                $deal->deal_initiator = $r1->find('td', 1)->text();
                            } else if (mb_eregi('История состояний', $text, $m2)
                                && $table = $r1->childNodes()[1]->find('table tr')) {
                                $lastEvent = null;
                                foreach ($table as $row) try {
                                    $lastEvent = $row;
                                }catch(\Throwable $e) {}
                                if ($lastEvent && !$deal->deal_verdict) {
                                    $childs = $lastEvent->childNodes();
                                    $date = Arr::last($childs);
                                    $deal->deal_end_date = Carbon::parse($date->text())->setHour(12);
                                    $deal->deal_verdict = $childs[0]->text();
                                }
                            }
                        }
                    } catch (\Throwable $e) {
                        DebtorException::report($debtor, $e);
                        $debtor->setError(Debtor::ERROR_NO_DEAL_DETAILS_LOAD);
                    }

                    if ($deal->checkSameInitiator()) {
                        $deal->save();
                    }
                    sleep(1);
                }
            } else {
                $debtor->setError(Debtor::ERROR_NO_DEAL_DATA);
                $debtor->save();
            }
            return;
        } catch (\Throwable $e) {
            DebtorException::report($debtor, $e);
//            throw $e;
        }
        $debtor->setError(Debtor::ERROR_PARSE_FAIL);
        $debtor->save();
    }

}
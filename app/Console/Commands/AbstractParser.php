<?php

namespace App\Console\Commands;
use App\Console\Commands\Parsers\RegionParsers\RegionCourtParser;
use App\Exceptions\AppException;
use App\Exceptions\LargeHtmlException;
use App\Models\Catalog\Court;
use App\Models\Catalog\CourtRequisite;
use App\Models\Catalog\CourtStructure;
use App\Models\Debtor;
use App\Models\DebtorDeal;
use GuzzleHttp\Client;
use Illuminate\Support\Arr;

/**
 * Class AbstractParser
 * @package App\Console\Commands
 */
abstract class AbstractParser
{
    protected $url = '';
    protected $html = '';
    protected $supreme = [];
    protected $city = [];
    protected $magistrate = [];
    protected $regionCode = 0;
    protected $courtLinkPart = null;
    protected $hardMode = false;
    /** @var RegionCourtParser null  */
    protected $magistrateParser = null;
    protected $dealUrl = null;
    /** @var \simple_html_dom domHtml */
    public $domHtml;
    public $domUrl = '';
    public $history = [];
    public const MIN_COURT_STRUCTURE_SIZE = 3;
    public const PHONE_PATTERN = '([А-Я\.][^\d]{25,}[^\(0-9])('. self::SIMPLE_PHONE_PATTERN . '|([a-zA-Z\d\-_]+@[a-zA-Z\.]+))';
    public const SIMPLE_PHONE_PATTERN  = '((т.{0,4}\s+|факс\s+)?[\d\(\+][\d\-\(\)\s]{4,20}\d)';
    public const FIO_PATTERN = '([А-ЯЙЁ][А-ЯЙЁа-яёй&]+\s+[А-ЯЙЁ]([а-яёй&]+|\.\s?[А-Я]\.)(\s+[А-ЯЙЁ][а-яёй&]+))';
    public const ADMINISTARTE_WORDS_PATTERN = 'ука(з ?)|президент|росси|рф|республик|государственн|думой|автономн|верховн|советск|президиум|федераци|совета судей|постановление';
    public const FIO_EXCLUDE_PATTERN = '('.self::SIMPLE_WORK_POSITION_PATTERN . '|'.self::ADMINISTARTE_WORDS_PATTERN .'|ф\.\s?и\.\s?о)';
    public const DEPARTMENT_EXCLUDE_PATTERN =  '(факс|телефон|тел.|т.)';
    public const SIMPLE_WORK_POSITION_PATTERN = 'седатель|аместитель|разряд|екретарь|главный|омощник|ачальник|удь[ия] |удь[ия]\b|администратор|специалист';
    public const WORK_POSITION_PATTERN = '([^\d\s]+(ам |уд[^ао\s]|'.self::SIMPLE_WORK_POSITION_PATTERN.')[^\n\rА-Я,\.\-]+)';
    public const AREA_KEYS_PATTERN = '/(^|\n ?|\t +|\. ?)(([^:\s\n\r\.]+\s?){1,5})(:|\s[^\s \.\)А-ЯЁ]{1,6}\.)/u';
    public const TAG_DELIMETER = 'TAGENDPLACE';

    /**
     * AbstractParser constructor.
     * @param null|string $siteUrl
     */
    public function __construct(?string $siteUrl = null)
    {
        $this->url = $siteUrl;
        if (!function_exists('str_get_html')) {
            include __DIR__ . '/./simple_html_dom.php';
        }
        try {
            $this->html = file_get_contents(static::getUrl());
        } catch (\Throwable $e) {
            static::changeUrl($siteUrl);
        }
    }

    /**
     * @return string
     */
    public function getDomUrl(): string
    {
        return $this->domUrl;
    }

    /**
     * @param bool $hardMode
     */
    public function setHardMode(bool $hardMode): void
    {
        $this->hardMode = $hardMode;
    }

    /**
     * @param int $regionCode
     */
    public function setRegionCode(int $regionCode): void
    {
        $this->regionCode = $regionCode;
    }

    protected function changeUrl(string $url, bool $dump = true) : void
    {
        $opt = [];
        if (strpos($url, 'https://') === 0) {
            $opt = [
                "ssl"=> [
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ],
            ];
        }
        static::setHtml(file_get_contents($url, false, stream_context_create($opt)));
        $this->saveHistory();
        $this->domUrl = $url;
        if ($dump) dump($this->domUrl);
    }

    public function saveHistory() : void
    {
        $this->history[] = $this->domUrl;
    }

    public function checkHistory(?string $url = null) : bool
    {
        return array_search($url ?: $this->domUrl, $this->history) !== false;
    }
    /**
     * @return array|Court[]
     */
    public abstract function loadSupreme(): array ;

    /**
     * @return array|Court[]
     */
    public abstract function loadCity(): array ;

    /**
     * @return array|Court[]
     */
    public abstract function loadMagistrate():array;



    public function readStructuresByText(string $text, array &$structures,?string $department = null) : bool
    {
        //если есть номер телфона,разделяем по нему и выделяем части
        $new = [];
        if ($cleared = $text) {
            while(mb_eregi('([^\s\(]*('. self::ADMINISTARTE_WORDS_PATTERN .')[а-яй\-ё\w]*)', $cleared, $m)) {
                $pos = mb_strpos($cleared, $m[1]);
                $cleared = mb_substr($cleared, 0, $pos) . mb_substr($cleared, $pos+mb_strlen($m[1]));
            }
        }
        if ($cleared&& preg_match('/'. self::PHONE_PATTERN .'/', $cleared, $m)) {
            $cleared = preg_replace('/([\d\(\)\-])([\s]+)([\d\(\)\-])/', '$1$3', $cleared);
            $cleared = str_replace('&nbsp;', '', $cleared);
            if (preg_match_all('/'.self::PHONE_PATTERN.'/', $cleared, $m)) {
                foreach ($m[1] as $i => $fio) {
                    if (preg_match_all('/.*'.self::FIO_PATTERN.'.*/u',$fio, $fios)) {
                        unset($fios[0][count($fios[0]) - 1]); //удаляем последне ФИО с номером, остальные переносим для отдельного парсинга
                        foreach ($fios[0] as $j => $f) {
                            $m[1][$i] = mb_substr($m[1][$i], mb_strpos($m[1][$i], $f)+ mb_strlen($f));
                            $m[1][] = $f;
                            $m[3][] = '';
                        }
                    }
                }
                foreach ($m[3] as $i=> $phone) {
                    $m2 = [];
                    if (mb_ereg(self::FIO_PATTERN,$this->clear($m[1][$i]), $m2)  && !mb_eregi(self::FIO_EXCLUDE_PATTERN, $m2[1])) {
                        $fio = $this->clear($m2[1], '|\s\s+');
                        $structures[] = $structure = new CourtStructure(compact('fio', 'phone'));
                        $new[] = $structure;
                        $structure->source_url = $this->domUrl;
                        if ($department) $structure->department = $department;
                    }
                }
            }
            if ($m && count($new) < count($m[1]) - self::MIN_COURT_STRUCTURE_SIZE) $new = [];
        }
        // иначе пасрсим расположенные рядом ФИО и должность
        if(count($new) === 0) {
            $parts = array_filter(preg_split('/'.self::TAG_DELIMETER.'/', $text));
            if (count($parts) === 1 && count($np = Arr::where(preg_split('/\t*/', $parts[0]), function($v){
                return preg_match('/'.self::FIO_PATTERN.'/',$v);
            }))) $parts = $np;
            foreach($tmp = $parts as $i => $part) {
                if (count($ps = preg_split('/[^\t\r\n]+\d+/', $part)) > 2) {
                    unset($parts[$i]);
                    unset($tmp[$i]);
                    $parts = array_merge($parts, $ps);
                } else {
                    $splitted = [];
                    search_second_fio:
                    if (mb_ereg(self::FIO_PATTERN, $part, $first)
                        && !mb_eregi(self::FIO_EXCLUDE_PATTERN,$first[1])
                        && mb_ereg(self::FIO_PATTERN, (str_replace($first[1], '', $part)), $second)) {
                        $pos = mb_strpos($part, $second[0]);
                        $parts[] = $first = mb_substr($part, 0, $pos);
                        $splitted[] = $first;
                        $parts[$i] = $part = mb_substr($part, $pos);
                        if ($pos !== false) goto search_second_fio;
                    }
                }
            }
            $parts = array_values($parts);
            foreach($parts as $i => $part) {
                $len = intdiv(mb_strlen($part),2);
                $work_position = '';
                $fio = '';
                $part = mb_eregi_replace('('.self::ADMINISTARTE_WORDS_PATTERN.')', '',$part);
                if (mb_ereg(self::FIO_PATTERN.'.{0,'.$len.'}'.self::WORK_POSITION_PATTERN,$part, $m)) {
                    $fio = $m[1];
                    $work_position = $m[4];
                } else if (mb_ereg(self::FIO_PATTERN,$part, $m2)){
                    $fio = $m2[1];
                    if ((mb_ereg(self::WORK_POSITION_PATTERN,$parts[$i], $m3)
                            || $i > 0 && mb_ereg(self::WORK_POSITION_PATTERN,$parts[$i-1], $m3)))
                        $work_position = $m3[1];
                }
                if ($fio && !mb_eregi(self::FIO_EXCLUDE_PATTERN, $fio)) {
                    $structures[] = $structure = new CourtStructure(compact('fio', 'work_position'));
                    $structure->source_url = $this->domUrl;
                    if ($department) $structure->department = $department;
                }
            }
            //if (count($parts)) dump($parts);
        }
//        foreach ($structures as $s) dump($s->toArray());
//        dump('____');
        return count($structures) > 0;
    }

    public function getRequisiteByText(string $text) : ?CourtRequisite
    {
        $notifyWords = ['внимание'];
        foreach ($notifyWords as $word) if (($from = mb_stripos($text,  $word)) !== false
            && ($to = mb_strpos($text, self::TAG_DELIMETER, $from)) !== false) {
            $text = ($from > 0 ? mb_substr($text, 0, $from) .' ' : ' ') . mb_substr($text, $to);
        }
        foreach (CourtRequisite::REPLACES as $rule  => $val) {
            $text = preg_replace('/'.$rule.'/u', $val, $text);
        }
        //убираем лишние пробелы и табуляцию, вместе с "непереносимым пробелом"
        $text = preg_replace('/(\s\s+|\n|\r|&nbsp;)/', ' ', $text);
        //убираем числа стоящие отдельно из 2 символов, например "поле 14"
        $text = preg_replace('/([^0-9№]{4,})[0-9]{2}([^0-9]{4,})/u', '$1$2',$text);
        $requisite = new CourtRequisite();
        $requisite->readNumbersFromText($this->clear($text), $this->regionCode);
        $delimeter = '!!!';
        foreach ($hot_keys = CourtRequisite::HOT_KEYS as $f => $words) {
            foreach ($words as $word) {
                if (($pos = mb_stripos($text, $word)) !== false
                    || mb_eregi('('.$word .'( +'.self::TAG_DELIMETER.')?)', $text, $m)) {
                    if (isset($m) && $m) {
                        $pos = mb_strpos($text, $m[1]);
                        $word = $m ? $m[1] : $word;
                        unset($m);
                    }
                    $text = mb_substr($text, 0, $pos) . $delimeter . $word .
                        mb_substr($text, $pos + mb_strlen($word));
                    $hot_keys[$f]= [$word];
                    break;
                }
            }
        }
        $raws = array_filter(preg_split('/' . $delimeter . '/', trim($text)));
        foreach (($raws) as $i => $raw) {
            if (mb_strlen($raw) > 1500) {
                $raw = $this->firstTagValue( $raw);
                if (mb_strlen($raw) > 1000) continue;
            }
            foreach ($hot_keys as $field => $words) {
                $m = null;
                foreach ($words as $word) if (!$requisite->$field
                    && (($pos = mb_stripos(($raw), ($word))) !== false)) {
                    $isd = array_search($field, CourtRequisite::STRINGS) === false;
                    $nextString = mb_substr($raws[$i], $pos + mb_strlen($word));
                    if (!$isd) {
                        $nextString = $this->firstTagValue($nextString)?:'';
                        $nextString = preg_replace('/('.self::TAG_DELIMETER. '\s*)(\(.*)/u', '$2', $nextString);
                        $v = trim($this->clear($nextString,'|_+|={5,}'));//нижнее подчеркивание
                        $v = preg_replace('/([0-9]\s*([^0-9\s]{1,10})?){8,20}/u', '$2', $v);// убираем все значения числовых реквизит
                        foreach (CourtRequisite::HOT_KEYS as $ff => $hks) // убираем ключевые слова остальных реквизит
                            if ($ff !== $field) $v = preg_replace(('/('. str_replace('/','\/', implode('|', $hks)) .')/iu'), '', $v);
                        if (($w = Arr::get(CourtRequisite::REQUIRED_WORDS,$field))&&!mb_eregi('('.$w.')', $v))//если отстутсвуют обязательные слова - очищаем
                            $v = null;

                        $v = preg_replace('/^[\W\s,.;\-:]{1,15}/u', ' ', $v); // очищаем символы в начале строки
                        if (mb_strlen($v) > 600) {
                            $v = mb_substr($v, 0, 600);
                        }
                        //TAG_DELIMETER first letter
                    } else if ($isd && preg_match('/(\d[^Tа-яА-Я№]+)/', $nextString, $m)) {
                        $v = preg_split('/\s\s[^\d]/', $m[1])[0];
                    } else if ($isd && preg_match('/' . self::TAG_DELIMETER . '(.+?)' . self::TAG_DELIMETER . '/', $nextString, $m)) {
                        $v = $m[1];
                    }
                    if (isset($v) && $v) {
                        if ($isd) $v = mb_ereg_replace('[^0-9]', '', $v) ?: $v;
                        if ($isd && !preg_match('/[0-9]+/', $v)) continue;
                        if (($l = Arr::get(CourtRequisite::NUMBER_LENGTHS, $field)) && mb_strlen($v) > $l)
                            $v = mb_substr($v, -$l);
                        if (!$isd || !$l || $l == mb_strlen($v)) $requisite->$field = $v;
                    }
                    unset($v);
                    $m = null;
                    break;
                }
            }
        }
        if (!$requisite->inn && !$requisite->kpp && !$requisite->account) return null;
        $requisite->source_url = $this->getDomUrl();
        return $requisite;
    }




    public function firstLinkNodeFromContainerNode(\simple_html_dom_node $root, string $containerSelector, ?array $keywords = null): ?\simple_html_dom_node
    {

        $links = [];
        if (!$keywords) return null;
        foreach ($root->find($containerSelector) ?? [] as $link) {
            if ((mb_eregi('(' .$keywords[0] . ')', $link->innertext())
                    || mb_eregi('(' .$keywords[0] . ')', $link->text()))
                    && ((!$ex = Arr::get($keywords, 2)) || !mb_eregi("($ex)", $link->text()))) {
                $links[] = $link;
            }
        }
        if (count($links) !== 1 && count($keywords) > 1 && $keywords[1]) {
            foreach ((array) $keywords[1] as $i => $keyword) {
                foreach ($links as $j => $link) {
                    if (mb_strpos(mb_strtolower($link->innertext()), $keyword) !== false)
                        return $link;
                }
            }
        }
        if (count($links) == 0) return null;
        return Arr::first($links);
    }

    public function firstTagValue(string $str, string $delimetr = self::TAG_DELIMETER, int $minLength = 5) : ?string
    {
        return Arr::first(preg_split('/('.$delimetr.')/u', $str) ?:[]
            , function($v) use ($minLength){
                return mb_strlen(preg_replace('/\s\s+/', ' ',$v)) >= $minLength;
            }
        );
    }
    public function firstElementByIdPrefix(string $idPrefix, ?\simple_html_dom_node $root = null, bool $skipZero = false) : ?\simple_html_dom_node
    {
        if (!$root) $root = $this->domHtml->root;
        $text = $root->innertext();
        //dump('searching #' . $idPrefix . ' in ' . $root->getAttribute('class') . ' '. $this->domUrl) ;
        search:
        if (preg_match("/($idPrefix(\d+)\b)/", $text, $m)){
            if ($skipZero && $m[2] == '0') {
                $text = str_replace($m[1], '', $text);
                goto search;
            }
            return $root->find('#'. $m[1], 0);
        }
        //dump('not found');
        return null;
    }

    public static function clear(string $string, string $addRegexp = '') : string {
        return trim(preg_replace('/('.self::TAG_DELIMETER.'|\n|\r'.$addRegexp.')/', ' ', $string));
    }

    /**
     * @return string
     */
    public function getUrl() : string {
        return $this->url;
    }

    /**
     * @return array
     */
    public function getSupreme(): array
    {
        return $this->supreme;
    }

    /**
     * @param array $supreme
     */
    public function setSupreme(array $supreme): void
    {
        $this->supreme = $supreme;
    }

    /**
     * @return array
     */
    public function getCity(): array
    {
        return $this->city;
    }

    /**
     * @param array $city
     */
    public function setCity(array $city): void
    {
        $this->city = $city;
    }

    /**
     * @return array
     */
    public function getMagistrate(): array
    {
        return $this->magistrate;
    }

    /**
     * @param array $magistrate
     */
    public function setMagistrate(array $magistrate): void
    {
        $this->magistrate = $magistrate;
    }

    /**
     * @return string
     */
    public function getHtml(): string
    {
        return $this->html;
    }

    /**
     * @param string $html
     * @throws LargeHtmlException
     */
    public function setHtml(string $html): void
    {
        $html = preg_replace('/src="data:image\/(png|jpeg|jpg);base64[^>]+>/i', 'src=', $html);
        $this->html = $html;
        $this->domHtml = str_get_html($this->getHtml());
        if (!$this->domHtml) throw new LargeHtmlException("Large page html for " . $this->domUrl);
    }

    /**
     * @return int
     */
    public function getRegionCode(): int
    {
        if ($this->regionCode) return $this->regionCode;
        return Court::query()->where('url', '~*', $this->url)
            ->firstOrNew()->region_code ?: $this->regionCode;
    }

    /**
     * @return null
     */
    public function getCourtLinkPart()
    {
        return $this->courtLinkPart;
    }

    /**
     * @param null $courtLinkPart
     */
    public function setCourtLinkPart($courtLinkPart): void
    {
        $this->courtLinkPart = $courtLinkPart;
    }

    public function needDebugCourt(Court $court) : bool
    {
        return !$this->getCourtLinkPart() || strpos($court->url, $this->getCourtLinkPart()) !== false;
    }

    /**
     * @param string $url
     * @param Court $court
     * @return CourtStructure
     */
    public function saveStructureWithFile(string $url, Court $court) : CourtStructure
    {
        $s = new CourtStructure();
        $s->file = $url;
        $s->source_url= static::getDomUrl();
        $court->structure()->save($s);
        return $s;
    }

    /**
     * @param string $url
     * @param Court $court
     * @return CourtRequisite
     */
    public function saveRequisiteWithFile(string $url, Court $court) : CourtRequisite
    {
        $s = new CourtRequisite();
        $s->file = $url;
        $s->source_url= static::getDomUrl();
        $court->requisite()->save($s);
        return $s;
    }


    public function getDomainByUri(string $url) : ?string
    {
        preg_match('/((https?:\/\/)?[^\/?]+).*/', $url, $m);
        return Arr::get($m, 1);
    }

    /**
     * @param \simple_html_dom_node $ul
     * @return Court
     */
    public function makeCourtByUlSimpleHtmlNode(\simple_html_dom_node $ul): Court
    {

        $court = new Court();
        $parts = $ul->childNodes();
        foreach ($parts as $i => $child) {
            $child = trim(($child->innertext()));
            if (!$child) {
                continue;
            }
            if ((strpos($child, 'Адрес')) !== false) {
                $court->address = str_replace('Адрес:', '', $child);
            }
            if ((strpos($child, 'Телефон')) !== false) {
                $court->phone = str_replace('Телефон:', '', $child);
            }

            if ((strpos($child, 'mail')) !== false) {
                $court->email = $ul->childNodes()[$i]->find('a', 0)->innertext();
            }
            if ((strpos($child, 'сайт')) !== false) {
                $court->url = $ul->childNodes()[$i]->find('a', 0)->innertext();
            }
        }
        $court->region_code = $this->getRegionCode();
        $court->name = $ul->parent->parent->parent->parent->childNodes(0)->innertext();
        return $court;
    }

    public function success(string $entity, Court $court) : void
    {
        dump('Parsed ' . $court->$entity()->count() . ' '. $entity . ' for ' . $court->url);
    }

    public function error(string $entity, Court $court, ?\Throwable $e = null) {
        $method= $this->hardMode ? 'dd' : 'dump';
        if ($e) {
            dump($e->getLine() . $e->getMessage(), $e->getTraceAsString());
        }
        $method('Not parsed ' . $entity . ' for '. $court->url .' in page ' . $this->domUrl);
    }

    /**
     * @param RegionCourtParser $magistrateParser
     */
    public function setMagistrateParser(RegionCourtParser $magistrateParser): void
    {
        $this->magistrateParser = $magistrateParser;
    }

    /**
     * @param DebtorDeal $deal
     */
    public function checkAndFillDeal(DebtorDeal $deal, Debtor $debtor) : void
    {
        $debtor->setError(Debtor::ERROR_UNKNOWN_PARSER);
    }

    protected function getDealIndexes(\simple_html_dom_node $tableHeadRow, array $result = []) : array
    {
        $childs = ($tableHeadRow->find('th') ?: $tableHeadRow->find('td'));
        $result = $result ?: [
            'deal_number' => '((номер|№) ?дела)|(дело)',
            'deal_verdict' => 'решение',
            'deal_start_date' => 'дата( |<\/?\w+>)(поступления|дела)',
            'deal_end_date' => 'дата( |<\/?\w+>)решения',
            'deal_initiator' => 'категория|лица|информация',
            'deal_link' => 'подробн(ее|о)',
        ];
        foreach ($result as $field => $val) {
            foreach ($childs as $i => $child) {
                $m = [];
                if ((mb_eregi('(' . $val . ')', $child->outertext(), $m))) {
                    $result[$field] = $i;
                    break;
                }
            }
            if (!is_int($result[$field])) unset($result[$field]);
        }
        return $result;
    }

}

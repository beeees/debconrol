<?php

namespace App\Console\Commands;

use App\Console\Commands\Parsers\RegionParsers\KRKParser;
use App\Console\Commands\Parsers\RegionParsers\YanaoMirsudParser;
use App\Console\Commands\Parsers\SudrfParser;
use App\Jobs\CheckDebtorCourtJob;
use App\Jobs\ErrorReportJob;
use App\Jobs\ReportJob;
use App\Models\Catalog\Court;
use App\Models\Catalog\Region;
use App\Models\Debtor;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class CourtStructureUpdate extends Command implements RegionParsersInfo
{
    /**
     * --all             - To run new parse process for all regions without courts
     * --all --force     - To run new parse process for all
     * --hard            - To finish parse process for first bad (to parse) court site link
     * @var string
     */
    protected $signature = 'update:court-data {region?} {court_url_part?} {--hard} {--supreme} {--city} {--magistrate} {--all} {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load courts data from web sites';

    private $region;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (!$code = $this->argument('region')){
            if (!$this->option('all')) {
                foreach (self::REGIONS as $code => $region){
                    $this->info("[$code] - {$region['name']}");
                }
                $code = $this->ask('Enter region code to update:');
            } else {
                foreach (self::REGIONS as $code => $region) {
                    try { if (($this->option('force') || !(Court::query()->where(['region_code' => $code])->count()))) {
                        $this->info("[$code] - {$region['name']}");
                        $this->parseRegion($code);
                        if (!Court::query()->where(['region_code' => $code])->count()) {
                            dispatch_now(new ReportJob('Не удалось распарсить список судов для региона ' . $region['name'], true));
                        } else $this->replaceDebtorDeletedCourts($code);
                    }} catch (\Throwable $e) {
                        dispatch_now(new ErrorReportJob($e, 'Не удалось распарсить список судов для региона ' . $region['name'], true));
                    }
                }
                return;
            }
        } else {
            $this->info("[$code] - " . self::REGIONS[$code]['name']);
        }
        $this->parseRegion($code);
        $this->replaceDebtorDeletedCourts($code);
        return 0;
    }

    /**
     * @param $region
     */
    private function parseRegion($region)
    {
        $claz = Arr::get(self::REGIONS[$region], 'parser', SudrfParser::class);
        /** @var AbstractParser $parser */
        $parser = new $claz(Arr::get(self::REGIONS[$region], 'url', 'https://sudrf.ru'));
        if (isset(self::REGIONS[$region]['magistrate'])) {
            $mClaz = self::REGIONS[$region]['magistrate']['parser'];
            $parser->setMagistrateParser(new $mClaz(self::REGIONS[$region]['magistrate']['url']));
        }
        $parser->setRegionCode($region);
        $parser->setCourtLinkPart($part = $this->argument('court_url_part'));
        if ($this->option('hard')) $parser->setHardMode(true);
        $this->region = Region::query()->firstOrCreate([
            'code' => $region,
        ],[
            'name' => self::REGIONS[$region]['name'],
        ]);
        if ($parser instanceof AbstractParser) {
            if (!$this->option('city') && !$this->option('magistrate') && Arr::get(self::REGIONS[$region], 'supreme', true))
                $this->saveSupreme($parser);
            if (!$this->option('supreme') && !$this->option('magistrate'))
                $this->saveCity($parser);
            if (!$this->option('city') && !$this->option('supreme'))
                $this->saveMagistrate($parser);
        }
    }

    /**
     * @param AbstractParser $parser
     */
    private function saveSupreme(AbstractParser $parser)
    {
        try {
            DB::beginTransaction();
            $this->region->courts()->supreme()->delete();
            $courts  = $parser->loadSupreme();
            DB::commit();
        } catch (\Throwable $e) {
            DB::rollBack();
            throw  $e;
        }
    }

    /**
     * @param AbstractParser $parser
     */
    private function saveCity(AbstractParser $parser)
    {
        try {
            DB::beginTransaction();
            $this->region->courts()->city()->delete();
            $courts  = $parser->loadCity();
            DB::commit();
        } catch (\Throwable $e) {
            DB::rollBack();
            throw  $e;
        }
    }
    private function saveMagistrate(AbstractParser $parser)
    {
        try {
            DB::beginTransaction();
            $this->region->courts()->magistrate()->delete();
            $courts = $parser->loadMagistrate();
            DB::commit();
        } catch (\Throwable $e) {
            DB::rollBack();
            throw  $e;
        }
    }

    /**
     * @param $region
     */
    private function replaceDebtorDeletedCourts($region)
    {
        $debtors = Debtor::query()->whereHas('courts', function($q) use($region) {
            $q->onlyTrashed()->where('region_code', $region);
        })->get(['id'])->pluck('id')->toArray();
        if ($debtors) dispatch(new CheckDebtorCourtJob(0, 10, null, $debtors, true));
    }

}

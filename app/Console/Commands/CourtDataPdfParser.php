<?php

namespace App\Console\Commands;

use App\Jobs\ReportJob;
use App\Models\Catalog\Court;
use App\Models\Catalog\CourtRequisite;
use App\Models\FnsCourtData;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Console\Command;

class CourtDataPdfParser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:court-nalog-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $client = new Client();
        $r = $client->get('https://service.nalog.ru/static/tree2.html?inp=ifns&tree=SOUN_PAYEE&treeKind=LINKED&aver=3.0.20&sver=4.38.64&pageStyle=GM2');
        $body = ($r->getBody()->getContents());
        preg_match('/var TREE.?=.?(\[.+\]);/', $body, $m);
        $ifnsList = json_decode(@$m[1], true)[3];
        foreach ($ifnsList as $subject => $subjectIfns) {
            foreach ($subjectIfns[3] as $ifnsInfo) {
                $ifns = $ifnsInfo[0];
                $r = $client->post('https://service.nalog.ru/payment/payee-proc.json', [
                    RequestOptions::QUERY => [
                        'c' => 'getOktmmf',
                        'ifns' => $ifns,
                        'okatom' => '',
                    ]
                ]);
                $oktmmfList = json_decode($r->getBody()->getContents(), true)['oktmmfList'];
                foreach ($oktmmfList as $oktmn => $name) {
                    $r = $client->post('https://service.nalog.ru/payment/gp-proc.json', [
                        RequestOptions::QUERY => [
                            'step' => 'PAYER',
                            'payerKind' => 'ul',
                            'paymentDocKind' => 'po',
                            'kbkGroup' => 'tribunal',
                            'kbkNoProg' => 'tribunal-18210803010010000110',
                            'kbkProg' => '1050',
                            'sum' => '123',
                            'gpAddrDocKind1' => '1',
                            'gpAddrDocKind2' => '',
                            'objectAddr' => '',
                            'objectAddr_zip' => '',
                            'objectAddr_ifns' => '',
                            'objectAddr_okatom' => '',
                            'region' => '',
                            'ifns' => $ifns,
                            'oktmmf' => $oktmn,
                            'fam' => '',
                            'nam' => '',
                            'otch' => '',
                            'innFl' => '',
                            'addrFl' => '',
                            'addrFl_zip' => '',
                            'addrFlIno' => '',
                            'addrFlTag' => 'rf',
                            'PreventChromeAutocomplete' => '',
                        ] + $replaces = [
                            'payerName' => 'Наименование компании',
                            'payerInn' => '2122004101',
                            'payerKpp' => '212201001',
                            'payerAccount' => '03100643000000011500',
                            'payerBankBic' => '019706900',
                            'payerBankName' => 'PAYER_BANK_NAME',
                            'payerBankAccount' => '77777777777777777777',
                        ]
                    ]);
                    $token = json_decode($r->getBody()->getContents(), true)['epdToken'][0];
                    $r = $client->post('https://service.nalog.ru/static/payment-order-new.pdf', [
                        RequestOptions::QUERY => compact('token'),
                    ]);
                    file_put_contents('file.pdf', $r->getBody()->getContents());
                    $parser = new \Smalot\PdfParser\Parser();
                    $pdf = $parser->parseFile('file.pdf');
                    $text = $pdf->getText();
                    foreach ($replaces as $replace) {
                        $text = mb_ereg_replace("\b$replace\b", 'CLEARED', $text);
                    }
                    $lines = explode("\n", $text);
                    $this->parsePdfLines($lines, $ifns, $oktmn);
                    unlink('file.pdf');
                    sleep(2);
                }
            }
        }
        return 0;
    }
    public function parsePdfLines(array $lines, $ifns, $oktmm) {
        foreach ($lines as $i => $line) {
            if (strpos($line, 'CLEARED') !== false) unset($lines[$i]);
        }
        $req = new CourtRequisite();
        foreach ($lines as $i => $line) {
            if (!$req->bank_name &&
                preg_match('/Банк\s+получателя/u',$line)) {
                $req->bank_name = $lines[$i - 1];
                continue;
            }
            if (!$req->inn &&
                preg_match('/ИНН[\D]+(\d+)[\D]+КПП[\D]+(\d+)/u',$line, $m)) {
                $req->inn = $m[1];
                $req->kpp = $m[2];
                continue;
            }

            if (!$req->receiver_name &&
                preg_match('/Получатель/u',$line)) {
                $req->receiver_name = $lines[$i - 1];
                continue;
            }

            if (!$req->bik &&
                preg_match('/БИК[\D]+(\d+)/u',$line, $m)) {
                $req->bik = $m[1];
                continue;
            }

            if ((!$req->account || !$req->kazna_account) &&
                preg_match('/Сч\.[\D]+№[\D]?(\d+)/u',$line, $m)) {
                if (strpos($m[1], CourtRequisite::NUMBER_START_REGEXPS['kazna_account'][0]) === 0) {
                    $req->kazna_account = $m[1];
                } else {
                    $req->account = $m[1];
                }
                continue;
            }
            if (!$req->kbk && preg_match('/(('.implode('|',CourtRequisite::NUMBER_START_REGEXPS['kbk']).')\d+)\s+(\d+)/', $line, $m)) {
                $req->kbk = $m[1];
                $req->oktmo = $m[3];
            }
        }
        $req->source_url = 'https://service.nalog.ru';
//        dump($req->toArray());
        if (count($arr = $req->toArray()) == 9 && $req->oktmo && $req->inn) {
            $req = CourtRequisite::query()->where([
                'inn' => $req->inn,
                'oktmo' => $req->oktmo,
            ])->first();
            if ($req) {
                $req->fill($arr);
                if ($req->by_ifns && $req->isDirty()) {
                    $msg = 'На сайте налоговой изменились реквизиты, привязанные к участку('.
                        $ifns.','. $oktmm.') - ' . $req->court->fullName();
                    dispatch(new ReportJob($msg));
                }
            } else {
                $fnslist = FnsCourtData::query()->where([
                    'ifns' => $ifns,
                    'oktmo' => $oktmm,
                ])->get();
                $arr['by_ifns'] = true;
                foreach ($fnslist as $fns) {
                    if ($court = Court::query()->byName($fns->name)
                        ->whereDoesntHave('requisite')
                        ->where(['region_code' => (int)substr($ifns.'', 0,2)])->first())
                        $court->requisite()->save(new CourtRequisite($arr));
                }
            }
        } else {
            dump('bad pdf ИФНС '. $ifns . ' ОКТМН ' . $oktmm);
//            if ($this->ask("Continue(Y|N)?") !== 'Y') exit();
        }
    }
}

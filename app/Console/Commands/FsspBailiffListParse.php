<?php

namespace App\Console\Commands;

use App\Models\Catalog\Bailiff;
use Facades\App\Utils\ParserApi;
use Facades\App\Utils\AntikapchaApi;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\RequestOptions;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class FsspBailiffListParse extends Command
{
    public const TABLE_HEADERS = [
        0 => "Территориальный отдел судебных приставов",
        1 => "Адрес",
        2 => "ФИО старшего судебного пристава",
        3 => "Телефон",
        4 => "Факс",
        5 => "Часы приема",
        6 => "Телефоны справочной службы",
        7 => "Районы обслуживания",
    ];
    public const ARRAY_HEADERS = [
        'name',
        'address',
        'fio',
        'phone',
        'faks',
        'time',
        'other_phones',
        'districts',
    ];
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bailiff:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle()
    {
        $regionError = 0;
        for ($i = 1; $i <=82; $i++) {
            try {
                $bs = $this->parseRegion($i);
                if ($bs) {
                    Bailiff::query()->where(['region_code' => $i])->delete();
                    foreach ($bs as $b) $b->save();
                }
            } catch (ServerException $e) {
                if ($e->getCode() === 503) {
//                    dump($e->getMessage());
                    dump('503 response');
                    $regionError++;
                    if ($regionError > $maxError = 30) {
                        $regionError = 0; continue;
                    } else {
                        $i--;
                    }
                    sleep(10);
                } else {
                    throw $e;
                }
            }
        }
    }

    /**
     * @param int $region
     * @return array|Bailiff[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function parseRegion(int $region) : array
    {
        $result = [];
        $jar = new CookieJar();
        $client = new Client();
        $random = time();
        $diff = 10;
        $callbackParam = 'jQuery3400009589346414565325_' . $random;
        $page = 1;
        $code = null;
        start:
        $params = (function () use ($region, $callbackParam, &$page, &$diff, $random, $code) {
            return ['nocache' => 1,
                    'system' => 'agency_reestr',
                    'is' => [
                        'all' => '',
                        'query' => '',
                        'region_id' => [str_pad($region, 2, '0', STR_PAD_LEFT)],
                    ],
                    'callback' => $callbackParam,
                    'page' => &$page,
                    '_' => time(),
                ] + ($code ? compact('code') : []);
        });
        $r = $client->get($url = 'https://is.fssp.gov.ru/ajax_search', [
            RequestOptions::QUERY => $params(),
            RequestOptions::COOKIES => $jar,
        ]);
        $con = $r->getBody()->getContents();
        decode:
        $data = json_decode($con, true);
        dump('load new image');
        if (!Arr::get($data, 'data') && !strpos($con, '<img src')) {
            goto  start;
        }
        $matches = [];
        preg_match('/img src=(\\\\)?"data:image\/(jpeg|png|jpg);base64,([^\\\\"]+)(\\\\)*"/', $con = isset($data['data']) ? $data['data'] : $con, $matches);
        if ($a = &$matches[3]) {
            file_put_contents('img.'.$matches[2], base64_decode($a));
            try {
                $task = ParserApi::captcha($a);
            } catch (ClientException $e) {
                $task= [];
            }
            toRepeat:
            if ($text = &$task['code']){
                if (!$text) {
                    dump('noSolution');
                    $random++;
                    goto start;
                } else {
                    $code = $text;
                    get_page:
                    $r = $client->get($url = 'https://is.fssp.gov.ru/ajax_search', [
                        RequestOptions::QUERY => $params(),
                        RequestOptions::COOKIES => $jar,
                    ]);
                    dump('decoded =' . $text);
                    $con = $r->getBody()->getContents();
                    if (strpos($con, 'form') !== false) {
                        $code = null;
                        goto decode;
                    }
                    if (strpos($con, 'ничего не найдено') == false) {
                        preg_match('/.*({"data":".+","err":[^}]+})\);/', $con, $m);
                        $dom = str_get_html(json_decode($m[1], true)['data']);
                        foreach ($dom->root->find('table tr') as $i => $row) {
                            if ($i === 0) foreach ($row->find('th') as $th) {
                                $header = trim(AbstractParser::clear($th->innertext(), '|\s\s+'));
                                if (array_search($header, self::TABLE_HEADERS) === false
                                    && $this->ask('Headers in table will be changed, continue (Y/N)?') !== 'Y') return [];
                            } else {
                                $array = [];
                                foreach ($row->find('td') as $j => $td) {
                                    $array[] = trim(AbstractParser::clear($td->innertext(), '|\s\s+'));
                                }
                                $result[] = $b = new Bailiff(array_combine(self::ARRAY_HEADERS, $array));
                                $b->region_code = $region;
                            }
                        }
                        $page++;
                        dump('wait next page');
                        sleep(2);
                        goto get_page;
                    }
                }
            }
        }
        return $result;
    }
}

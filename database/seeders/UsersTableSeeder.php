<?php

namespace Database\Seeders;

use App\Models\CanAuthUser;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $email = Factory::create()->email;
        $email = env('ADMIN_USER_EMAIL', 'jura120596@gmail.com');
        if (DB::table('users')->where(compact('email'))->count()) return;
        DB::table('users')->insert([
            [
                'email' => $email,
                'password' => Hash::make(env('ADMIN_USER_PASSWORD', 'admin1')),
                'phone' => Factory::create()->phoneNumber,
                'is_company_user' => false,
                'is_super_admin' => true,
                'name' => 'Super Admin',
                'info' => '',
                'user_group' => CanAuthUser::ADMIN_GROUP,
            ],
        ]);
    }
}

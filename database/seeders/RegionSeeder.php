<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('regions')->insert([
            [
                'name' => 'Region 1',
                'code' => 1,
            ],
            [
                'name' => 'Region 2',
                'code' => 2,
            ],
        ]);
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBailiffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bailiffs', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('region_code')->nullable();
            $table->string('name', 500)->nullable();
            $table->string('address', 500)->nullable();
            $table->string('fio', 500)->nullable();
            $table->string('phone', 50)->nullable();
            $table->string('faks', 50)->nullable();
            $table->string('time', 200)->nullable();
            $table->string('other_phones', 500)->nullable();
            $table->string('districts')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bailiffs');
    }
}

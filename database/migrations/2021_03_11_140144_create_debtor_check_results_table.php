<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDebtorCheckResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debtor_check_results', function (Blueprint $table) {
            $table->id();
            $table->string('region');                       
            $table->string('address');                      
            $table->string('ip_date');                      
            $table->string('ip_number');                    
            $table->string('ip_summary_case');              
            $table->string('id_date');                      
            $table->integer('id_type');
            $table->string('id_type_name');                 
            $table->string('id_number');                    
            $table->string('id_issuer');                    
            $table->string('ip_end_date');                  
            $table->string('ip_end_reason');                
            $table->string('subject_name');                 
            $table->double('subject_amount');
            $table->double('subject_fees');
            $table->string('bailiff_department_name');      
            $table->string('bailiff_department_post_index');
            $table->string('bailiff_department_address');   
            $table->string('bailiff_fullname');             
            $table->string('bailiff_phones');
            $table->timestamps();
        });
        Schema::create('debtors_results', function(Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('debtor_id');
            $table->foreign('debtor_id')
                ->references('id')
                ->on('debtors')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->unsignedBigInteger('debtor_check_result_id');
            $table->foreign('debtor_check_result_id')
                ->references('id')
                ->on('debtor_check_results')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('debtors_results');
        Schema::dropIfExists('debtor_check_results');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNullableWorkPosition extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('court_structures', function (Blueprint $table) {
            $table->string('work_position', 500)->nullable()->change();
            $table->string('fio', 255)->nullable()->change();
            $table->string('file', 500)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('court_structures', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRequisitesResourceColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('court_requisites', function (Blueprint $table) {
            $table->string('source_url', 255)->nullable();
        });
        Schema::table('court_structures', function (Blueprint $table) {
            $table->string('source_url', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('court_requisites', function (Blueprint $table) {
            $table->dropColumn('source_url');
        });
        Schema::table('court_structures', function (Blueprint $table) {
            $table->dropColumn('source_url');
        });
    }
}

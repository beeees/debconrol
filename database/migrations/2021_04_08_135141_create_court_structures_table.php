<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourtStructuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('court_structures', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('court_id');
            $table->foreign('court_id')
                ->references('id')
                ->on('courts')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->string('work_position', 255);
            $table->string('fio', 255);
            $table->string('room', 255)->nullable();
            $table->string('phone', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('court_structures');
    }
}

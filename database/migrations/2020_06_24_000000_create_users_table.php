<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email')
                ->unique()
                ->nullable();
            $table->string('password')
                ->nullable(false)
                ->default('');
            $table->string('phone',25)->unique()->nullable();
            $table->boolean('is_blocked')
                ->default(false);
            $table->boolean('is_company_user')->index();
            $table->boolean('is_super_admin')->nullable();
            $table->mediumInteger('user_group');
            $table->string('name');
            $table->string('info');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('group_roles');
        Schema::dropIfExists('groups');
        Schema::dropIfExists('roles');
    }
}

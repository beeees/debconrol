<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCompanyUploadedFilesParserTaskColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_uploaded_files', function (Blueprint $table) {
            $table->string('task_type')->nullable();
            $table->string('task_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_uploaded_files', function (Blueprint $table) {
            $table->dropColumn(['task_type', 'task_code']);
        });
    }
}

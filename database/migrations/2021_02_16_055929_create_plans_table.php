<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_plans', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('sort');
            $table->string('name')->unique();
            $table->double('cost');
            $table->smallInteger('interval');
            $table->smallInteger('interval_type');
            $table->unsignedInteger('cases')->nullable();
            $table->unsignedInteger('records')->nullable();
            $table->boolean('full_check');
            $table->boolean('is_active');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_plans');
    }
}

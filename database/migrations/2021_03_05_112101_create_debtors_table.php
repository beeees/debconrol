<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDebtorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debtors', function (Blueprint $table) {
            $table->id();
            $table->string('mark'      );
            $table->string('name'      );
            $table->string('secondname');
            $table->string('lastname'   );
            $table->timestamp('birthday'  )->nullable();
            $table->integer('regionCode'    )->nullable();
            $table->timestamp('finished_at')->nullable();
            $table->integer('ip'        )->nullable();
            $table->double('sum'       )->nullable();
            $table->integer('regions'   )->nullable();
            $table->integer('ended'     )->nullable();
            $table->integer('repeatly'  )->nullable();
            $table->integer('gibdd'     )->nullable();
            $table->double('gibdd_sum' )->nullable();
            $table->integer('zkh'       )->nullable();
            $table->double('zkh_sum'   )->nullable();
            $table->integer('credit'    )->nullable();
            $table->double('credit_sum')->nullable();
            $table->integer('ipoteka'   )->nullable();
            $table->double('ipoteka_sum')->nullable();
            $table->unsignedBigInteger('case_id');
            $table->unsignedInteger('company_uploaded_file_id');
            $table->foreign('company_uploaded_file_id')
                ->references('id')
                ->on('company_uploaded_files')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('case_id')
                ->references('id')
                ->on('cases')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('debtors');
    }
}

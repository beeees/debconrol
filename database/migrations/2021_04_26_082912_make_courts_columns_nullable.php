<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakeCourtsColumnsNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('courts', function (Blueprint $table) {
            $table->string('address', 1000)->nullable()->change();
            $table->string('phone', 255)->nullable()->change();
            $table->string('email', 255)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Models\Catalog\Court::query()->whereNull('address')->update(['address' => '']);
        \App\Models\Catalog\Court::query()->whereNull('phone')->update(['phone' => '']);
        \App\Models\Catalog\Court::query()->whereNull('email')->update(['email' => '']);
        Schema::table('courts', function (Blueprint $table) {
            $table->string('address', 1000)->nullable(false)->change();
            $table->string('phone', 255)->nullable(false)->change();
            $table->string('email', 255)->nullable(false)->change();
        });
    }
}

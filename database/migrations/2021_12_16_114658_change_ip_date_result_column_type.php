<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeIpDateResultColumnType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('debtor_check_results', function (Blueprint $table) {
            $table->timestamp('ip_date_tmp')->nullable();
        });
        foreach (\App\Models\DebtorCheckResult::all() as $result) {
            try {
                $result->ip_date_tmp = \Carbon\Carbon::parse($result->ip_date);
                $result->save();
            } catch (\Throwable $e) {}
        }
        Schema::table('debtor_check_results', function (Blueprint $table) {
            $table->dropColumn('ip_date');
        });
        Schema::table('debtor_check_results', function (Blueprint $table) {
            $table->timestamp('ip_date')->nullable();
        });
        foreach (\App\Models\DebtorCheckResult::all() as $result) {
            try {
                $result->ip_date = \Carbon\Carbon::parse($result->ip_date_tmp);
                $result->save();
            } catch (\Throwable $e) {}
        }
        Schema::table('debtor_check_results', function (Blueprint $table) {
            $table->dropColumn('ip_date_tmp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        throw new Exception('cant rollback');
    }
}

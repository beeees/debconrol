<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeIdDateResultColumnType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('debtor_check_results', function (Blueprint $table) {
            $table->timestamp('id_date_tmp')->nullable();
        });
        foreach (\App\Models\DebtorCheckResult::all() as $result) {
            try {
                $result->id_date_tmp = \Carbon\Carbon::parse($result->id_date);
                $result->save();
            } catch (\Throwable $e) {}
        }
        Schema::table('debtor_check_results', function (Blueprint $table) {
            $table->dropColumn('id_date');
        });
        Schema::table('debtor_check_results', function (Blueprint $table) {
            $table->timestamp('id_date')->nullable();
        });
        foreach (\App\Models\DebtorCheckResult::all() as $result) {
            try {
                $result->id_date = \Carbon\Carbon::parse($result->id_date_tmp);
                if ($result->id_date->diffInDays(\Carbon\Carbon::now()) !== 0) $result->save();
            } catch (\Throwable $e) {}
        }
        Schema::table('debtor_check_results', function (Blueprint $table) {
            $table->dropColumn('id_date_tmp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        throw new Exception('cant rollback');
    }
}

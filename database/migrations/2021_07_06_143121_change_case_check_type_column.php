<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeCaseCheckTypeColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cases', function (Blueprint $table) {
            $table->smallInteger('check_type')->nullable();
        });
        \Illuminate\Support\Facades\DB::table('cases')->where('full_check', false)
            ->update(['check_type' => \App\Models\Company\CompanyCase::ASSESSED_CHECK_TYPE]);
        \Illuminate\Support\Facades\DB::table('cases')->where('full_check', true)
            ->update(['check_type' => \App\Models\Company\CompanyCase::FULL_CHECK_TYPE]);
        Schema::table('cases', function (Blueprint $table) {
            $table->dropColumn('full_check');
            $table->smallInteger('check_type')->nullable(false)->change();
        });
        Schema::table('company_plans', function (Blueprint $table) {
            $table->boolean('deal_check')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('cases', function (Blueprint $table) {
            $table->boolean('full_check')->default(false);
        });
        \Illuminate\Support\Facades\DB::table('cases')->where('check_type', \App\Models\Company\CompanyCase::FULL_CHECK_TYPE)
            ->update(['full_check' => true]);
        Schema::table('cases', function (Blueprint $table) {
            $table->dropColumn('check_type');
        });


        Schema::table('company_plans', function (Blueprint $table) {
            $table->dropColumn('deal_check');
        });
    }
}

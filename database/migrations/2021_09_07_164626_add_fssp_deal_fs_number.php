<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class AddFsspDealFsNumber
 */
class AddFsspDealFsNumber extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('debtor_fssp_deals', function (Blueprint $table) {
            $table->string('fs_number', 255)->nullable();
            $table->string('deal_number', 255)->nullable()->change();
            $table->string('closed', 255)->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('debtor_fssp_deals', function (Blueprint $table) {
            $table->dropColumn('fs_number');
        });
    }
}

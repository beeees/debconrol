<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourtRequisitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('court_requisites', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('court_id');
            $table->foreign('court_id')
                ->references('id')
                ->on('courts')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->string ('receiver_name', 1000)->nullable();
            $table->string ('inn', 255)->nullable();
            $table->string ('kpp', 255)->nullable();
            $table->string ('account', 255)->nullable();
            $table->string ('bank_name', 255)->nullable();
            $table->string ('bik', 255)->nullable();
            $table->string ('oktmo', 255)->nullable();
            $table->string ('kbk', 255)->nullable();
            $table->string ('kazna_account', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('court_requisites');
    }
}

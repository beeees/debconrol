<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDealLinkColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('debtor_deals', function (Blueprint $table) {
            $table->string('deal_link', 500)->nullable();
            $table->string('deal_initiator')->nullable();
            $table->string('loaded_initiator')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('debtor_deals', function (Blueprint $table) {
            $table->dropColumn('deal_link');
            $table->dropColumn('deal_initiator');
            $table->dropColumn('loaded_initiator');
        });
    }
}

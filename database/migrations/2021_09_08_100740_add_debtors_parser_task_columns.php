<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDebtorsParserTaskColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parser_tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('task_type')->nullable();
            $table->string('task_code')->nullable();
            $table->unsignedInteger('progress_items')->default(0);
            $table->unsignedInteger('items')->default(0);
            $table->timestamp('finished_at')->nullable();
            $table->timestamps();
        });
        Schema::create('fssp_tasks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('debtor_fssp_deal_id');
            $table->foreign('debtor_fssp_deal_id')
                ->references('id')
                ->on('debtor_fssp_deals')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->unsignedBigInteger('parser_task_id');
            $table->foreign('parser_task_id')
                ->references('id')
                ->on('parser_tasks')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fssp_tasks');
        Schema::dropIfExists('parser_tasks');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDebtorCourtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('debtors', function(Blueprint $table) {
            $table->string('loaded_address')->nullable();
            $table->string('address')->nullable();
        });
        Schema::create('debtor_courts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('debtor_id');
            $table->foreign('debtor_id')
                ->references('id')
                ->on('debtors')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->unsignedBigInteger('court_id');
            $table->foreign('court_id')
                ->references('id')
                ->on('courts')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('debtors', function(Blueprint $table) {
            $table->dropColumn('loaded_address');
            $table->dropColumn('address');
        });
        Schema::dropIfExists('debtor_courts');
    }
}

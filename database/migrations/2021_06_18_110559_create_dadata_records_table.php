<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDadataRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dadata_address_records', function (Blueprint $table) {
            $table->id();
            $table->string('address')->unique();
            $table->json('result')->nullable();
            $table->timestamps();
        });
        Schema::create('dadata_fio_records', function (Blueprint $table) {
            $table->id();
            $table->string('fio')->unique();
            $table->json('result')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dadata_address_records');
        Schema::dropIfExists('dadata_fio_records');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courts', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255);
            $table->string('address', 1000);
            $table->string('phone', 255);
            $table->string('email', 255);
            $table->string('url', 500);
            $table->smallInteger('court_type');
            $table->mediumInteger('region_code');
            $table->foreign('region_code')
                ->references('code')
                ->on('regions')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courts');
    }
}

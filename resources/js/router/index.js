import Vue from 'vue';
import VueRouter from 'vue-router';
import Store from '@/store';

Vue.use(VueRouter);

const routes = [
    {
        path: '/login',
        name: 'login-page',
        component:  () => import('@/views/auth/login'),
    },
    {
        path: '/reset-password',
        name: 'reset-password-page',
        component:  () => import('@/views/auth/reset'),
    },
    {
        path: '/',
        name: 'site-dashboard',
        component: () => import('@/views/dashboard'),
    },
    {
        path: '/user/service',
        name: 'service-users',
        component:  () => import('@/views/user/service/index'),
    },
    {
        path: '/user/service/:id',
        name: 'service-users-profile',
        component: () => import('@/views/user/service/profile'),
    },
    {
        path: '/user/company',
        name: 'company-users',
        component: () => import('@/views/user/company/index'),
    },
    {
        path: '/user/company/:id',
        name: 'company-users-profile',
        component: () => import('@/views/user/company/profile'),
    },
    {
        path: '/plan',
        name: 'company-plans',
        component: () => import('@/views/plans/index'),
    },
    {
        path: '/plan/:id',
        name: 'company-plan-edit',
        component: () => import('@/views/plans/edit'),
    },
    {
        path: '/company/:company_id/case',
        name: 'company-cases',
        component: () => import('@/views/company_case/index'),
    },
    {
        path: '/company/:company_id/case/0',
        name: 'company-case-create',
        component: () => import('@/views/company_case/create'),
    },
    {
        path: '/company/:company_id/case/:id',
        name: 'company-case-manage',
        component: () => import('@/views/company_case/main'),
    },
    {
        path: '/company',
        name: 'company',
        component: () => import('@/views/company/index'),
    },
    {
        path: '/company/:id',
        name: 'company-edit',
        component: () => import('@/views/company/edit'),
    },
    {
        path: '/region',
        name: 'regions',
        component: () => import('@/views/courts/regions'),
    },
    {
        path: '/region/:code',
        name: 'region-courts',
        component: () => import('@/views/courts/region_courts'),
    },
    {
        path: '/bailiff/region',
        name: 'regions-bailiffs',
        component: () => import('@/views/bailiff/regions'),
    },
    {
        path: '/bailiff/region/:code',
        name: 'region-bailiffs',
        component: () => import('@/views/bailiff/region_bailiffs'),
    },

    {
        path: '/region/:code/court/:type',
        name: 'region-courts-by-type',
        component: () => import('@/views/courts/courts'),
    },
    {
        path: '/region/:code/court/:type/:id',
        name: 'court',
        component: () => import('@/views/courts/court'),
    },
    {
        path: '/403',
        name: 'no-permission',
        component: () => import('@/views/403'),
    },
    {
        path: '*',
        name: 'not-found',
        component: () => import('@/views/404'),
    },

];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

router.beforeEach( (to, from, next) => {

  // Check Auth before each route
    switch (to.name) {
        case 'login-page':
        case 'reset-password-page':
            if (Store.getters['auth/authenticated']) {
                next({name: 'site-dashboard'});
                break;
            } else {
                next();
                break;
            }
        default:
            if (!Store.getters['auth/authenticated']) {
                next({name: 'login-page'});
                break;
            }
            next();
    }

});

export default router;



const methods = {
    validateField: (vm, field) => {
        vm.messages = {
            email: null,
            phone: null,
            name: null,
            password: null,
            password_confirmation: null,
            subdomain:null,
        };
        if(vm.toValidate && vm.toValidate.status === 422) {
            vm.messages.email = vm.toValidate.data.errors.email ?
                vm.toValidate.data.errors.email[0] : null;
            vm.messages.phone = vm.toValidate.data.errors.phone ?
                vm.toValidate.data.errors.phone[0] : null;
            vm.messages.password = vm.toValidate.data.errors.password ?
                vm.toValidate.data.errors.password[0] : null;
            vm.messages.name = vm.toValidate.data.errors.name ?
                vm.toValidate.data.errors.name[0] : null;
            vm.messages.password_confirmation = vm.toValidate.data.errors.password_confirmation ?
                vm.toValidate.data.errors.password_confirmation[0] : null;
            vm.messages.subdomain = vm.toValidate.data.errors.subdomain ?
                vm.toValidate.data.errors.subdomain[0] : null;
        }
        if (vm) return !vm.messages[field] || vm.messages[field];
        return true;
    },
};
/**
 * Auth module for vuex store.
 *
 * Holds user authentication details.
 */
import Axios from '@/utils/axios';
export default {

    state: {
        email: (vm) => [
            v => !!v || 'E-mail обязателен для заполнения',
            value => {
                const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                return pattern.test(value) || 'Некорректный e-mail.'
            },
            v =>  methods.validateField(vm, 'email'),
        ],
        number: (float) => [
            value => {
                const pattern = float ? /^[0-9]+(\.[0-9]+)*$/ : /^[0-9]+$/;
                return !value || pattern.test(value) || 'Некорректное значение.'
            },
        ],
        name: (vm) => [
            v => !!v || 'Наименование обязательно для заполнения',
            v =>  methods.validateField(vm, 'name'),
        ],
        subdomain: (vm) => [
            v => !!v || 'Поддомен обязателен для заполнения',
            v =>  methods.validateField(vm, 'subdomain'),
        ],
        phone: (vm) => [
            v => !!v || 'Телефон обязателен для заполнения',
            value => {
                const pattern = /^(\+?7[0-9]{10})$/;
                return pattern.test(value) || 'Некорректный телефон.'
            },
            v => methods.validateField(vm, 'phone'),
        ],
        password: (vm, required) => [
            v => !!v || !required|| 'Обязательно для заполнения.',
            v => v.length === 0 || (v.length >= vm.$store.state.passwordLength)
            || (`Пароли не указаны, не совпадают или содержит менее ` + vm.$store.state.passwordLength + ` символов`),
            v => methods.validateField(vm, 'password'),
        ],
        passwordConfirmation: (vm, required) => [
            v => !!v ||!required || 'Обязательно для заполнения.',
            v => v.length === 0 || (v.length >= vm.$store.state.passwordLength)
                || (`Пароли не указаны, не совпадают или содержит менее ` + vm.$store.state.passwordLength + ` символов`),
            v => !vm || !vm.user.password_confirmation || vm.user.password === vm.user.password_confirmation
                || (`Пароли не указаны, не совпадают или содержит менее ` + vm.$store.state.passwordLength + ` символов`),
            v => methods.validateField(vm, 'password_confirmation'),

        ],
        fio: [
            v => !!v || 'Имя обязательно для заполнения',
            v => (v && v.length >= 10) || 'Имя должно содержать более 10 символов',
        ],
        blockReason: [
            v => !!v || 'Поле обязательно для заполнения',
            v => (v && v.length >= 10) || 'Поле должно содержать более 10 символов',
        ],
        required: [
            v => (v instanceof Array ? !!v.length : !!v) || 'Поле обязательно для заполнения',
        ],
        company: {
            email: [
                value => {
                    const pattern = /^((([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,})))?$/
                    return !value ||pattern.test(value) || 'Некорректный e-mail.'
                },
            ],
            phone: [
                value => {
                    const pattern = /^(\+?7[0-9]{10})?$/;
                    return !value ||pattern.test(value) || 'Некорректный телефон.'
                },
            ],
            url: [
                value => {
                    const pattern = /^(http(s)?:\/\/(\w+\.)+\w+)?$/;
                    return !value ||pattern.test(value) || 'Введите корректный адрес сайта'
                }
            ],
            discount: [
                v => v>=0 && v<=100 || 'Скидка должна быть в пределах от 0-100%'
            ],
        }
    },
};

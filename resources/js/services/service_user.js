
import Axios, * as others from "@/utils/axios";

const path = '/user/service';
class ServiceUserService {
    constructor() {
        this.loadMeta();
    }
    getGroups() {
        if (!this.groups) {
            this.loadMeta()
        }
        return this.groups;
    }
    getStatuses() {
        if (!this.statuses) {
            this.loadMeta()
        }
        return this.statuses;
    }
    prepareParams(params) {
        params = {...params};
        if (params.user_group !== undefined)
            params.user_group = params.user_group ? params.user_group.value : null;
        if (params.status !== undefined)
            params.status = params.status ? params.status.value : null;
        if (!params.form_confirmed || !params.password_confirmation.length || !params.password){
            params.password_confirmation = undefined;
            params.password = undefined;
        }
        return params;
    }
    getAll(filter) {
        filter = filter === undefined ? {} : filter;
        return  Axios.get(path, {params: this.prepareParams(filter)});
    }

     loadMeta() {
        return  Axios.get(path + `/meta`).then((response) => {
            this.statuses = response.data.data.statuses.map(function(status,k) {
                return {
                    value: k,
                    text: status,
                }
            });
            this.groups = Object.keys(response.data.data.groups).map(function(gid) {
                return {
                    value: gid,
                    text: response.data.data.groups[gid].ru,
                }
            });
        });
    }

    get(id) {
        return Axios.get(path + `/${id}`);
    }

    create(data) {
        return Axios.post(path, this.prepareParams(data));
    }

    update(id, data) {
        return Axios.put(path + `/${id}`, this.prepareParams(data));
    }

    delete(id) {
        return http.delete(path + `/${id}`);
    }

    deleteAll() {
        return http.delete(path + ``);
    }

    findByTitle(title) {
        return http.get(path + `?title=${title}`);
    }
}

export default new ServiceUserService();
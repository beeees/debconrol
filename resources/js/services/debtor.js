
import Axios, * as others from "@/utils/axios";
import router from '@/router'

class CaseDebtorService {
    getResultColumns() {
        if (!this.resultColumns) {
            this.loadMeta()
        }
        return this.resultColumns;
    }
    getColumns() {
        if (!this.columns) {
            this.loadMeta()
        }
        return this.columns;
    }
    getCasePath() {
        return '/company/' + router.currentRoute.params.company_id + '/case/' + router.currentRoute.params.id+ '/debtor';
    }
    getCourtsArea(id) {
        return Axios.get(this.getCasePath() +'/' + id + '/area')
    }

    get(id) {
        return Axios.get(this.getCasePath() + '/' + id );
    }
    prepareParams(filter) {
        let params = {...filter};
        if (params.errors && params.errors.value !== null)  params.errors = params.errors.value
        else delete params.errors
        if (params.deal_state && params.deal_state.value !== null)  params.deal_state = params.deal_state.value
        else delete params.deal_state
        return params;
    }
    getAll(filter) {
        filter = filter === undefined ? {} : filter;
        return  Axios.get(this.getCasePath(), {params: this.prepareParams(filter)});
    }
    delete(ids) {
        return Axios.post(this.getCasePath() +'/delete', {ids, _method: 'DELETE'})
    }
    refresh(ids) {
        return Axios.post(this.getCasePath() +'/refresh', {ids, _method: 'PUT'})
    }
    update(debtor) {
        return Axios.post(this.getCasePath() +'/' + debtor.id, {...debtor, _method: 'PUT'})
    }
    loadMeta() {
        return  Axios.get(this.getCasePath() + `/meta`).then((response) => {
            if (response.data.data) {
                this.columns = [];
                Object.keys(response.data.data).forEach((type_key) => {
                    this.columns[type_key] = [];
                    Object.keys(response.data.data[type_key]).forEach( (v, k)=> {
                        this.columns[type_key][k]= {
                            text: response.data.data[type_key][v],
                            value: v,
                        };
                    });

                })
                console.log(this.columns);
            }
        });
    }

}

export default new CaseDebtorService();
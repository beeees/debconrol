
import Axios, * as others from "@/utils/axios";
import ServiceUserService from "./service_user";
const path = '/user/company';
class CompanyUserService {
    constructor() {
        this.loadMeta();
    }
    getGroups() {
        if (!this.groups) {
            this.loadMeta()
        }
        return this.groups;
    }
    getCompanies() {
        if (!this.companies) {
            this.loadMeta()
        }
        return this.companies;
    }
    getStatuses() {
        if (!this.statuses) {
            this.loadMeta()
        }
        return this.statuses;
    }
    prepareParams(params) {
        params = ServiceUserService.prepareParams(params);
        let companies = [];
        if (params.companies !== undefined){
            Object.keys(params.companies).map(v => {
                if (params.companies[v] instanceof Object && params.companies[v].value)
                    companies.push(params.companies[v].value);
            });
        }
        params.companies = companies;
        return params;
    }
    getAll(filter) {
        filter = filter === undefined ? {} : filter;
        return  Axios.get(path, {params: this.prepareParams(filter)});
    }
    download(filter) {
        filter = filter === undefined ? {} : filter;
        filter.export = true;
        Axios.get(path, {
            method: 'GET',
            responseType: 'blob',
            params: this.prepareParams(filter),
        }).then((response) => {
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', decodeURIComponent(response.headers['content-disposition'].split(';')[2].split("utf-8''")[1]));
            document.body.appendChild(link);
            link.click();
        });
    }

    loadMeta() {
        return  Axios.get(path + `/meta`).then((response) => {
            if (response.data.data.statuses)
            this.statuses = response.data.data.statuses.map(function(status,k) {
                return {
                    value: k,
                    text: status,
                }
            });
            if (response.data.data.groups)
            this.groups = Object.keys(response.data.data.groups).map(function(gid) {
                return {
                    value: gid,
                    text: response.data.data.groups[gid].ru,
                }
            });
            if (response.data.data.companies)
            this.companies = Object.keys(response.data.data.companies).map(function(c) {
                return {
                    value: response.data.data.companies[c].id,
                    text: response.data.data.companies[c].name,
                }
            });
        });
    }

    get(id) {
        return Axios.get(path + `/${id}`);
    }

    create(data) {
        return Axios.post(path, this.prepareParams(data));
    }
    checkEmail(data) {
        return Axios.get(path + '/check', {params: this.prepareParams(data)});
    }

    update(id, data) {
        return Axios.put(path + `/${id}`, this.prepareParams(data));
    }

    delete(id) {
        return http.delete(path + `/${id}`);
    }

    deleteAll() {
        return http.delete(path + ``);
    }

    findByTitle(title) {
        return http.get(path + `?title=${title}`);
    }
}

export default new CompanyUserService();
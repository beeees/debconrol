
import Axios, * as others from "@/utils/axios";
const path = '/plan';
class CompanyPlanService {
    constructor() {
        this.loadMeta();
    }
    getPeriods() {
        if (!this.periods) {
            this.loadMeta()
        }
        return this.periods;
    }
    prepareParams(params) {
        params = {...params};
        if (params.interval_type instanceof Object) {
            params.interval_type = params.interval_type.value;
        }
        return params;
    }
    getAll(filter) {
        filter = filter === undefined ? {} : filter;
        return  Axios.get(path, {params: this.prepareParams(filter)});
    }

    loadMeta() {
        return  Axios.get(path + `/meta`).then((response) => {
            if (response.data.data.periods)
                this.periods = Object.keys(response.data.data.periods).map(function(id) {
                    return {
                        value: id,
                        text: response.data.data.periods[id],
                    }
                });
        });
    }

    get(id) {
        return Axios.get(path + `/${id}`);
    }

    create(data) {
        return Axios.post(path, this.prepareParams(data));
    }

    update(id, data) {
        return Axios.put(path + `/${id}`, this.prepareParams(data));
    }

    delete(id) {
        return Axios.delete(path + `/${id}`);
    }

    deleteAll() {
        return Axios.delete(path + ``);
    }

    findByTitle(title) {
        return Axios.get(path + `?title=${title}`);
    }
}

export default new CompanyPlanService();
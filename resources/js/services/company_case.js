
import Axios, * as others from "@/utils/axios";
import router from '@/router'
class CompanyCaseService {

    prepareParams(caseParams) {
        let params = {...caseParams};
        if (params?.check_type?.value !== undefined) params.check_type = params.check_type.value;
        return params;
    }
    getCompanyPath(){
        return '/company/' + router.currentRoute.params.company_id +'/case';
    }
    getAll(filter) {
        filter = filter === undefined ? {} : filter;
        return  Axios.get(this.getCompanyPath(), {params: this.prepareParams(filter)});
    }

    get(id) {
        return Axios.get(this.getCompanyPath() + `/${id}`);
    }

    create(data) {
        return Axios.post(this.getCompanyPath(), this.prepareParams(data));
    }

    update(id, data) {
        return Axios.put(this.getCompanyPath() + `/${id}`, this.prepareParams(data));
    }

    delete(id) {
        return Axios.delete(this.getCompanyPath() + `/${id}`);
    }
    getCheckType(checkType) {
        switch (checkType) {
            case 3: return 'ФССП';
            case 2: return 'Судебные дела';
            case 1: return 'Полная';
            default: return "Оценка";
        }
    }
    getCheckTypes() {
        return [
            {value: 0, text: 'Оценка'},
            {value: 1, text: 'Полная'},
            {value: 2, text: 'Судебные дела'},
            {value: 3, text: 'Исполнительные производства'},
        ];
    }
    toFsspCheck(caseId, selectedItems) {
        return Axios.post(this.getCompanyPath() +`/${caseId}/deal-fssp`, {debtors: selectedItems.map((v) => v?.id|| v)})
    }
    toDealCheck(caseId, selectedItems) {
        return Axios.post(this.getCompanyPath() +`/${caseId}/fssp-deal`, {debtors: selectedItems.map((v) => v?.id|| v)})
    }
}

export default new CompanyCaseService();
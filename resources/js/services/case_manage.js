
import Axios, * as others from "@/utils/axios";
import router from '@/router'

class CaseManageService {
    constructor() {
        this.formats = this.getFormats();
    }
    getCasePath() {
        return '/company/' + router.currentRoute.params.company_id + '/case/' + router.currentRoute.params.id+ '/file';
    }
    getFormats() {
        if (!this.formats || !this.formats.length) {
            Axios.get(this.getCasePath() + '/formats').then(response => {
                this.formats = response.data.data;
            });
        }
        return this.formats || [];
    }

    download(format) {
        Axios.get(this.getCasePath() + '/example/' + format , {
            method: 'GET',
            responseType: 'blob',
        }).then((response) => {
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', decodeURIComponent(response.headers['content-disposition'].split(';')[2].split("utf-8''")[1]));
            document.body.appendChild(link);
            link.click();
        });
    }

    prepareParams(params, simple) {
        params = {...params};
        if (simple) return params;
        let formData = new FormData();
        Object.keys(params).map(function(name, k) {
            if (name != 'file' && !(params[name] instanceof Object)) {
                let val = params[name];
                val = val === null ? '' : val;
                formData.append(name,  val);
            }
        });
        if (params.format !== undefined && params.file && params.file.name) formData.append("file", params.file, params.file.name);
        return formData;
    }

    import(id, data) {
        data = {id, ...data};
        return Axios.post(this.getCasePath(), this.prepareParams(data));
    }

    get(id) {
        return Axios.get(this.getCasePath() + '/' + id )
    }

    check(files) {
        let params = {files : []};
        if (files && files.length) {
            files.forEach((v) => {
                params.files[params.files.length] = v.id;
            })
        }
        return Axios.get(this.getCasePath() + '/progress', {params} );
    }
}

export default new CaseManageService();
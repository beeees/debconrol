
import Axios, * as others from "@/utils/axios";
import router from '@/router'

class FileLoaderService {

    download(path, filter) {
        filter = filter === undefined ? {} : filter;
        Object.keys(filter).forEach((key) => {
            if (filter[key] && filter[key].value !== undefined) {
                filter[key] = filter[key].value;
            }
        });
        return  Axios.get(path, {
            params: filter,
            method: 'GET',
            responseType: 'blob',
        }).then((response) => {
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', decodeURIComponent(response.headers['content-disposition'].split(';')[2].split("utf-8''")[1]));
            document.body.appendChild(link);
            link.click();
        });
    }


}

export default new FileLoaderService();
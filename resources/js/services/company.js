
import Axios, * as others from "@/utils/axios";
import company_plan from "./company_plan";
const path = '/company';
class CompanyService {
    constructor() {
        this.loadMeta();
    }
    getPlans() {
        if (!this.plans) {
            this.loadMeta()
        }
        return this.plans;
    }
    prepareParams(params, simple) {
        params = {...params};
        let formData = new FormData();
        Object.keys(params).map(function(name, k) {
            if (name != 'logo' && !(params[name] instanceof Object)) {
                let val = params[name];
                val = val === null ? '' : val;
                formData.append(name,  val);
            }
        });
        if (params.company_plan_id)
            params.company_plan_id = params.company_plan_id.value;
        formData.set('company_plan_id', params.company_plan_id);
        if (params.is_active instanceof Object)
            params.is_active = params.is_active.value;
        if (params.user && params.user.id) formData.set('user_id', params.user.id > 0 ? params.user.id : null);
        formData.set('is_active' , params.is_active ? 1 : 0);
        if (simple) return params;
        // files
        if (params.form_confirmed && params.logo && params.logo.name) formData.append("logo", params.logo, params.logo.name);
        return formData;
    }
    getAll(filter) {
        filter = filter === undefined ? {} : filter;
        return  Axios.get(path, {params: this.prepareParams(filter, true)});
    }

    loadMeta() {
        return  Axios.get(path + `/meta`).then((response) => {
            if (response.data.data.plans)
                this.plans = Object.keys(response.data.data.plans).map(function(id) {
                    return {
                        value: response.data.data.plans[id].id,
                        text: response.data.data.plans[id].name,
                    }
                });
        });
    }

    get(id) {
        return Axios.get(path + `/${id}`);
    }

    create(data) {
        return Axios.post(path, this.prepareParams(data));
    }

    update(id, data) {
        return Axios.post(path + `/${id}`, this.prepareParams({...data, _method: 'PUT'}));
    }

    delete(id) {
        return Axios.delete(path + `/${id}`);
    }

    deleteAll() {
        return Axios.delete(path + ``);
    }

    findByTitle(title) {
        return Axios.get(path + `?title=${title}`);
    }
}

export default new CompanyService();
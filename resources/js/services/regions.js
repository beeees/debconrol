import Axios from "@/utils/axios";

const path = '/court';
class RegionService {

    constructor() {
        this.loadMeta();
    }
    getRegions() {
        if (!this.regions) {
            this.loadMeta()
        }
        return this.regions;
    }
    getCourtTypes() {
        if (!this.court_types) {
            this.loadMeta()
        }
        return [{value: null, text: 'Все суды'}].concat(this.court_types);
    }
    getSupremeCourtType() {
        if (!this.supreme_type) {
            this.loadMeta()
        }
        return this.supreme_type;
    }
    getLastUpdate() {
        if (!this.last_update)
            this.loadMeta();
        return this.last_update;
    }
    prepareParams(params) {
        params = {...params};
        return params;
    }
    loadMeta() {
        if (this.regions !== undefined) return;
        return  Axios.get(path + `/meta`).then((response) => {
            if (response.data.data.regions)
                this.regions = Object.keys(response.data.data.regions).map(function(id) {
                    return {
                        value: response.data.data.regions[id].code,
                        text: response.data.data.regions[id].name,
                        created_at: response.data.data.regions[id].created_at,
                    }
                });
            if (response.data.data.court_types)
                this.court_types = Object.keys(response.data.data.court_types).map(function(id) {
                    return {
                        value: id,
                        text: response.data.data.court_types[id],
                    }
                });
            this.last_update = response.data.data.last_update;
        });
    }
    getCourtsByTypeCounts(region) {
        return Axios.get('/region/' + region);
    }
    getAllBailiffs(filter) {
        return Axios.get('/bailiff/', {params: this.prepareParams(filter)});
    }
    getAll(filter) {
        return Axios.get(path,  {params: this.prepareParams(filter)});
    }
    get(id) {
        return Axios.get(path+ '/'+id)
    }
    getPath() {
        return path;
    }
    import(region, data) {
        let params = {...data};
        let formData = new FormData();
        if (params.file && params.file.name) formData.append("file", params.file, params.file.name);
        return Axios.post('/region/' + region + '/court/import', formData);
    }
    ifnsImport(data) {
        let params = {...data};
        let formData = new FormData();
        if (params.file && params.file.name) formData.append("file", params.file, params.file.name);
        return Axios.post('/court/ifns', formData);
    }
}

export default new RegionService();
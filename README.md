**manual**

## Configuring

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. npm install vue-template-compiler --save-dev --production=false
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

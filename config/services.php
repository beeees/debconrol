<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    'dadata' => [
        'baseUrl' => env('DADATA_API_URL', "https://cleaner.dadata.ru/api/v1/"),
        'secret' => env('DADATA_SECRET', ''),
        'token' => env('DADATA_TOKEN', ''),
    ],
    'antikapcha' => [
        'baseUrl' => env('ANTIKAPCHA_API_URL', "https://api.anti-captcha.com"),
        'key' => env('ANTIKAPCHA_KEY', ''),
    ],

];
